/*! \file particleSystemCPU.cpp
 *  \brief Particle system effect
 *  \author Adam
*/

#include "scene.hpp"
#include "model.hpp"
#include "camera.hpp"
#include "shader.hpp"
#include "particleSystemCPU.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <algorithm>
#include <functional>

ParticleSystemCPU::ParticleCPU::ParticleCPU()
{
    alpha = -1;
    cameraDistance = -1;
    xyz[0] = xyz[1] = xyz[2] = -std::numeric_limits<float>::max();
}

ParticleSystemCPU::ParticleSystemCPU(size_t particleCount, const std::string& name, Scene* scene, Model* model, const glm::mat4& modelMatrix)
    : SceneObject(name, scene, model, modelMatrix)
    , lastAnimate(0)
    , particlesVBO(0)
    , activeParticleCount(0)
    , particlesCPU(particleCount)
    , particlesGPU(particleCount)
{
    // The VBO containing the positions and sizes of the particles
    glGenBuffers(1, &particlesVBO);
    glBindBuffer(GL_ARRAY_BUFFER, particlesVBO);
    glBufferData(GL_ARRAY_BUFFER, particleCount * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    ParticleSystemCPU::setShader(scene->getShader("particleCPU"));
}

ParticleSystemCPU::~ParticleSystemCPU()
{
    glDeleteBuffers(1, &particlesVBO);
}

void ParticleSystemCPU::draw() const
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_STENCIL_TEST);
    glEnable(GL_DEPTH_TEST);

    shader->use();

    glm::mat4 M = getGlobalModelMatrix();
    glm::mat4 V = scene->getCamera()->getViewMatrix();
    glm::mat4 MV = V * M;
    glm::mat4 VP = scene->getCamera()->getProjectionMatrix() * scene->getCamera()->getViewMatrix();
    glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;

    //upload particles
    glBindBuffer(GL_ARRAY_BUFFER, particlesVBO);
    glBufferData(GL_ARRAY_BUFFER, particlesGPU.size() * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, activeParticleCount * sizeof(GLfloat) * 4, particlesGPU.data());
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "V"), 1, GL_FALSE, glm::value_ptr(V));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MV"), 1, GL_FALSE, glm::value_ptr(MV));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "VP"), 1, GL_FALSE, glm::value_ptr(VP));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MVP"), 1, GL_FALSE, glm::value_ptr(MVP));

    glBindVertexArray(vao);
    glVertexAttribDivisor(glGetAttribLocation(shader->programHandle(), "position"), 0); // particles vertices : always reuse the same 4 vertices -> 0
    glVertexAttribDivisor(glGetAttribLocation(shader->programHandle(), "particle"), 1); // positions : one per quad (its center) -> 1
    glDrawArraysInstanced(GL_TRIANGLES, 0, 4, activeParticleCount);
    glBindVertexArray(0);

    glDisable(GL_BLEND);
    glEnable(GL_STENCIL_TEST);
    glDisable(GL_DEPTH_TEST);
}

void ParticleSystemCPU::push(const glm::vec3& pos)
{
    ParticleCPU p;
    p.xyz[0] = pos.x;
    p.xyz[1] = pos.y;
    p.xyz[2] = pos.z;
    p.alpha = 1.0f;
    particlesNew.push_back(p);
}

bool ParticleSystemCPU::animate(double time)
{
    double deltaT = time - lastAnimate;
    deltaT = std::max(deltaT, 0.0); //handle restart
    ParticleSystemCPU::update(deltaT);
    lastAnimate = time;
    return SceneObject::animate(time);
}

void ParticleSystemCPU::update(double deltaT)
{
    activeParticleCount = 0;
    float delta = static_cast<float>(deltaT / 10.0);

    for(size_t i = 0; i < particlesCPU.size(); ++i) {
        ParticleCPU& p = particlesCPU[i];

        if(p.alpha > 0.0f)
        { //particle lives
            p.alpha -= delta;
            glm::vec4 viewPos = scene->getCamera()->getGlobalModelMatrix() * glm::vec4(0, 0, 0, 1);
            p.cameraDistance = glm::length(glm::vec4(p.xyz[0], p.xyz[1], p.xyz[2], 1) - viewPos);
            if(p.alpha > 0.0f) {
                //particle lives
                ++activeParticleCount;
            } else {
                //particle just died
                if(!particlesNew.empty()) { //add new particle
                    particlesCPU[i] = particlesNew.back();
                    particlesNew.pop_back();
                } else { //no new particle
                    p.cameraDistance = -1;
                }
            }
        } else {
            //particle is dead
            if(!particlesNew.empty()) {
                particlesCPU[i] = particlesNew.back();
                particlesNew.pop_back();
            }
        }
    }

    //NOTE: deletes new particles, if the buffer is too small
    //not nice, but buffer is big enough and frame independent
    particlesNew.clear();

    //sort particles by camera distance to get correct transparency
    //buffer will be aligned, since dead particles have minus camera distance
    std::sort(particlesCPU.begin(), particlesCPU.end(), std::greater<ParticleCPU>());

    //copy particles to buffer which is copied to GPU(ParticleGPU has less variables)
    //NOTE: it would be possible to upload ParticleCPU and add stride to position binding
    for(size_t i = 0; i < (size_t)activeParticleCount; ++i) {
        ParticleCPU& src = particlesCPU[i];
        ParticleGPU& dst = particlesGPU[i];

        dst.alpha = src.alpha;
        dst.xyz[0] = src.xyz[0];
        dst.xyz[1] = src.xyz[1];
        dst.xyz[2] = src.xyz[2];
    }
}

void ParticleSystemCPU::setShader(Shader* val)
{
    shader = val;

    glDeleteVertexArrays(1, &vao);
    glGenVertexArrays(1, &vao);

    glBindVertexArray(vao);

    GLint pos;
    glBindBuffer(GL_ARRAY_BUFFER, model->getPosition());
    pos = glGetAttribLocation(shader->programHandle(), "position");
    glEnableVertexAttribArray(pos);
    glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, particlesVBO);
    pos = glGetAttribLocation(shader->programHandle(), "particle");
    glEnableVertexAttribArray(pos);
    glVertexAttribPointer(pos, 4, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
