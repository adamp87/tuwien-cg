/*! \file lightCamera.cpp
 *  \brief Unused
 *  \author Felix
*/

#include "light.hpp"
#include "lightCamera.hpp"
#include "model.hpp"
#include "scene.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "texture.hpp"
#include "sceneObject.hpp"

#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>

const unsigned int texSizeHeight = 512;
const unsigned int texSizeWidth = 512;

class LightCamera::EffectObjectLightCamShadow : public SceneObject
{
public:
    //glm::mat4 M;
    //glm::mat4 V;
    //glm::mat4 MV;
    //glm::mat4 MVP;
    //glm::mat4 PV;
    //glm::vec3 MlightPosition;
    //glm::vec4 lpos;
    //Shader* sAmbient;
    //Shader* sSv;

    EffectObjectLightCamShadow(Shader* shaderArg, std::shared_ptr<SceneObject> effectParent)
        : SceneObject(effectParent)
    {
        setShader(shaderArg);
    }

    void draw() const {
        if(!(shader && model && scene))
            return;
        shader->use();

        glm::mat4 M = getGlobalModelMatrix();
        glm::mat4 V = scene->getCamera()->getViewMatrix();
        glm::mat4 MV = V * M;
        glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;
        glm::mat3 N = glm::mat3(glm::inverseTranspose(M));
        glm::mat4 PV = scene->getCamera()->getProjectionMatrix() * V;

        glm::vec4 MlightPosition = glm::vec4(scene->getLight()->getPosition(),1.0);
        glm::vec2 near_far = glm::vec2(scene->getCamera()->getNearClip(),scene->getCamera()->getFarClip());

        glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "PV_mat"), 1, GL_FALSE, glm::value_ptr(PV));
        glUniform2fv(glGetUniformLocation(shader->programHandle(), "near_far"), 1, glm::value_ptr(near_far));
        glUniform4fv(glGetUniformLocation(shader->programHandle(), "MlightPosition"), 1, glm::value_ptr(MlightPosition));
        glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));

        glBindVertexArray(vao);
        glDrawElements(GL_TRIANGLES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);

    }
};

LightCamera::LightCamera(const std::string& name, Scene* scene, glm::vec3& pos, glm::vec3& up, glm::vec3& eye, float fov, float near, float far)
    : Camera(name,scene,pos,up,eye,fov,near,far) {
     
     // Create and bind an FBO
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
     
     // depth texture is needed for z-testing
     setUpDepthTexture();

     glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);
     
     glDrawBuffer(GL_NONE);
     glReadBuffer(GL_NONE);

     glBindTexture(GL_TEXTURE_2D, 0);
     glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

LightCamera::~LightCamera()
{

}

void LightCamera::setShader(Shader* val) {
    SceneObject::setShader(shader);
}

void LightCamera::draw() const
{

}

void LightCamera::update(double deltaT) {
    Camera::update(deltaT);
}

void LightCamera::reset() {
    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        effect = obj->getEffect("LightCamShadow");
        if(!effect) {
            std::unique_ptr<SceneObject> eff(new EffectObjectLightCamShadow(scene->getShader("VolumeLightShadowPass"),obj));
            obj->addEffect("LightCamShadow", eff);
        }
    }
}

void LightCamera::drawShadowMap() 
{
    //iterate over all sceneObjects and render them into the 6 faces.
    static const GLfloat zero[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    static const GLfloat one = 1.0f;

    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    
    glClearBufferfv(GL_COLOR, 0, zero);
    glClearBufferfv(GL_DEPTH, 0, &one);
    
    glViewport(0, 0, texSizeWidth, texSizeHeight);   


    //render into the cubemap faces
    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        effect = obj->getEffect("LightCamShadow");
        if(effect) {
            effect->draw();
        } else {
            std::cout << "Internal error: No effect for object" << std::endl;
        }
    }

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "Framebuffer incomplemte after rendering: " << glewGetErrorString(glGetError()) << std::endl;
        return;
    }

    //rebind default framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    int width, height;
    glfwGetWindowSize(scene->getWindow(), &width, &height);
    glViewport(0, 0, width, height);
}

void LightCamera::setUpDepthTexture() 
{
    glGenTextures(1, &depthTexture);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_DEPTH_COMPONENT24, texSizeWidth, texSizeHeight, 0,GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}
