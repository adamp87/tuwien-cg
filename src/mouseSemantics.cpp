/*! \file mouseSemantics.cpp
 *  \brief Mouse movement handling
 *  \author Felix
*/

#include "mouseSemantics.hpp"
#include "bubbleCursor.hpp"
#include "scene.hpp"

#include <stdlib.h>

#include <iostream>
#include <functional>
#include <algorithm>

#include <glm/gtc/type_ptr.hpp>

void cursorEnterCallbackHandle(GLFWwindow *window, int entered) {
    Scene *sceneObj = reinterpret_cast<Scene *> (glfwGetWindowUserPointer(window));
    sceneObj->getMouseSemantics()->cursorEnterCallBack(window, entered);
}

void windowFocusCallbackHandle(GLFWwindow *window, int entered) {
    Scene *sceneObj = reinterpret_cast<Scene *> (glfwGetWindowUserPointer(window));
    sceneObj->getMouseSemantics()->windowFocusCallBack(window, entered);
}

void keyCallbackHandle(GLFWwindow *window, int key, int scancode, int action, int mode) {
    Scene *sceneObj = reinterpret_cast<Scene *> (glfwGetWindowUserPointer(window));
    sceneObj->getMouseSemantics()->keyCallback(window, key, scancode, action, mode);
    sceneObj->getBubbleCursor()->keyCallback(window, key, scancode, action, mode);
}

//TODO autofocus of window
MouseSemantics::MouseSemantics(Scene* scene, float _M, float _m, float _C, Texture* _texture)
    : SceneObject("mouse",scene)
	, M(_M)
	, m(_m)
	, C(_C)
	, latestPos(glm::vec2(0,0))
	, distance(0)
	, inWindow(false)
	, firstTimeInWindow(false)
    , focused(true)
	, texture(_texture)
	, inverseScaleFunction(false)
	, additionalScaling(1.0f)
{	
	glfwSetInputMode(scene->getWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    glfwSetCursorEnterCallback(scene->getWindow(), cursorEnterCallbackHandle);
    glfwSetWindowFocusCallback(scene->getWindow(), windowFocusCallbackHandle);
    glfwSetKeyCallback(scene->getWindow(), keyCallbackHandle);
}

MouseSemantics::~MouseSemantics() {
	glDeleteBuffers(1, &vboPositions);
	glDeleteVertexArrays(1, &vao);
}

void MouseSemantics::toggleScaleFunction() {
	inverseScaleFunction = !inverseScaleFunction;
}

void MouseSemantics::updateDistanceState(unsigned int _distance) {
	distance = (int) _distance;
}

void MouseSemantics::setShader(Shader *_shader) {
	shader = _shader;

	//TODO try w/o this but don't know if possible since glDrawArrays... is needed
	float position[] = { 1.0f, 1.0f };

	glDeleteVertexArrays(1, &vao);
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vboPositions);
	glBindBuffer(GL_ARRAY_BUFFER, vboPositions);
	glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float), position, GL_STATIC_DRAW);
	GLint pos;
	pos = glGetAttribLocation(shader->programHandle(), "position");
	if (pos != -1) {
		glEnableVertexAttribArray(pos);
		glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void MouseSemantics::cursorEnterCallBack(GLFWwindow *window, int entered) {
	if (entered) {
		inWindow = true;
		firstTimeInWindow = true;
	}
	else {
		inWindow = false;
		firstTimeInWindow = false;
	}
}

void MouseSemantics::windowFocusCallBack(GLFWwindow *window, int entered) {
	if (entered) {
		focused = true;
	}
	else {
		focused = false;
	}
}

void MouseSemantics::keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_F10 && action == GLFW_PRESS) {
		toggleScaleFunction();
	}
	else if (key == GLFW_KEY_J && action == GLFW_PRESS) {
		additionalScaling += 0.05f;
	}
	else if (key == GLFW_KEY_K && action == GLFW_PRESS) {
		additionalScaling += -0.05f;
	}
}

void MouseSemantics::update(double deltaT) {

	double xpos;
	double ypos;
	glfwGetCursorPos(scene->getWindow(), &xpos, &ypos);

	//disable movement scaling when moving camera
	if (glfwGetMouseButton(scene->getWindow(), GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS
		|| glfwGetMouseButton(scene->getWindow(), GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS
		|| !inWindow
		|| !focused) {
		return;
	}

	if (firstTimeInWindow) {
		firstTimeInWindow = false;
		latestPos.x = xpos;
		latestPos.y = ypos;
		return;
	}

	glm::vec2 direction = glm::vec2((float)xpos, (float)ypos) - latestPos;
	float dlength = glm::length(direction);

#if 0
	system("cls");
	std::cout << "Latest pos x " << latestPos.x << "Latest pos y " << latestPos.y << std::endl;
	std::cout << "Curr pos x " << xpos << "Curr pos y " << ypos << std::endl;
	std::cout << "Length: " << dlength << std::endl;
	std::cout << "distance: " << distance << std::endl;
	std::cout << "distance / C: " << distance / C << std::endl;
	std::cout << "(distance / C) * (M - m): " << (distance / C) * (M - m) << std::endl;
	std::cout << "(distance / C) * (M - m) + m: " << (distance / C) * (M - m) + m << std::endl;
	std::cout << "(distance / C) * (M - m) + m: " << (distance / C) * (M - m) + m << std::endl;
	std::cout << "(m - M) / std::pow((distance + 1), 0.1): " << (m - M) / std::pow((distance + 1), 0.1) << std::endl;
	std::cout << "Inverse scale funciton " << (m - M) / std::pow((distance + 1), 0.1) + M << std::endl;
#endif

	if (dlength > glm::epsilon<float>()) {
		/*std::cout << "Scale factor   " << calculateScaleFactor() << std::endl;
		std::cout << "distance / C  " << std::max(0.2f, std::min((float)distance / C, 50.0f)) << std::endl;*/
		direction = direction * calculateScaleFactor() * additionalScaling;
		latestPos = latestPos + direction;

		glfwSetCursorPos(scene->getWindow(), latestPos.x, latestPos.y);
	}
}

float MouseSemantics::calculateScaleFactor() {

	if (!inverseScaleFunction) {
		if (C < distance) {
			return M;
		}
		else if (0 < distance && distance < C) {
			return (distance / C) * (M - m) + m;
		}
		else {
			return m;
		}
	}
	else {
		return (m - M) / std::pow((distance + 1), 0.4) + M;
	}
}

void MouseSemantics::draw() {

	//do not show cursor when moving the camera or when the cursor is not in the window
	if (glfwGetMouseButton(scene->getWindow(), GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS
		|| glfwGetMouseButton(scene->getWindow(), GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS
		|| !inWindow
		|| !focused)
		return;

	shader->use();

	int width, height;
	glfwGetWindowSize(scene->getWindow(), &width, &height);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_POINT_SPRITE);
	glEnable(GL_PROGRAM_POINT_SIZE);
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, texture->getHandle());
	glUniform1i(glGetUniformLocation(shader->programHandle(), "cursorTexture"), 0);
	glUniform1f(glGetUniformLocation(shader->programHandle(), "size"), std::max(0.2f , std::min((float)(2.0f*distance)/C, 1.0f)));
	glUniform2fv(glGetUniformLocation(shader->programHandle(), "mousePos"), 1, glm::value_ptr(glm::vec2(latestPos.x/width, (height - latestPos.y)/height)));

	glBindVertexArray(vao);
	glDrawArrays(GL_POINTS, 0, 1);
	glBindVertexArray(0);

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);	
}
