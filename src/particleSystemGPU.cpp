/*! \file particleSystemGPU.cpp
 *  \brief Particle system effect
 *  \author Adam
*/

#include "scene.hpp"
#include "model.hpp"
#include "camera.hpp"
#include "shader.hpp"
#include "shadowMap.hpp"
#include "shadowOmni.hpp"
#include "particleSystemGPU.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <algorithm>

const int counterCount = 2;
const int maxNewParticle = 6;
const int sizeOfParticle = 16 * 3;
const int particleMaxCount = 512 << 2;

ParticleSystemGPU::ParticleSystemGPU(const std::string& name, Scene* scene, Model* model, const glm::mat4& modelMatrix)
    : Tessellation(name, scene, model, modelMatrix)
    , lastUpdate(0)
    , lastAnimate(0)
{
    // Atomic Counters
    counters.resize(counterCount, 0);
    glGenBuffers(1, &countersVBO);
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, countersVBO);
    glBufferData(GL_ATOMIC_COUNTER_BUFFER, counterCount * sizeof(GLuint), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);

    // The SSBO containing the pos, dir and attribs of the particles
    std::vector<GLuint> nulls(sizeOfParticle * particleMaxCount, 0);
    glGenBuffers(1, &particlesVBO);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, particlesVBO);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeOfParticle * particleMaxCount, nulls.data(), GL_STREAM_COPY);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    // The SSBO containing the x, y, z and life of the particles in model space
    glGenBuffers(1, &particlesPosVBO);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, particlesPosVBO);
    glBufferData(GL_SHADER_STORAGE_BUFFER, 4 * sizeof(float) * particleMaxCount, NULL, GL_STREAM_COPY);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    computeShader = scene->getShader("particleSystemGPU");
    ParticleSystemGPU::setShader(scene->getShader("particleTessGPU"));
}

ParticleSystemGPU::~ParticleSystemGPU()
{
    glDeleteBuffers(1, &countersVBO);
    glDeleteBuffers(1, &particlesVBO);
    glDeleteBuffers(1, &particlesPosVBO);
}

void ParticleSystemGPU::update(double deltaT)
{
    computeShader->use();

    // Bind the SSBO
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, particlesPosVBO);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, particlesVBO);
    glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 2, countersVBO);

    // Zero counters
    std::fill(counters.begin(), counters.end(), 0);
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, countersVBO);
    glBufferSubData(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(GLuint) * counterCount, counters.data());
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);

    glUniform1f (0, (float)deltaT);
    glUniform1ui(1, newParticles.size());
    glUniform1ui(2, particleMaxCount);

    //upload new particles(NOTE: currently only one)
    if(newParticles.size()) {
        glUniform3fv(3, 1, glm::value_ptr(newParticles[0].pos));
        glUniform3fv(4, 1, glm::value_ptr(newParticles[0].dir));
        glUniform3fv(5, 1, glm::value_ptr(newParticles[0].atr));
        //glUniform3fv(3, newParticles.size() * 3, glm::value_ptr(newParticles[0].pos));
    }

    glDispatchCompute(particleMaxCount, 1, 1);

    //get counter values
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, countersVBO);
    glGetBufferSubData(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(GLuint) * counterCount, counters.data());
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
    glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 2, 0);

    newParticles.clear();
}

void ParticleSystemGPU::draw() const
{
    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_STENCIL_TEST);

    shader->use();

    glm::mat4 M = getGlobalModelMatrix();
    glm::mat4 V = scene->getCamera()->getViewMatrix();
    glm::mat4 MV = V * M;
    glm::mat4 VP = scene->getCamera()->getProjectionMatrix() * scene->getCamera()->getViewMatrix();
    glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;
    glm::mat3 N = glm::mat3(glm::inverseTranspose(M));

    glm::vec4 McameraPosition = scene->getCamera()->getGlobalModelMatrix() * glm::vec4(0,0,0,1);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_3D, dispTexture);
    glUniform1i(glGetUniformLocation(shader->programHandle(), "dispTexture"), 0);

    glUniform1ui(glGetUniformLocation(shader->programHandle(), "tessLevelIn"), tessLevelIn);
    glUniform1ui(glGetUniformLocation(shader->programHandle(), "tessLevelOut"), tessLevelOut);

    glUniformMatrix3fv(glGetUniformLocation(shader->programHandle(), "N"), 1, GL_FALSE, glm::value_ptr(N));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "V"), 1, GL_FALSE, glm::value_ptr(V));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MV"), 1, GL_FALSE, glm::value_ptr(MV));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "VP"), 1, GL_FALSE, glm::value_ptr(VP));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MVP"), 1, GL_FALSE, glm::value_ptr(MVP));

    glUniform1f (glGetUniformLocation(shader->programHandle(), "shininess"), model->getShininess());
    glUniform4fv(glGetUniformLocation(shader->programHandle(), "ambientColor"), 1, glm::value_ptr(model->getAmbient()));
    glUniform4fv(glGetUniformLocation(shader->programHandle(), "diffuseColor"), 1, glm::value_ptr(model->getDiffuse()));
    glUniform4fv(glGetUniformLocation(shader->programHandle(), "specularColor"), 1, glm::value_ptr(model->getSpecular()));
    glUniform4fv(glGetUniformLocation(shader->programHandle(), "McameraPosition"), 1, glm::value_ptr(McameraPosition));

#ifdef USE_OMNI_SHADOW
    ShadowOmni* shadow = (ShadowOmni*)scene->getLight().get();
    glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_CUBE_MAP, shadow->getDepthTexture());
    glUniform1i(glGetUniformLocation(shader->programHandle(), "cm_z_tex"), 1);

    glm::vec2 near_far = glm::vec2(scene->getCamera()->getNearClip(),scene->getCamera()->getFarClip());
    glUniform2fv(glGetUniformLocation(shader->programHandle(), "near_far"), 1, glm::value_ptr(near_far));
#elif defined USE_SHADOW_MAP
    ShadowMap* shadow = (ShadowMap*)scene->getLight().get();
    glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_2D, shadow->getDepthTexture());
    glUniform1i(glGetUniformLocation(shader->programHandle(), "shadowMap"), 1);
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "depthBiasMVP"), 1, GL_FALSE, glm::value_ptr(shadow->getDepthBiasMVP()));
#endif

    glUniform3fv(glGetUniformLocation(shader->programHandle(), "lightPower"), 1, glm::value_ptr(scene->getLight()->getPower()));
    glUniform3fv(glGetUniformLocation(shader->programHandle(), "lightAmbient"), 1, glm::value_ptr(scene->getLight()->getAmbient()));
    glUniform3fv(glGetUniformLocation(shader->programHandle(), "lightDiffuse"), 1, glm::value_ptr(scene->getLight()->getDiffuse()));
    glUniform3fv(glGetUniformLocation(shader->programHandle(), "lightSpecular"), 1, glm::value_ptr(scene->getLight()->getSpecular()));
    glUniform3fv(glGetUniformLocation(shader->programHandle(), "MlightPosition"), 1, glm::value_ptr(scene->getLight()->getPosition()));

    glBindVertexArray(vao);
    glVertexAttribDivisor(glGetAttribLocation(shader->programHandle(), "particle"), 1); // positions : one per cube (its center) -> 1
    glVertexAttribDivisor(glGetAttribLocation(shader->programHandle(), "position"), 0); // particles vertices : always reuse the same 4 vertices -> 0
    glVertexAttribDivisor(glGetAttribLocation(shader->programHandle(), "normal"), 0); // particles vertices : always reuse the same 4 vertices -> 0
    glVertexAttribDivisor(glGetAttribLocation(shader->programHandle(), "uv"), 0); // particles vertices : always reuse the same 4 vertices -> 0
    glDrawArraysInstanced(GL_PATCHES, 0, model->getIndexCount(), counters[0]);
    glBindVertexArray(0);

    glEnable(GL_STENCIL_TEST);
    glDisable(GL_DEPTH_TEST);
}

void ParticleSystemGPU::push(const NewParticle& p)
{
    if(maxNewParticle <= newParticles.size())
        return;

    newParticles.push_back(p);
}

bool ParticleSystemGPU::animate(double time)
{
    double deltaT = time - lastAnimate;
    deltaT = std::max(deltaT, 0.0); //handle restart
    ParticleSystemGPU::update(deltaT);
    lastAnimate = time;
    return SceneObject::animate(time);
}

void ParticleSystemGPU::setShader(Shader* val)
{
    shader = val;

    glDeleteVertexArrays(1, &vao);
    glGenVertexArrays(1, &vao);

    glBindVertexArray(vao);

    GLint pos;
    glBindBuffer(GL_ARRAY_BUFFER, model->getPosition());
    pos = glGetAttribLocation(shader->programHandle(), "position");
    glEnableVertexAttribArray(pos);
    glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, particlesPosVBO);
    pos = glGetAttribLocation(shader->programHandle(), "particle");
    glEnableVertexAttribArray(pos);
    glVertexAttribPointer(pos, 4, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, model->getNormals());
    pos = glGetAttribLocation(shader->programHandle(), "normal");
    glEnableVertexAttribArray(pos);
    glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, model->getUVs());
    pos = glGetAttribLocation(shader->programHandle(), "uv");
    glEnableVertexAttribArray(pos);
    glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
