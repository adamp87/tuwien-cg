/*! \file camera.cpp
 *  \brief %Camera handling
 *  \author Adam, Felix
*/

#include "scene.hpp"
#include "camera.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <iostream>

Camera::Camera(const std::string& name, Scene* scene, const glm::vec3& eye, float vAngle, float hAngle, float fov, float near, float far)
    : SceneObject(name, scene)
    , fov(fov)
    , farClip(far)
    , nearClip(near)
    , eye(eye)
	, moved(true)
	, initState(true)
{
    int width;
    int height;
    glfwGetWindowSize(scene->getWindow(), &width, &height);
    projectionMatrix = glm::perspective(fov, width / (float)height, nearClip, farClip);

    cursorX = width / 2.0;
    cursorY = height / 2.0;

    glm::vec3 direction(cos(vAngle) * sin(hAngle),
                        cos(vAngle) * cos(hAngle),
                        sin(vAngle));
    glm::vec3 right(sin(hAngle - 3.14f/2.0f),
                    cos(hAngle - 3.14f/2.0f),
                    0);
    glm::vec3 up = glm::cross(right, direction);

    viewMatrix = glm::lookAt(eye, eye + direction, up);
    modelMatrix = glm::inverse(viewMatrix);
}

Camera::Camera(const std::string& name, Scene* scene, glm::vec3& pos, glm::vec3& up, glm::vec3& eye, float fov, float near, float far)
    : SceneObject(name, scene)
    , fov(glm::degrees(fov))
    , farClip(far)
    , nearClip(near)
    , up(up)
    , eye(eye)
    , pos(pos)
	, moved(true)
	, initState(true)
{
    int width;
    int height;
    glfwGetWindowSize(scene->getWindow(), &width, &height);
    projectionMatrix = glm::perspective(glm::degrees(fov), width / (float)height, near, far);

    cursorX = width / 2.0;
    cursorY = height / 2.0;
}

void Camera::setViewMatrix(const glm::mat4& modelMatrix)
{
    this->modelMatrix = modelMatrix;
    glm::mat4 M = getGlobalModelMatrix();
	viewMatrix = glm::inverse(M);
}

void Camera::initializeLookupVector() {
	glm::mat4 M = getGlobalModelMatrix();
	eye = glm::vec3(M * glm::vec4(eye, 1));
	eye = -eye;
}

Camera::~Camera()
{

}

void Camera::draw() const
{

}

bool Camera::movement(){
	return moved;
}

void Camera::update(double deltaT)
{
    int width;
    int height;
    double xpos;
    double ypos;
    GLFWwindow* window = scene->getWindow();
    glfwGetCursorPos(window, &xpos, &ypos);
    glfwGetWindowSize(window, &width, &height);
	moved = true; //assume camera will be moved

    if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
    {//rotate
        glfwSetCursorPos(window, cursorX, cursorY);
        float verticalAngle = (cursorY - ypos);
        float horizontalAngle = (cursorX - xpos);

		glm::vec4 c_pos = getGlobalModelMatrix() * glm::vec4(0,0,0,1);
		//rotate
		eye = glm::rotate(eye, (float) deltaT * 5.0f * horizontalAngle, up);

		glm::vec3 rotateAround = glm::cross(eye, up);
		eye = glm::rotate(eye, (float) deltaT * 5.0f * verticalAngle, rotateAround);

        this->viewMatrix = glm::lookAt(glm::vec3(c_pos.x, c_pos.y, c_pos.z), glm::vec3(c_pos.x, c_pos.y, c_pos.z) + eye, up);

        //parentGlobal * local = global
        //local = parentGlobal^-1 * global
        this->modelMatrix = glm::mat4(1); //clear M, so getGlobalM() will give parent in global
        this->modelMatrix = glm::inverse(getGlobalModelMatrix()) * glm::inverse(viewMatrix);
#if 0
        viewMatrix = glm::inverse(modelMatrix);
        viewMatrix = glm::rotate(viewMatrix, (float)verticalAngle, glm::vec3(1, 0, 0));
        viewMatrix = glm::rotate(viewMatrix, (float)horizontalAngle, glm::vec3(0, 0, 1));
        setViewMatrix(glm::inverse(viewMatrix));
#endif
	} else if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
    {//move z
        glfwSetCursorPos(window, cursorX, cursorY);
        double delta = (cursorY - ypos);

        modelMatrix = glm::translate(modelMatrix, glm::vec3(0, 0, delta));
        setViewMatrix(modelMatrix);
    } else if(glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
    {//move up
        modelMatrix = glm::translate(modelMatrix, glm::vec3(0, 10 * deltaT, 0));
        setViewMatrix(modelMatrix);
    } else if(glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
    {//move down
        modelMatrix = glm::translate(modelMatrix, glm::vec3(0, -10 * deltaT, 0));
        setViewMatrix(modelMatrix);
    } else if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
    {//move left
        modelMatrix = glm::translate(modelMatrix, glm::vec3(-10 * deltaT, 0, 0));
        setViewMatrix(modelMatrix);
    } else if(glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
    {//move right
        modelMatrix = glm::translate(modelMatrix, glm::vec3(5 * deltaT, 0, 0));
        setViewMatrix(modelMatrix);
    } else if(glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
    {//zoom out
        fov += float(10 * deltaT);
        projectionMatrix = glm::perspective(fov, width / (float)height, nearClip, farClip);
    } else if(glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
    {//zoom in
        fov -= float(10 * deltaT);
        projectionMatrix = glm::perspective(fov, width / (float)height, nearClip, farClip);
    } else {//no camera action
        cursorX = xpos;
        cursorY = ypos;
		moved = false; // drop assumption
    }

	if (initState) { //this is needed for the first frame since otherwise there would be no movement at all
		moved = true;
		initState = false;
	}
}

bool Camera::animate(double time) {
#if 0
	bool ret = SceneObject::animate(time);
    setViewMatrix(modelMatrix);
    return ret;
#endif
	return true;
}
