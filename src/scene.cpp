/*! \file scene.cpp
 *  \brief Render Engine
 *  \author Adam
*/

#include "model.hpp"
#include "scene.hpp"
#include "camera.hpp"
#include "shader.hpp"
#include "texture.hpp"
#include "sceneObject.hpp"
#include "assimpLoader.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <algorithm>

Scene::Scene(GLFWwindow* window)
    : window(window)
{

}

Scene::~Scene()
{

}

void Scene::init(const std::string& scenePath)
{
    AssimpLoader loader(scenePath, this); (void)loader;
}

bool Scene::getObject(const std::string& name, std::shared_ptr<SceneObject>& object, size_t pos) const
{
    //More then one object is allowed with the same name.
    //Pos gives back the pos-th elementh. Indexed from 1..N
    size_t p = pos;
    object = rootNode;
    return getObjectRecursive(name, object, p);
}

bool Scene::getObjectRecursive(const std::string& name, std::shared_ptr<SceneObject>& object, size_t& pos) const
{
    if(object->getName() == name && --pos == 0)
        return true;

    size_t i = 0;
    std::shared_ptr<SceneObject> child;
    while(object->getChild(i++, child)) {
        if(getObjectRecursive(name, child, pos)) {
            object = child;
            return true;
        }
    }

    return false;
}

void Scene::clearRender()
{
    renderList.clear();
}

void Scene::changeRenderShader(const std::string& from, const std::string& to)
{
    Shader* f = getShader(from);
    Shader* t = getShader(to);
    for(auto it = renderList.begin(); it != renderList.end(); ++it) {
        std::shared_ptr<SceneObject>& obj = it->second;

        if(obj->getShader() == f)
            obj->setShader(t);
    }
}

void Scene::remRender(std::shared_ptr<SceneObject>& object)
{
    auto it = renderList.begin();
    while(it != renderList.end()) {
        if(it->second == object) {
            size_t idx = it->first;

            //shift all in group
            auto it_next = it; ++it_next;
            while(it_next != renderList.end()) {
                size_t idx_next = it_next->first;
                if(idx_next - idx != 1)
                    break;

                it->second = it_next->second;
                ++idx;
                ++it;
                ++it_next;
            }

            //remove last in group
            renderList.erase(it);
            it = renderList.begin();
        }
        ++it;
    }
}

void Scene::getRender(size_t idx, std::shared_ptr<SceneObject>& object) const
{
    //find idx
    object.reset();
    auto it = renderList.find(idx);
    if(it == renderList.end())
        return;

    object = it->second;
}

size_t Scene::pushRender(std::shared_ptr<SceneObject>& object, size_t idx)
{
    //increment idx until empty place
    auto it = renderList.lower_bound(idx);
    while(it != renderList.end() && it->first == idx) {
        ++it;
        ++idx;
    }

    //insert, return index
    renderList.insert(it, std::make_pair(idx, object));
    return idx;
}

size_t Scene::insertRender(std::shared_ptr<SceneObject>& object, size_t idx)
{
    auto last_idx = idx;
    auto it = renderList.lower_bound(idx);

    while(it != renderList.end() && it->first == last_idx) {
        ++it;
        ++last_idx;
    }

    while(last_idx != idx) {
        renderList[last_idx+1] = renderList[last_idx];
        --last_idx;
    }

    if(it->first == idx)
        renderList.erase(idx);

    //insert, return index
    renderList[idx] = object;
    return idx;
}

Shader* Scene::getShader(const std::string& name) const
{
    auto it = shaders.find(name);
    if(it == shaders.end())
        return nullptr; //shader not found
    return it->second.get();
}

void Scene::addShader(const std::string& name, std::unique_ptr<Shader>& shader) {
    shaders[name] = std::move(shader);
}
