/*! \file selectorBase.cpp
 *  \brief Mouse selection handling
 *  \author Adam
*/

#include "scene.hpp"
#include "model.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "selectorBase.hpp"

#include <iostream>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>

class SelectorBase::EffectObject : public SceneObject
{
public:
    EffectObject(std::shared_ptr<SceneObject> effectParent, unsigned int id)
        : SceneObject(effectParent)
        , id(id)
    {
        setShader(scene->getShader("selectorDraw"));
    }

    void draw() const {
        if(!model)
            return;

        shader->use();

        glm::mat4 M = getGlobalModelMatrix();
        glm::mat4 V = scene->getCamera()->getViewMatrix();
        glm::mat4 MV = V * M;
        glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;

        glUniform1ui(glGetUniformLocation(shader->programHandle(), "ID"), id);
        glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MVP"), 1, GL_FALSE, glm::value_ptr(MVP));

        glBindVertexArray(vao);
        glDrawElements(GL_TRIANGLES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    }

    void setShader(Shader* val) {
        shader = val;
        SceneObject::setShader(val);
    }

private:
    unsigned int id;
};

SelectorBase::SelectorBase(const std::string& name, Scene* scene)
    : SceneObject(name, scene)
{
    lastSelected = 0xFFFF;
    lastDistance = 0xFFFF;

    /// \todo, allow resize?
    glfwGetWindowSize(scene->getWindow(), &width, &height);

    // The framebuffer for the pickable objects
    glGenFramebuffers(1, &idBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, idBuffer);

    // Texture for the rendered pickable objects
    glGenTextures(1, &idTexture);
    glBindTexture(GL_TEXTURE_2D, idTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R16UI, width, height, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, idTexture, 0);

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

SelectorBase::~SelectorBase()
{
    glDeleteBuffers(1, &idBuffer);
    glDeleteTextures(1, &idTexture);
}

void SelectorBase::update(double)
{

}

void SelectorBase::draw() const
{
    glBindFramebuffer(GL_FRAMEBUFFER, idBuffer);

    glEnable(GL_DEPTH_TEST);
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glCullFace(GL_BACK);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);

    for(auto it = scene->begin(Scene::SentinelDrawPickable), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        effect = obj->getEffect("selectorDraw");
        if(effect) {
            effect->draw();
        } else {
            std::cout << "Internal error: No effect for object" << std::endl;
        }
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //TODO, if resize is handled, this could be deleted
    int globalWidth, globalHeight;
    glfwGetWindowSize(scene->getWindow(), &globalWidth, &globalHeight);
    glViewport(0, 0, globalWidth, globalHeight);

    // Always check that our framebuffer is ok
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "SelectorGPU: " << glewGetErrorString(glGetError()) << std::endl;
        return;
    }
}

void SelectorBase::reset()
{
    lastSelected = 0xFFFF;
    lastDistance = 0xFFFF;
    for(auto it = scene->begin(Scene::SentinelDrawPickable), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        effect = obj->getEffect("selectorDraw");
        if(!effect) {
            if(obj->getName() == "gpu") {
                /// \todo Add instanced effect.
                //std::unique_ptr<SceneObject> eff(new EffectObjectInstanced(obj));
                //obj->addEffect("selectorDraw", eff);
            } else {
                std::unique_ptr<SceneObject> eff(new EffectObject(obj, it->first));
                obj->addEffect("selectorDraw", eff);
            }
        }
    }
}
