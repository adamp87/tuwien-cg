/*! \file volumetricLight.cpp
 *  \brief Volumetric Lighting Effect
 *  \author Felix
*/

#include "volumetricLight.hpp"
#include "scene.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "texture.hpp"
#include "model.hpp"

#include <iostream>
#include <GLFW/glfw3.h>
#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>

class VolumetricLight::EffectObjectVolumetric : public SceneObject
{
public:
    EffectObjectVolumetric(Shader* shaderArg, std::shared_ptr<SceneObject> effectParent)
        : SceneObject(effectParent)
    {
        EffectObjectVolumetric::setShader(shaderArg);
    }

    void draw() const {
        if(!(shader && model && scene))
            return;
        
        shader->use();

        glm::mat4 M = getGlobalModelMatrix();
        glm::mat4 V = scene->getCamera()->getViewMatrix();
        glm::mat4 MV = V * M;
        glm::mat4 PV = scene->getCamera()->getProjectionMatrix() * V;
        glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;

        glm::vec4 inColor = glm::vec4(0,0,0,1.0);

        glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "PV_mat"), 1, GL_FALSE, glm::value_ptr(PV));
        glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));
        //TODO: note that this could be properly done in blender with ambient color 
        glUniform4fv(glGetUniformLocation(shader->programHandle(), "inColor"), 1, glm::value_ptr(inColor));

        glBindVertexArray(vao);
        glDrawElements(GL_TRIANGLES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
        //glDrawArrays(GL_TRIANGLES, 0, model->getIndexCount());
        glBindVertexArray(0);
    }

    void setShader(Shader* val) {
        shader = val;
        SceneObject::setShader(val);
    }
};

class VolumetricLight::EffectObjectVolumetricPost : public SceneObject
{
public:

    EffectObjectVolumetricPost(Shader* shaderArg, std::shared_ptr<SceneObject> effectParent)
        : SceneObject(effectParent)
    {
        EffectObjectVolumetricPost::setShader(shaderArg);
    }
    
    void draw() const {
        if(!(shader && model && scene))
            return;

        shader->use();

        std::shared_ptr<VolumetricLight> parentObj = scene->getVolumetricLight();

        glm::mat4 M = getGlobalModelMatrix();
        glm::mat4 V = scene->getCamera()->getViewMatrix();
        glm::mat4 MV = V * M;
        glm::mat4 PV = scene->getCamera()->getProjectionMatrix() * V;
        glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;

        glActiveTexture(GL_TEXTURE0 + 0);
        glBindTexture(GL_TEXTURE_2D, parentObj->getFrameBufferTexture());
        glUniform1i(glGetUniformLocation(shader->programHandle(), "frameSampler"), 0);

        glUniform2fv(glGetUniformLocation(shader->programHandle(), "ScreenLightPos"), 1, glm::value_ptr(parentObj->getScreenLightPos()));
        glUniform1f(glGetUniformLocation(shader->programHandle(), "exposure"), 1.0);
        glUniform1f(glGetUniformLocation(shader->programHandle(), "weight"), 0.5);
        glUniform1f(glGetUniformLocation(shader->programHandle(), "decay"), 0.5);
        glUniform1f(glGetUniformLocation(shader->programHandle(), "density"), 0.5);

        glBindVertexArray(vao);
        glDrawElements(GL_TRIANGLES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
        //glDrawArrays(GL_TRIANGLES, 0, model->getIndexCount());
        glBindVertexArray(0);
    }


    void setShader(Shader* val) {
        shader = val;
        SceneObject::setShader(val);
    }
};


VolumetricLight::VolumetricLight(const std::string& name, Scene* scene, Model* model, const glm::mat4& modelMatrix)
    : SceneObject(name,scene,model,modelMatrix) {
  
     std::vector<float> positions;
     std::vector<unsigned int> indices;
     std::vector<unsigned int> adjacentIndices;
     std::vector<float> normals;
     std::vector<float> uvs;
     float width = 1.0f;
     float height = 1.0f;

     glfwGetWindowSize(scene->getWindow(), &texWidth, &texHeight);
     glViewport(0, 0, texWidth, texHeight);

     Model::getQuadModel(positions, indices, uvs, width, height);
     quadModel = new Model(positions,indices,adjacentIndices,normals,uvs);

     glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
     
     // depth texture is needed for z-testing
     setUpDepthTexture();

     setUpColorTexture();

     glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);
     glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, colorTexture, 0);

     glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

VolumetricLight::~VolumetricLight() {
     
}

void VolumetricLight::setUpColorTexture() {

     glGenTextures(1, &colorTexture);
     glBindTexture(GL_TEXTURE_2D, colorTexture);
     
     // Set up texture mapping parameters      
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 
                      texWidth, 
                      texHeight, 
                      0, 
                      GL_RGBA, 
                      GL_FLOAT, 
                      0);
    
}

void VolumetricLight::setUpDepthTexture() {

     int width, height;
     glfwGetWindowSize(scene->getWindow(), &width, &height);
     glViewport(0, 0, width, height);

     glGenTextures(1, &depthTexture);
     glBindTexture(GL_TEXTURE_2D, depthTexture);

     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
     glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, texWidth, texHeight, 0,GL_DEPTH_COMPONENT, GL_FLOAT, 0);

}

void VolumetricLight::screenTest() {
     glm::mat4 M = getGlobalModelMatrix();
     glm::mat4 V = scene->getCamera()->getViewMatrix();
     glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * V * M;
     glm::vec4 position = MVP * glm::vec4(0,0,0,1);
     glm::vec3 screenSpace = glm::vec3(position.x / position.w, position.y / position.w, position.z / position.w);
     glm::vec3 minBorder = glm::vec3(-1,-1,-1);
     glm::vec3 maxBorder = glm::vec3(1,1,1);

     if(screenSpace.x > -1 && screenSpace.y > -1 && screenSpace.z > -1) {
         if(screenSpace.x < 1 && screenSpace.y < 1 && screenSpace.z < 1) {
             isInScreen = true;
             glm::mat4 biasMatrix(
                0.5, 0.0, 0.0, 0.0,
                0.0, 0.5, 0.0, 0.0,
                0.0, 0.0, 0.5, 0.0,
                0.5, 0.5, 0.5, 1.0
                );

             glm::mat4 biasMVP = biasMatrix * MVP;
             ScreenLightPos = biasMVP * glm::vec4(0,0,0,1);
             ScreenLightPos.x = ScreenLightPos.x / ScreenLightPos.w;
             ScreenLightPos.y = ScreenLightPos.y / ScreenLightPos.w;
             ScreenLightPos.z = ScreenLightPos.z / ScreenLightPos.w;
             return;
         }
     }
     isInScreen = false;
     return;
}

void VolumetricLight::reset() {
    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);
        if(this == obj.get())
            continue;

        effect = obj->getEffect("volumetricFirstPass");
        if(!effect) {
            std::unique_ptr<SceneObject> eff(new EffectObjectVolumetric(scene->getShader("volumetricFirstPass") ,obj));
            obj->addEffect("volumetricFirstPass", eff);
        }

        effect = 0;
        effect = obj->getEffect("volumetricPostProcess");
        if(!effect) {
            std::unique_ptr<SceneObject> eff(new EffectObjectVolumetricPost(scene->getShader("volumetricPostProcess"), obj));
            obj->addEffect("volumetricPostProcess", eff);
        }
    }
}

void VolumetricLight::draw() const {
    if(!(shader && model && scene))
        return;

    shader->use();

    glm::mat4 M = getGlobalModelMatrix();
    glm::mat4 V = scene->getCamera()->getViewMatrix();
    glm::mat4 MV = V * M;
    glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;
    glm::mat3 N = glm::mat3(glm::inverseTranspose(M));
    glm::mat4 PV = scene->getCamera()->getProjectionMatrix() * V;

    glm::vec4 McameraPosition = scene->getCamera()->getGlobalModelMatrix() * glm::vec4(0,0,0,1);

    if(model->getTexture()) {
        glActiveTexture(GL_TEXTURE0 + 0);
        glBindTexture(GL_TEXTURE_2D, model->getTexture()->getHandle());
        glUniform1i(glGetUniformLocation(shader->programHandle(), "colorTexture"), 0);
    }

    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "PV_mat"), 1, GL_FALSE, glm::value_ptr(PV));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));

    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
    //glDrawArrays(GL_TRIANGLES, 0, model->getIndexCount());
    glBindVertexArray(0);
}

void VolumetricLight::firstPass() {

    screenTest();
    if(!isInScreen)
        return;

    static const GLfloat zero[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    static const GLfloat one = 1.0f;
    static const GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0 };
    
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glDrawBuffers(1, draw_buffers);
    
    glClearBufferfv(GL_COLOR, 0, zero);
    glClearBufferfv(GL_DEPTH, 0, &one);
    
    glViewport(0, 0, texWidth, texHeight);   

    //TODO is called twice (second time in main)
    draw();

    //render into the colorbuffer
    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);
        if(this == obj.get())
            continue;

        effect = obj->getEffect("volumetricFirstPass");
        if(effect) {
            effect->draw();
        } else {
            std::cout << "Internal error: No effect for object" << std::endl;
        }
    }
    

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "Framebuffer incomplemte after rendering: " << glewGetErrorString(glGetError()) << std::endl;
        return;
    }

    //rebind default framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    int width, height;
    glfwGetWindowSize(scene->getWindow(), &width, &height);
    glViewport(0, 0, width, height);
}

void VolumetricLight::switchShader(Shader* val) {
    shader = val;

    glDeleteVertexArrays(1, &vao);
    glGenVertexArrays(1, &vao);

    glBindVertexArray(vao);

    GLint pos;
    glBindBuffer(GL_ARRAY_BUFFER, quadModel->getPosition());
    pos = glGetAttribLocation(shader->programHandle(), "position");
    if(pos != -1) {
        glEnableVertexAttribArray(pos);
        glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }

    if(model->getTexture()) {
        glBindBuffer(GL_ARRAY_BUFFER, quadModel->getUVs());
        pos = glGetAttribLocation(shader->programHandle(), "uv");
        if(pos != -1) {
            glEnableVertexAttribArray(pos);
            glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, 0, 0);
        }
    }
       
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadModel->getIndices());

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void VolumetricLight::postProcess() {

    if(!isInScreen)
        return;

    switchShader(scene->getShader("volumetricPostProcess"));

    //render quad here
    shader->use();

    //glm::mat4 M = getGlobalModelMatrix();
    //glm::mat4 V = scene->getCamera()->getViewMatrix();
    //glm::mat4 MV = V * M;
    //glm::mat4 PV = scene->getCamera()->getProjectionMatrix() * V;
    //glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;
    //
    glActiveTexture(GL_TEXTURE0 + 0);
    glBindTexture(GL_TEXTURE_2D, getFrameBufferTexture());
    glUniform1i(glGetUniformLocation(shader->programHandle(), "frameSampler"), 0);

    glUniform4fv(glGetUniformLocation(shader->programHandle(), "ScreenLightPos"), 1, glm::value_ptr(getScreenLightPos()));
    glUniform1f(glGetUniformLocation(shader->programHandle(), "exposure"), 0.002f);
    glUniform1f(glGetUniformLocation(shader->programHandle(), "weight"), 8.65f);
    glUniform1f(glGetUniformLocation(shader->programHandle(), "decay"), 1.0f);
    glUniform1f(glGetUniformLocation(shader->programHandle(), "density"), 1.0f);

    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    SceneObject::setShader(scene->getShader("volumetricFirstPassSun"));
}
