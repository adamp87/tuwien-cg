/*! \file cubeMap.cpp
 *  \brief %CubeMap effect
 *  \author Felix
*/

#include "cubeMap.hpp"
#include "light.hpp"
#include "model.hpp"
#include "scene.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "texture.hpp"
#include "sceneObject.hpp"

#include <iostream>
#include <GLFW/glfw3.h>
#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>

class CubeMap::EffectObject : public SceneObject
{
public:
    EffectObject(Shader* shaderArg, std::shared_ptr<SceneObject> effectParent)
        : SceneObject(effectParent)
    {
        EffectObject::setShader(shaderArg);
    }

    void draw() const {
        if(!(shader && model && scene))
            return;

        shader->use();
        std::shared_ptr<CubeMap> effectParent = scene->getCubeMap();

        glm::mat4 M = getGlobalModelMatrix();
        glm::mat4 V = scene->getCamera()->getViewMatrix();
        glm::mat4 MV = V * M;
        glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;
        glm::mat3 N = glm::mat3(glm::inverseTranspose(M));

        glm::vec3 MlightPosition = scene->getLight()->getPosition();
        glm::vec4 MglobalPosition = M * glm::vec4(0,0,0,1);
        //glm::vec4 McameraPosition = scene->getCamera()->getGlobalModelMatrix() * glm::vec4(0,0,0,1);

        if(model->getTexture()) {
            glActiveTexture(GL_TEXTURE0 + 0);
            glBindTexture(GL_TEXTURE_2D, model->getTexture()->getHandle());
            glUniform1i(glGetUniformLocation(shader->programHandle(), "colorTexture"), 0);
        }

        glUniform1f (glGetUniformLocation(shader->programHandle(), "shininess"), model->getShininess());
        glUniform4fv(glGetUniformLocation(shader->programHandle(), "ambientColor"), 1, glm::value_ptr(model->getAmbient()));
        glUniform4fv(glGetUniformLocation(shader->programHandle(), "diffuseColor"), 1, glm::value_ptr(model->getDiffuse()));
        glUniform4fv(glGetUniformLocation(shader->programHandle(), "specularColor"), 1, glm::value_ptr(model->getSpecular()));
        glUniform3fv(glGetUniformLocation(shader->programHandle(), "MlightPosition"), 1, glm::value_ptr(MlightPosition));
        glUniform4fv(glGetUniformLocation(shader->programHandle(), "McameraPosition"), 1, glm::value_ptr(MglobalPosition));

        glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));
        glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MVP"), 1, GL_FALSE, glm::value_ptr(MVP));
        glUniformMatrix3fv(glGetUniformLocation(shader->programHandle(), "N"), 1, GL_FALSE, glm::value_ptr(N));

        std::vector<std::string> names;
        names.push_back("cm_posx");
        names.push_back("cm_negx");
        names.push_back("cm_posy");
        names.push_back("cm_negy");
        names.push_back("cm_posz");
        names.push_back("cm_negz");
        int i=0;
        for(auto it = effectParent->viewMatrices.begin(); it != effectParent->viewMatrices.end(); ++it,i++) {
            GLuint uniloc = glGetUniformLocation(shader->programHandle(), names[i].data());
            glUniformMatrix4fv(uniloc, 1, GL_FALSE, glm::value_ptr(*it));
        }

        glBindVertexArray(vao);
        glDrawElements(GL_TRIANGLES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
        //glDrawArrays(GL_TRIANGLES, 0, model->getIndexCount());
        glBindVertexArray(0);
    }

    void setShader(Shader* val) {
        shader = val;
        SceneObject::setShader(val);
    }
};

CubeMap::CubeMap(const std::string& name, Scene* scene, Model* model, const glm::mat4& modelMatrix)
    : SceneObject(name,scene,model,modelMatrix) {

     cubeTexWidth = 512;
     cubeTexHeight = 512;
     renderPasses = 0;
     ///\todo FIXTHIS
     //norender = true;

     cubeFaces[0] = GL_TEXTURE_CUBE_MAP_POSITIVE_X;
     cubeFaces[1] = GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
     cubeFaces[2] = GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
     cubeFaces[3] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
     cubeFaces[4] = GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
     cubeFaces[5] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;

     // Create and bind an FBO
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
     
     // depth texture is needed for z-testing
     setUpDepthTexture();
     // color texture for rendering to the dynamic cubemap
     setUpColorTexture();
     glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
     glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);
     glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, cubeTexture, 0);

     glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
     glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

CubeMap::~CubeMap() {
    glDeleteTextures(1, &depthTexture);
    glDeleteTextures(1, &cubeTexture);
    glDeleteFramebuffers(1, &frameBuffer);
}

void CubeMap::reset() {
    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        effect = obj->getEffect("CubeMap");
        if(!effect) {
            if(obj->getModel()->getTexture() == 0) {
                std::unique_ptr<SceneObject> eff(new EffectObject(scene->getShader("CMColor"), obj));
                obj->addEffect("CubeMap", eff);
            } else {
                std::unique_ptr<SceneObject> eff(new EffectObject(scene->getShader("CMTexture"), obj));
                obj->addEffect("CubeMap", eff);
            }
        }
    }
}

void CubeMap::draw() const {
    if(!(shader && model && scene))
        return;

    shader->use();

    glm::mat4 M = getGlobalModelMatrix();
    glm::mat4 V = scene->getCamera()->getViewMatrix();
    glm::mat4 MV = V * M;
    glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;
    glm::mat3 N = glm::mat3(glm::inverseTranspose(M));
    glm::mat4 PV = scene->getCamera()->getProjectionMatrix() * V;

    glm::vec4 McameraPosition = scene->getCamera()->getGlobalModelMatrix() * glm::vec4(0,0,0,1);

    glActiveTexture(GL_TEXTURE0 + 0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTexture);
    glUniform1i(glGetUniformLocation(shader->programHandle(), "cubeTexture"), 0);
    glUniform4fv(glGetUniformLocation(shader->programHandle(), "McameraPosition"), 1, glm::value_ptr(McameraPosition));

    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "PV_mat"), 1, GL_FALSE, glm::value_ptr(PV));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MVP"), 1, GL_FALSE, glm::value_ptr(MVP));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MV"), 1, GL_FALSE, glm::value_ptr(MV));
    glUniformMatrix3fv(glGetUniformLocation(shader->programHandle(), "N"), 1, GL_FALSE, glm::value_ptr(N));

    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
    //glDrawArrays(GL_TRIANGLES, 0, model->getIndexCount());
    glBindVertexArray(0);
}

void CubeMap::setUpColorTexture() {

     glGenTextures(1, &cubeTexture);
     glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTexture);

     // Set up texture mapping parameters      
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
     glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

     // Load Cube Map images
     for(int face = 0; face < 6; face++) {        
        // Load this faces texture map
         glTexImage2D(cubeFaces[face], 
                      0, 
                      GL_RGBA8, 
                      cubeTexWidth, 
                      cubeTexHeight, 
                      0, 
                      GL_RGBA, 
                      GL_FLOAT, 
                      0);
     }
}

void CubeMap::setUpDepthTexture() {

     glGenTextures(1, &depthTexture);
     glBindTexture(GL_TEXTURE_CUBE_MAP, depthTexture);

     // Set up texture mapping parameters      
     //glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     //glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

     // Load Cube Map images
     for(int face = 0; face < 6; face++) {        
        // Load this faces texture map
         glTexImage2D(cubeFaces[face], 
                      0, 
                      GL_DEPTH_COMPONENT24, 
                      cubeTexWidth, 
                      cubeTexHeight, 
                      0, 
                      GL_DEPTH_COMPONENT, 
                      GL_FLOAT, 
                      0);
     }
}

void CubeMap::renderToCubeMap() {

    /*renderPasses = renderPasses == 2 ? 0 : renderPasses+1;
    if(renderPasses != 0)
        return;*/

    //iterate over all sceneObjects and render them into the 6 faces.
    static const GLfloat zero[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    static const GLfloat one = 1.0f;
    static const GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0 };
    
    //reset view matrices if position has changed
    setViewMatrices();

    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glDrawBuffers(1, draw_buffers);
    
    glClearBufferfv(GL_COLOR, 0, zero);
    glClearBufferfv(GL_DEPTH, 0, &one);
    
    glViewport(0, 0, cubeTexWidth, cubeTexHeight);   

    //render into the cubemap faces
    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        effect = obj->getEffect("CubeMap");
        if(effect) {
            //TODO: by setting correct shader, this check is not needed
            //if(obj->getModel()->getTexture() != 0) {
            ///\todo FIXTHIS
            //if(!obj->doNotRender())
                    effect->draw();
            //}
        } else {
            std::cout << "Internal error: No effect for object" << std::endl;
        }
    }
//    for(auto it = scene->begin(); it != scene->end(); ++it) {
//        std::shared_ptr<SceneObject> obj = it->get();
//        if(!obj->excludeFromPass) {
//            if(obj->getModel() != 0 && obj->getModel()->getTexture() != 0) {
//                obj->renderToCubeMap(scene->getShader("CMTexture"), viewMatrices, position);
//            } else if(obj->getModel()) {
//                //obj->renderToCubeMap(scene->getShader("CMColor"), viewMatrices);
//            }
//        }
//    }

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "Framebuffer incomplemte after rendering: " << glewGetErrorString(glGetError()) << std::endl;
        return;
    }

    //rebind default framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    int width, height;
    glfwGetWindowSize(scene->getWindow(), &width, &height);
    glViewport(0, 0, width, height);

}

void CubeMap::setViewMatrices() {

        viewMatrices.clear();

        //pos_x
        viewMatrices.push_back(glm::mat4(0,0,-1,0,
                                    0,-1,0,0,
                                    -1,0,0,0,
                                    0,0,0,1));
        //neg_x
        viewMatrices.push_back(glm::mat4(0,0,1,0,
                                    0,-1,0,0,
                                    1,0,0,0,
                                    0,0,0,1));
        //neg_y
        viewMatrices.push_back(glm::mat4(1,0,0,0,
                                    0,0,-1,0,
                                    0,+1,0,0,
                                    0,0,0,1));
        //pos_y
        viewMatrices.push_back(glm::mat4(1,0,0,0,
                                    0,0,1,0,
                                    0,-1,0,0,
                                    0,0,0,1));
        //pos_z
        viewMatrices.push_back(glm::mat4(1,0,0,0,
                                    0,-1,0,0,
                                    0,0,-1,0,
                                    0,0,0,1));
        //neg_z
        viewMatrices.push_back(glm::mat4(-1,0,0,0,
                                    0,-1,0,0,
                                    0,0,1,0,
                                    0,0,0,1));

        glm::mat4 M = getGlobalModelMatrix();
        position = M * glm::vec4(0,0,0,1);
        glm::mat4 T = glm::translate(glm::mat4(1),glm::vec3(-position));
        glm::mat4 P = glm::perspective(90.0f, cubeTexWidth / (float)cubeTexHeight, 0.1f, 100.0f);
        for(int i=0;i<6;i++) {
            viewMatrices[i] = P * (viewMatrices[i] * T);
        }
    }
