/*! \file tank.cpp
 *  \brief %Tank handling for SteelWorms
 *  \author Adam
*/

#include "tank.hpp"
#include "scene.hpp"
#include "shader.hpp"
#include "gameLogic.hpp"
#include "particleSystemCPU.hpp"
#include "particleSystemGPU.hpp"

#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

Tank::Tank(const std::string& name, Scene* scene, Model* model, const glm::mat4& modelMatrix)
    : SceneObject(name, scene, model, modelMatrix)
    , hp(1)
    , active(false)
    , lastCPU(0)
    , lastGPU(0)
{

}

Tank::~Tank()
{

}

void Tank::reset()
{
    /// \todo getObject should be improved to start from this node
    std::shared_ptr<SceneObject> node;
    scene->getObject(name, node);
    trackL = node; //default
    trackR = node; //default

    scene->getObject("gun", node);
    gun = node;
    shootOrigin = node; //default

    scene->getObject("Turret", node);
    turret = node;

    if(scene->getObject("trackL", node))
        trackL = node;

    if(scene->getObject("trackR", node))
        trackR = node;

    if(scene->getObject("shoot", node))
        shootOrigin = node;

    std::shared_ptr<SceneObject> psCPUobj;
    scene->getObject("cpu", psCPUobj);
    psCPU = std::dynamic_pointer_cast<ParticleSystemCPU>(psCPUobj);

    std::shared_ptr<SceneObject> psGPUobj;
    scene->getObject("gpu", psGPUobj);
    psGPU = std::dynamic_pointer_cast<ParticleSystemGPU>(psGPUobj);

    std::shared_ptr<SceneObject> gameObj;
    scene->getObject("GameLogic", gameObj);
    gameLogic = std::dynamic_pointer_cast<GameLogic>(gameObj);

    scene->remRender(trackL);
    scene->remRender(trackR);
}

void Tank::update(double deltaT)
{
    if(!active || gameLogic == nullptr)
        return;

    if(glfwGetKey(scene->getWindow(), GLFW_KEY_W) == GLFW_PRESS)
    {//move forward
        float deltaX = static_cast<float>(-1.0 * deltaT);
        modelMatrix = glm::translate(modelMatrix, glm::vec3(deltaX, 0.0f, 0.0f));
        glm::vec4 pos = modelMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0);
        pos.z = gameLogic->getTerrain()->getHeight(pos.x, pos.y) - pos.z;
        modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, pos.z, 0.0f));

        pos = glm::vec4(0.0f, 0.0f,  0.35f, 1.0f);
        pos = modelMatrix * pos;
        pos.z = gameLogic->getTerrain()->getHeight(pos.x, pos.y);
        psCPU->push(glm::vec3(pos));

        pos = glm::vec4(0.0f, 0.0f, -0.35f, 1.0f);
        pos = modelMatrix * pos;
        pos.z = gameLogic->getTerrain()->getHeight(pos.x, pos.y);
        psCPU->push(glm::vec3(pos));
    } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_S) == GLFW_PRESS)
    {//move back
        float deltaX = static_cast<float>(+1.0 * deltaT);
        modelMatrix = glm::translate(modelMatrix, glm::vec3(deltaX, 0.0f, 0.0f));
        glm::vec4 pos = modelMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0);
        pos.z = gameLogic->getTerrain()->getHeight(pos.x, pos.y) - pos.z;
        modelMatrix = glm::translate(modelMatrix, glm::vec3(0.0f, pos.z, 0.0f));

        pos = glm::vec4(0.0f, 0.0f,  0.35f, 1.0f);
        pos = modelMatrix * pos;
        pos.z = gameLogic->getTerrain()->getHeight(pos.x, pos.y);
        psCPU->push(glm::vec3(pos));

        pos = glm::vec4(0.0f, 0.0f, -0.35f, 1.0f);
        pos = modelMatrix * pos;
        pos.z = gameLogic->getTerrain()->getHeight(pos.x, pos.y);
        psCPU->push(glm::vec3(pos));
    }
    if(glfwGetKey(scene->getWindow(), GLFW_KEY_A) == GLFW_PRESS)
    {//turn left
        float delta = static_cast<float>(50.0 * deltaT);
        modelMatrix = glm::rotate(modelMatrix, delta, glm::vec3(0.0f, 1.0f, 0.0f));
    } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_D) == GLFW_PRESS)
    {//turn right
        float delta = static_cast<float>(-50.0 * deltaT);
        modelMatrix = glm::rotate(modelMatrix, delta, glm::vec3(0.0f, 1.0f, 0.0f));
    }

    SceneObject::update(deltaT);
}

bool Tank::animate(double time)
{
    //handle restart
    if(time < lastCPU)
        lastCPU = time;
    if(time < lastGPU)
        lastGPU = time;

    if(0.1 < time - lastCPU) {
        glm::vec4 posL = trackL->getGlobalModelMatrix() * glm::vec4(0, 0, 0, 1);
        psCPU->push(glm::vec3(posL));

        glm::vec4 posR = trackR->getGlobalModelMatrix() * glm::vec4(0, 0, 0, 1);
        psCPU->push(glm::vec3(posR));

        lastCPU = time;
    }

    if(0.2 < time - lastGPU) {
        ParticleSystemGPU::NewParticle p;
        p.pos = glm::vec3(getShootOrigin() * glm::vec4(0, 0, 0, 1));
        p.dir = glm::vec3(getShootOrigin() * glm::vec4(0, 0, -1000, 0));
        p.atr = glm::vec3(16, 0.8, 0); //power, angle
        psGPU->push(p);

        lastGPU = time;
    }

    return SceneObject::animate(time);
}

glm::mat4 Tank::getShootOrigin() const
{
    return shootOrigin->getGlobalModelMatrix();
}

/////////
// GUN //
/////////

Tank::Gun::Gun(const std::string& name, Scene* scene, Model* model, const glm::mat4& modelMatrix)
    : SceneObject(name, scene, model, modelMatrix)
    , lastAnimate(0)
{

}

Tank::Gun::~Gun()
{

}

bool Tank::Gun::animate(double time)
{
    //handle restart
    if(time < lastAnimate)
        lastAnimate = time;

    float delta = static_cast<float>(time - lastAnimate);
    modelMatrix = glm::rotate(modelMatrix, delta * 10, glm::vec3(0, 1, 0));

    lastAnimate = time;
    return SceneObject::animate(time);
}

void Tank::Gun::update(double deltaT)
{
    if(glfwGetKey(scene->getWindow(), GLFW_KEY_I) == GLFW_PRESS)
    {//tilt up
        glm::vec4 pos = modelMatrix * glm::vec4(1.0, 1.0, 1.0, 1.0);
        if(0.5 < pos.y) {
            float deltaZ = static_cast<float>(-25.0 * deltaT);
            modelMatrix = glm::rotate(modelMatrix, deltaZ, glm::vec3(0.0f, 0.0f, 1.0f));
        }
    } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_K) == GLFW_PRESS)
    {//tilt down
        glm::vec4 pos = modelMatrix * glm::vec4(1.0, 1.0, 1.0, 1.0);
        if(pos.y < 1.0) {
            float deltaZ = static_cast<float>(25.0 * deltaT);
            modelMatrix = glm::rotate(modelMatrix, deltaZ, glm::vec3(0.0f, 0.0f, 1.0f));
        }
    }

    SceneObject::update(deltaT);
}

////////////
// TURRET //
////////////

Tank::Turret::Turret(const std::string& name, Scene* scene, Model* model, const glm::mat4& modelMatrix)
    : SceneObject(name, scene, model, modelMatrix)
{

}

Tank::Turret::~Turret()
{

}

void Tank::Turret::update(double deltaT)
{
    if(glfwGetKey(scene->getWindow(), GLFW_KEY_J) == GLFW_PRESS)
    {//rotate left
        float delta = static_cast<float>(50.0 * deltaT);
        modelMatrix = glm::rotate(modelMatrix, delta, glm::vec3(0.0f, 1.0f, 0.0f));
    } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_L) == GLFW_PRESS)
    {//rotate right
        float delta = static_cast<float>(-50.0 * deltaT);
        modelMatrix = glm::rotate(modelMatrix, delta, glm::vec3(0.0f, 1.0f, 0.0f));
    }

    SceneObject::update(deltaT);
}
