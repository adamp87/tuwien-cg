/*! \file sceneObject.cpp
 *  \brief Render Engine Object
 *  \author Adam
*/

#include "model.hpp"
#include "scene.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "texture.hpp"
#include "shadowMap.hpp"
#include "shadowOmni.hpp"
#include "sceneObject.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <algorithm>

SceneObject::SceneObject(std::shared_ptr<SceneObject>& effectParent, const glm::mat4& modelMatrix)
    : scene(effectParent->scene)
    , model(effectParent->model)
    , shader(0)
    , modelMatrix(modelMatrix)
    , animIDX(0)
{
    parent = effectParent;
    glGenVertexArrays(1, &vao);
}

SceneObject::SceneObject(const std::string& name, Scene* scene, Model* model, const glm::mat4& modelMatrix)
    : scene(scene)
    , model(model)
    , shader(0)
    , modelMatrix(modelMatrix)
    , name(name)
    , animIDX(0)
{
    glGenVertexArrays(1, &vao);
}

SceneObject::~SceneObject()
{
    glDeleteVertexArrays(1, &vao);
}

void SceneObject::draw() const
{
    if(!(shader && model && scene))
        return;

    shader->use();

    glm::mat4 M = getGlobalModelMatrix();
    glm::mat4 V = scene->getCamera()->getViewMatrix();
    glm::mat4 MV = V * M;
    glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;
    glm::mat3 N = glm::mat3(glm::inverseTranspose(M));
    glm::mat4 VP = scene->getCamera()->getProjectionMatrix() * V;

    glm::vec4 McameraPosition = scene->getCamera()->getGlobalModelMatrix() * glm::vec4(0,0,0,1);

    if(model->getTexture()) {
        glActiveTexture(GL_TEXTURE0 + 0);
        glBindTexture(GL_TEXTURE_2D, model->getTexture()->getHandle());
        glUniform1i(glGetUniformLocation(shader->programHandle(), "colorTexture"), 0);
    }

    glUniform1f (glGetUniformLocation(shader->programHandle(), "shininess"), model->getShininess());
    glUniform4fv(glGetUniformLocation(shader->programHandle(), "ambientColor"), 1, glm::value_ptr(model->getAmbient()));
    glUniform4fv(glGetUniformLocation(shader->programHandle(), "diffuseColor"), 1, glm::value_ptr(model->getDiffuse()));
    glUniform4fv(glGetUniformLocation(shader->programHandle(), "specularColor"), 1, glm::value_ptr(model->getSpecular()));
    glUniform4fv(glGetUniformLocation(shader->programHandle(), "McameraPosition"), 1, glm::value_ptr(McameraPosition));

    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "PV_mat"), 1, GL_FALSE, glm::value_ptr(VP));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "V"), 1, GL_FALSE, glm::value_ptr(V));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MV"), 1, GL_FALSE, glm::value_ptr(MV));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MVP"), 1, GL_FALSE, glm::value_ptr(MVP));
    glUniformMatrix3fv(glGetUniformLocation(shader->programHandle(), "N"), 1, GL_FALSE, glm::value_ptr(N));

#ifdef USE_OMNI_SHADOW
    ShadowOmni* shadow = (ShadowOmni*)scene->getLight().get();
    glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_CUBE_MAP, shadow->getDepthTexture());
    glUniform1i(glGetUniformLocation(shader->programHandle(), "cm_z_tex"), 1);

    glm::vec2 near_far = glm::vec2(scene->getCamera()->getNearClip(),scene->getCamera()->getFarClip());
    glUniform2fv(glGetUniformLocation(shader->programHandle(), "near_far"), 1, glm::value_ptr(near_far));
#elif defined USE_SHADOW_MAP
    ShadowMap* shadow = (ShadowMap*)scene->getLight().get();
    glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_2D, shadow->getDepthTexture());
    glUniform1i(glGetUniformLocation(shader->programHandle(), "shadowMap"), 1);
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "depthBiasMVP"), 1, GL_FALSE, glm::value_ptr(shadow->getDepthBiasMVP()));
#endif

    glUniform3fv(glGetUniformLocation(shader->programHandle(), "lightPower"), 1, glm::value_ptr(scene->getLight()->getPower()));
    glUniform3fv(glGetUniformLocation(shader->programHandle(), "lightAmbient"), 1, glm::value_ptr(scene->getLight()->getAmbient()));
    glUniform3fv(glGetUniformLocation(shader->programHandle(), "lightDiffuse"), 1, glm::value_ptr(scene->getLight()->getDiffuse()));
    glUniform3fv(glGetUniformLocation(shader->programHandle(), "lightSpecular"), 1, glm::value_ptr(scene->getLight()->getSpecular()));
    glUniform3fv(glGetUniformLocation(shader->programHandle(), "MlightPosition"), 1, glm::value_ptr(scene->getLight()->getPosition()));

    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

void SceneObject::reset()
{

}

void SceneObject::update(double)
{

}

bool SceneObject::animate(double time) {
    while(animIDX < animation.size()) {
        if(animation[animIDX].first < time) {
            modelMatrix = animation[animIDX++].second;
        } else
            return true;
    }
    return false; //animation finished
}

void SceneObject::setShader(Shader* val)
{
    shader = val;

    glDeleteVertexArrays(1, &vao);
    glGenVertexArrays(1, &vao);

    glBindVertexArray(vao);

    GLint pos;
    glBindBuffer(GL_ARRAY_BUFFER, model->getPosition());
    pos = glGetAttribLocation(shader->programHandle(), "position");
    if(pos != -1) {
        glEnableVertexAttribArray(pos);
        glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }

    glBindBuffer(GL_ARRAY_BUFFER, model->getNormals());
    pos = glGetAttribLocation(shader->programHandle(), "normal");
    if(pos != -1) {
        glEnableVertexAttribArray(pos);
        glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }

    if(model->getTexture()) {
        glBindBuffer(GL_ARRAY_BUFFER, model->getUVs());
        pos = glGetAttribLocation(shader->programHandle(), "uv");
        if(pos != -1) {
            glEnableVertexAttribArray(pos);
            glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, 0, 0);
        }
    }
       
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->getIndices());

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

bool SceneObject::getChild(size_t idx, std::shared_ptr<SceneObject>& child) const
{
    //index is larger
    if(childs.size() <= idx)
        return false;

    //return child
    child = childs[idx];
    return true;
}

bool SceneObject::delChild(size_t idx)
{
    //index is larger
    if(childs.size() <= idx)
        return false;

    //remove parent
    childs[idx]->parent.reset();

    //remove from child list
    childs.erase(childs.begin() + idx);
    return true;
}

bool SceneObject::addChild(std::shared_ptr<SceneObject>& child)
{
    assert(child->parent.expired());

    childs.push_back(child);
    child->parent = shared_from_this();

    return true;
}

bool SceneObject::setAnimation(const Animation& anim) {
    if(!animation.empty())
        return false; //already has animation

    animation = anim;
    return true;
}

void SceneObject::setAnimationTime(double time) {
    animIDX = 0;
    while(animIDX < animation.size() && animation[animIDX].first < time) {
        modelMatrix = animation[animIDX++].second;
    }
}

bool SceneObject::remEffect(const std::string& name) {
    auto it = effectChilds.find(name);
    if(it == effectChilds.end())
        return false;

    effectChilds.erase(it);
    return true;
}

SceneObject* SceneObject::getEffect(const std::string& name) const {
    auto it = effectChilds.find(name);
    if(it == effectChilds.end())
        return 0;

    return it->second.get();
}

bool SceneObject::addEffect(const std::string& name, std::unique_ptr<SceneObject>& effect) {
    auto it = effectChilds.find(name);
    if(it != effectChilds.end())
        return false; //has already child with name

    effectChilds[name] = std::move(effect);
    return true;
}
