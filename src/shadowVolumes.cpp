/*! \file shadowVolumes.cpp
 *  \brief OmniVolumes effect
 *  \author Felix
*/

#include "scene.hpp"
#include "model.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "texture.hpp"
#include "shadowVolumes.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <iostream>

class ShadowVolumes::EffectObjectAmbientPass : public SceneObject
{
public:
    //glm::mat4 M;
    //glm::mat4 V;
    //glm::mat4 MV;
    //glm::mat4 MVP;
    //glm::mat4 PV;
    //glm::vec3 MlightPosition;
    //glm::vec4 lpos;
    //Shader* sAmbient;
    //Shader* sSv;

    EffectObjectAmbientPass(Shader* shaderArg, std::shared_ptr<SceneObject> effectParent)
        : SceneObject(effectParent)
    {
        setShader(shaderArg);
    }

    void draw() {
        if(!(shader && model && scene))
            return;
        shader->use();

        glm::mat4 M = getGlobalModelMatrix();
        glm::mat4 V = scene->getCamera()->getViewMatrix();
        glm::mat4 MV = V * M;
        glm::mat4 PV = scene->getCamera()->getProjectionMatrix() * V;
        glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;

        glm::vec3 MlightPosition = scene->getLight()->getPosition();
        glm::vec4 lpos = glm::vec4(MlightPosition.x,MlightPosition.y,MlightPosition.z,1.0);

        if(model->getTexture()) {
            glActiveTexture(GL_TEXTURE0 + 0);
            glBindTexture(GL_TEXTURE_2D, model->getTexture()->getHandle());
            glUniform1i(glGetUniformLocation(shader->programHandle(), "colorTexture"), 0);
        }

        glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MVP"), 1, GL_FALSE, glm::value_ptr(MVP));
        //TODO: note that this could be properly done in blender with ambient color 
        glUniform4fv(glGetUniformLocation(shader->programHandle(), "inColor"), 1, glm::value_ptr(model->getDiffuse()));

        glBindVertexArray(vao);
        glDrawElements(GL_TRIANGLES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    }
};

class ShadowVolumes::EffectObjectSVPass : public SceneObject
{
public:
    EffectObjectSVPass(Shader* shaderArg, std::shared_ptr<SceneObject> effectParent)
        : SceneObject(effectParent)
    {
        setShader(shaderArg);
    }

    void draw() const {
        if(!(shader && model && scene))
            return;

       shader->use();

       glm::mat4 M = getGlobalModelMatrix();
        glm::mat4 V = scene->getCamera()->getViewMatrix();
        glm::mat4 MV = V * M;
        glm::mat4 PV = scene->getCamera()->getProjectionMatrix() * V;
        glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;

        glm::vec3 MlightPosition = scene->getLight()->getPosition();
        glm::vec4 lpos = glm::vec4(MlightPosition.x,MlightPosition.y,MlightPosition.z,1.0);

       glUniform1i(glGetUniformLocation(shader->programHandle(), "zpass"), 0); 

       glUniform4fv(glGetUniformLocation(shader->programHandle(), "l_pos"), 1, glm::value_ptr(lpos));
       glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));
       glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MVP"), 1, GL_FALSE, glm::value_ptr(MVP));
       glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "PV_mat"), 1, GL_FALSE, glm::value_ptr(PV));
        
       glBindVertexArray(vao);
       glDrawElements(GL_TRIANGLES_ADJACENCY, model->getIndexAdjCount(), GL_UNSIGNED_INT, 0);
       glBindVertexArray(0);
    }

    void setShader(Shader* val)
    {
        shader = val;

        glDeleteVertexArrays(1, &vao);
        glGenVertexArrays(1, &vao);

        glBindVertexArray(vao);

        GLint pos;
        glBindBuffer(GL_ARRAY_BUFFER, model->getPosition());
        pos = glGetAttribLocation(shader->programHandle(), "position");
        if(pos != -1) {
            glEnableVertexAttribArray(pos);
            glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);
        }
       
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->getIndicesAdj());

        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    Shader* getShader() {
        return shader;
    }
};

ShadowVolumes::ShadowVolumes(const std::string& name, Scene* scene, Model* model, Shader* shader, const glm::mat4& modelMatrix)
    : Light(name, scene, model, modelMatrix)
{
    ShadowVolumes::setShader(shader);
}

ShadowVolumes::~ShadowVolumes()
{

}

void ShadowVolumes::reset()
{
    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        effect = obj->getEffect("ambientPass");
        if(!effect) {
            if(obj->getModel()->getTexture() == 0) {
                std::unique_ptr<SceneObject> ambientEff(new EffectObjectAmbientPass(scene->getShader("SVambientColor"), obj));
                obj->addEffect("ambientPass", ambientEff);
            } else {
                std::unique_ptr<SceneObject> ambientEff(new EffectObjectAmbientPass(scene->getShader("SVambientTexture"), obj));
                obj->addEffect("ambientPass", ambientEff);
            }
        }
        effect = 0;
        effect = obj->getEffect("svPass");
        if(!effect) {
            std::unique_ptr<SceneObject> eff(new EffectObjectSVPass(scene->getShader("SV"), obj));
            obj->addEffect("svPass", eff);
        }

    }
}

void ShadowVolumes::update(double)
{

}

void ShadowVolumes::setUpAmbientPass() {
     glClearDepth(1.0); // clear to far plane distance in DC
     glClearStencil(0);
     // enable respective buffers for writing
     // (in this case a "clear");
     glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
     glDepthMask(GL_TRUE);
     glStencilMask(0xFF);
     // perform actual clear-buffers-operation
     glClear( GL_COLOR_BUFFER_BIT |
        GL_DEPTH_BUFFER_BIT |
        GL_STENCIL_BUFFER_BIT );
     glDisable(GL_CULL_FACE);
     //stencil test only needed when we construct the shadow volumes
     glDisable(GL_STENCIL_TEST);
     //we just perform depth testing in the ambient pass
     glEnable(GL_DEPTH_TEST);
     glDepthFunc(GL_LESS);
     //make sure that we actually write to the depth buffer
     glDepthMask(GL_TRUE);
     //not needed
     glDisable(GL_BLEND);
     glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
}

void ShadowVolumes::ambientPass() {
            
    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        ///\todo FIXTHIS
        //if(obj->doNotRender())
        //    continue;

        effect = obj->getEffect("ambientPass");

        if(effect) {
             effect->draw();
        } else {
            std::cout << "Internal error: No effect for object" << std::endl;
        }
    }

}

void ShadowVolumes::SVPass() {

    //make sure that the volume is clamped so that stencil buffer can be updated correctly
    glEnable(GL_DEPTH_CLAMP);
    //all polygons are considered! especially those on the silhouette need their back facing neigbors
    glDisable(GL_CULL_FACE);
    glEnable(GL_STENCIL_TEST);
    /*if(zpass) {
    glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_KEEP, GL_INCR_WRAP);
    glStencilOpSeparate(GL_BACK, GL_KEEP, GL_KEEP, GL_DECR_WRAP);
    }*/
    //else { //zfail
    glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);
    glStencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
    //}
    glStencilFunc(GL_ALWAYS, 0, 0xFF);
    glStencilMask( 0xFF );
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDepthMask(GL_FALSE); // do not write to z-buffer
    glDisable(GL_BLEND);
    //do not write anything, just update the stencil buffer in this pass
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    
    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        effect = obj->getEffect("svPass");
        if(effect) {
            effect->draw();
        } else {
            std::cout << "Internal error: No effect for object" << std::endl;
        }
    }
    
    //glEnable(GL_CULL_FACE);
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP); //works well, but:
    //glStencilOp(GL_KEEP,GL_KEEP,GL_INCR);
    // The INCR zpass stencil operation avoids double
    // blending of lighting contributions in usually quite rare
    // circumstance when two fragments alias to exact same pixel
    // location and depth value
    //write only if the stencil value is exactly zero
    glStencilFunc(GL_EQUAL, 0, 0xFF);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //needed for ambient light
    //not needed 
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_ONE, GL_ONE);
    //glDisable(GL_CULL_FACE);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
}

void ShadowVolumes::draw() const
{
    //called in two pass outside
}

void ShadowVolumes::setShader(Shader* val)
{
    shader = val;
}
