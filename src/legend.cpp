/*! \file legend.cpp
 *  \brief Mouse legend
 *  \author Felix
*/

#ifndef NOLEGEND

#include "legend.hpp"
#include "scene.hpp"
#include "bubbleCursor.hpp"

#include <iostream>
#include <functional>
#include <algorithm>
#include <sstream>

#include <glm/gtc/type_ptr.hpp>

//void printMatrix(float x, float y, float sx, float sy) {
//	std::cout << std::endl;
//	std::cout << std::endl;
//	std::cout << std::endl;
//	std::cout << std::endl;
//	std::cout << "[ " << x << ", " << y << ", " << "0, " << "0 ]" << std::endl;
//	std::cout << "[ " << x + sx << ", " << y << ", " << "1, " << "0 ]" << std::endl;
//	std::cout << "[ " << x << ", " << y - sy << ", " << "0, " << "1 ]" << std::endl;
//	std::cout << "[ " << x + sx << ", " << y - sy << ", " << "1, " << "1 ]" << std::endl;
//}

//TODO autofocus of window
Legend::Legend(Scene* _scene, MouseSemantics *_mouse, Texture *_texture, float upperleftx, float upperlefty, float xstep, float ystep)
    : SceneObject("legend",_scene)
	, mouse(_mouse)
    , background(new Background(_scene, _texture))
	, upperleftx(std::max(upperleftx,0.0f))
	, upperlefty(std::max(upperlefty, 0.0f))
	, xreset(std::max(upperleftx, 0.0f))
	, yreset(std::max(upperlefty, 0.0f))
	, xstep(xstep)
	, ystep(std::max(ystep,STANDARD_TEXTSIZE))
	, maxXposText(-1.0f)
	, maxYposText(1.0f)
	, offsetX(5)
	, offsetY(5)
	, frameCount(0)
	, accumulatedTime(0)
	, FPS(1)
{	
	if (FT_Init_FreeType(&ft)) {
		throw std::runtime_error("Error while loading freetype library");
	}

	if (FT_New_Face(ft, "../fonts/console.ttf", 0, &face)) {
		throw std::runtime_error("Creation of face failed");
	}

	FT_Set_Pixel_Sizes(face, 0, 15);
	prepareText();
	background->setShader(scene->getShader("textShaderTextured"));
#if 0
	glGenTextures(1, &charTex);
	glBindTexture(GL_TEXTURE_2D, charTex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
#endif
}

Legend::~Legend() {
	delete background;
}

void Legend::calcFPS(double deltaT) {
		++frameCount;
		accumulatedTime += deltaT;
		if (0.5 < accumulatedTime) {
			FPS = frameCount * 2;
			frameCount = 0;
			accumulatedTime = 0;
		}
}

//load some ASCII characters
void Legend::prepareText() {
	char currChar = ' ';
	GLuint currTexHandle;
	for (currChar; currChar != '~'; currChar++) {

		if (FT_Load_Char(face, currChar, FT_LOAD_RENDER)) {
			continue;
		}

		glGenTextures(1, &currTexHandle);
		glBindTexture(GL_TEXTURE_2D, currTexHandle);

		FT_GlyphSlot g = face->glyph;

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, g->bitmap.width, g->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, g->bitmap.buffer);

		FT_Pos ax = g->advance.x >> 6;
		FT_Pos ay = g->advance.y >> 6;

		unsigned int bw = g->bitmap.width;
		unsigned int bh = g->bitmap.rows;

		FT_Int bl = g->bitmap_left;
		FT_Int bt = g->bitmap_top;

		Glyph gsave = Glyph(ax, ay, bw, bh, bl, bt);

		charTexHandleMap.insert(std::pair<char, GLuint>(currChar,currTexHandle));
		charGlyphMap.insert(std::pair<char, Glyph>(currChar, gsave));

		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

void Legend::setShader(Shader *_shader) {
	shader = _shader;

	glDeleteVertexArrays(1, &vao);
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vboPositions);
	glBindBuffer(GL_ARRAY_BUFFER, vboPositions);
	GLint pos;
	pos = glGetAttribLocation(shader->programHandle(), "position");
	if (pos != -1) {
		glEnableVertexAttribArray(pos);
		glVertexAttribPointer(pos, 4, GL_FLOAT, GL_FALSE, 0, 0);
	}

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Legend::update(double deltaT) {
	calcFPS(deltaT);
}

void Legend::renderText(const char *text, float x, float y, float sx, float sy) {
	const char *p;

	for (p = text; *p; p++) {
		if (charGlyphMap.find(*p) == charGlyphMap.end())
			continue;

		Glyph g = charGlyphMap.find(*p)->second;

		glActiveTexture(GL_TEXTURE0 + 0);
		glBindTexture(GL_TEXTURE_2D, charTexHandleMap.find(*p)->second);
		glUniform1i(glGetUniformLocation(shader->programHandle(), "charTexture"), 0);

		float x2 = x + g.bl * sx;
		float y2 = -y - g.bt * sy;
		float w = g.bw * sx;
		float h = g.bh * sy;

		GLfloat box[4][4] = {
			{ x2, -y2, 0, 0 },
			{ x2 + w, -y2, 1, 0 },
			{ x2, -y2 - h, 0, 1 },
			{ x2 + w, -y2 - h, 1, 1 },
		};

		if (maxXposText < (x + w)) {
			maxXposText = x + w;
		}

		//TODO: could be improved but no significant performance loss
		glBufferData(GL_ARRAY_BUFFER, sizeof (GLfloat) * 16, box, GL_DYNAMIC_DRAW);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		x += g.ax * sx;
		y += g.ay * sy;
	}
}

void Legend::updatePosition(bool reset) {
	if (reset) {
		upperleftx = xreset;
		upperlefty = yreset;
	}
	else {
		upperleftx += xstep;
		upperlefty += ystep;
	}
}

void Legend::draw() {

	int width, height;
	glfwGetWindowSize(scene->getWindow(), &width, &height);

	float sx = 2.0 / width;
	float sy = 2.0 / height;

	updatePosition(false);
	maxYposText = 1 - upperlefty * sy;
	updatePosition(true);
	background->renderBackground(-1 + (upperleftx - offsetX) * sx, 1 - (upperlefty - offsetY) * sy, maxXposText + offsetX * sx, maxYposText + offsetY * sy);

	shader->use();

	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glDisable(GL_DEPTH_TEST);

	GLfloat black[4] = { 0, 0, 0, 1 };
	glUniform4fv(glGetUniformLocation(shader->programHandle(), "color"), 1, black);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vboPositions);

#if 0
	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, charTex);
	glUniform1i(glGetUniformLocation(shader->programHandle(), "charTexture"), 0);
#endif 

	std::ostringstream ss;

	{
		updatePosition(false);
		ss << "Cursor speed: " << mouse->getSpeed();
		renderText(ss.str().data(), -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
		ss.str("");
		updatePosition(false);
		renderText("", -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
	}

	{
		updatePosition(false);
		ss << "Scale factor: " << mouse->getScaleFactor();
		renderText(ss.str().data(), -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
		updatePosition(false);
		renderText(" J -> increase", -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
		ss.str("");
		updatePosition(false);
		renderText(" K -> decrease", -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
		ss.str("");
		updatePosition(false);
		renderText("", -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
	}

	{
		updatePosition(false);
		ss << "Scale funciton: " << mouse->scaleFunction();
		renderText(ss.str().data(), -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
		updatePosition(false);
		renderText(" F10 -> toggle function", -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
		ss.str("");
		updatePosition(false);
		renderText("", -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
	}

	{
		updatePosition(false);
		ss << "Selection mode: " << selectorText;
		renderText(ss.str().data(), -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
		updatePosition(false);
		renderText(" F11 -> toggle mode", -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
		ss.str("");
		updatePosition(false);
		renderText("", -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
	}

	{
		updatePosition(false);
		std::string enabledString = "disabled";
		if (scene->getBubbleCursor()->drawingEnabled()) {
			enabledString = "enabled";
		}
		ss << "Random Bubble Cursors: " << enabledString;
		renderText(ss.str().data(), -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
		ss.str("");
		updatePosition(false);
		renderText(" B -> enable/disable", -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
		updatePosition(false);
		renderText("", -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
	}

	{
		updatePosition(false);
		float distancePix = mouse->getDistance();
		std::string distance;
		if (distancePix == 65535) {
			ss << "Distance: Out Of Range";
		}
		else {
			ss << "Distance: " << mouse->getDistance() << " pixels";
		}
		renderText(ss.str().data(), -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
		ss.str("");
		updatePosition(false);
		renderText("", -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
	}

	{
		updatePosition(false);
		ss << "FPS: " << FPS;
		renderText(ss.str().data(), -1 + upperleftx * sx, 1 - upperlefty * sy, sx, sy);
	}

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	
}


//Background of legend
Legend::Background::Background(Scene *_scene, Texture *_texture)
	: SceneObject("legend", _scene)
	, texture(_texture)
{
}

Legend::Background::~Background() {

}

void Legend::Background::setShader(Shader *_shader) {
	shader = _shader;

	//TODO try w/o this but don't know if possible since glDrawArrays... is needed
	float position[] = { 1.0f, 1.0f };

	glDeleteVertexArrays(1, &vao);
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vboPositions);
	glBindBuffer(GL_ARRAY_BUFFER, vboPositions);
	//glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float), position, GL_STATIC_DRAW);
	GLint pos;
	pos = glGetAttribLocation(shader->programHandle(), "position");
	if (pos != -1) {
		glEnableVertexAttribArray(pos);
		glVertexAttribPointer(pos, 4, GL_FLOAT, GL_FALSE, 0, 0);
	}

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Legend::Background::draw() {

}

void Legend::Background::renderBackground(float x, float y, float sx, float sy) {

	shader->use();

	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLfloat black[4] = { 0, 0, 0, 1 };
	glUniform4fv(glGetUniformLocation(shader->programHandle(), "color"), 1, black);

	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, texture->getHandle());
	glUniform1i(glGetUniformLocation(shader->programHandle(), "charTexture"), 0);

	/*
	float x2 = x + g.bl * sx;
	float y2 = -y - g.bt * sy;
	float w = g.bw * sx;
	float h = g.bh * sy;

	GLfloat box[4][4] = {
		{ x2, -y2, 0, 0 },
		{ x2 + w, -y2, 1, 0 },
		{ x2, -y2 - h, 0, 1 },
		{ x2 + w, -y2 - h, 1, 1 },
	};*/

	GLfloat box[4][4] = {
		{ x, y, 0, 0 },
		{ sx, y, 1, 0 },
		{ x, sy, 0, 1 },
		{ sx, sy, 1, 1 },
	};

	//printMatrix(x,y,sx,sy);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vboPositions);
	//TODO: slow, improvement needed! preprocessing!
	glBufferData(GL_ARRAY_BUFFER, sizeof (GLfloat) * 16, box, GL_DYNAMIC_DRAW);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//Old code
#if 0
	shader->use();

	int width, height;
	glfwGetWindowSize(scene->getWindow(), &width, &height);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_POINT_SPRITE);
	glEnable(GL_PROGRAM_POINT_SIZE);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, texture->getHandle());
	glUniform1i(glGetUniformLocation(shader->programHandle(), "cursorTexture"), 0);
	glUniform1f(glGetUniformLocation(shader->programHandle(), "size"), 1.0f);
	//glUniform2fv(glGetUniformLocation(shader->programHandle(), "mousePos"), 1, glm::value_ptr(glm::vec2(x / width, y / height)));
	glUniform2fv(glGetUniformLocation(shader->programHandle(), "mousePos"), 1, glm::value_ptr(glm::vec2(0.5 ,0.5)));

	glBindVertexArray(vao);
	glDrawArrays(GL_POINTS, 0, 1);
	glBindVertexArray(0);

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
#endif
}

#endif //NOLEGEND
