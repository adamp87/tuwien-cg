#include "debug.hpp"
#include "sun.hpp"
#include "gun.hpp"
#include "tank.hpp"
#include "model.hpp"
#include "scene.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "turret.hpp"
#include "texture.hpp"
#include "terrain.hpp"
#include "gameLogic.hpp"
#include "wheelTrack.hpp"
#include "sceneObject.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <stdexcept>

#define APP_NAME "SteelWorms 27.06.2014"

int main(int argc, char** argv)
{
    const int width = 800;
    const int height = 600;

    GLFWwindow* window = NULL;
    std::unique_ptr<Debug> debug;
    std::unique_ptr<Scene> scene;
    std::unique_ptr<GameLogic> gameLogic;
    std::vector<std::unique_ptr<Texture> > textures;

    std::unique_ptr<Shader> shadowShader;
    std::unique_ptr<Shader> genericShader;
    std::unique_ptr<Shader> particleShader;

    std::unique_ptr<Model> cubeModel;
    std::unique_ptr<Model> waterModel;
    std::unique_ptr<Model> sphereModel;
    std::unique_ptr<Model> terrainModel;
    std::vector<std::unique_ptr<Model> > tankModels;

    std::shared_ptr<Sun> sun;
    std::shared_ptr<Tank> tank1;
    std::shared_ptr<Tank> tank2;
    std::shared_ptr<Camera> camera;
    std::shared_ptr<Terrain> water;
    std::shared_ptr<Terrain> terrain;
    std::shared_ptr<WheelTrack> wheelTrack;

    try {
        {//window creation
            std::string mode;
            if(argc == 2)
                mode.assign(argv[1]);

            if(!glfwInit())
                throw std::runtime_error("Error on glfwInit");

            GLFWmonitor* monitor = NULL;
            if(mode.empty() || mode == "fauto") {
                monitor = glfwGetPrimaryMonitor();
                auto screen = glfwGetVideoMode(monitor);
                int refresh = 60;
                if(mode == "fauto")
                    refresh = screen->refreshRate;
                glfwWindowHint(GLFW_REFRESH_RATE, refresh);
            }

            if(!(window = glfwCreateWindow(width, height, APP_NAME, monitor, NULL)))
                throw std::runtime_error("Error on glfwCreateWindow");
            glfwMakeContextCurrent(window);
            if(glewInit() != GLEW_OK)
                throw std::runtime_error("Error on glewInit");
            if(!GLEW_VERSION_3_3)
                throw std::runtime_error("OpenGL 3.3 is not supported");
        }

        scene.reset(new Scene());
        shadowShader.reset(new Shader("../shader/shadow.vert", "../shader/shadow.frag"));
        genericShader.reset(new Shader("../shader/generic.vert", "../shader/generic.frag"));
        particleShader.reset(new Shader("../shader/particle.vert", "../shader/particle.frag"));
        //cubeShader.reset(new Shader("../shader/cube.vert", "../shader/cube.frag"));
        //cubeColorShader.reset(new Shader("../shader/cube.vert", "../shader/cubeColor.frag"));

        {//get cube and sphere model
            std::vector<float> uvs;
            std::vector<float> normals;
            std::vector<float> positions;
            std::vector<unsigned int> indices;
            Model::getCubeModel(positions, indices, normals, uvs);
            cubeModel.reset(new Model(positions, indices, normals, uvs));
            Model::getSphereModel(positions, indices, normals, uvs);
            std::unique_ptr<Texture> texture(new Texture("../texture/Sun.bmp"));
            sphereModel.reset(new Model(positions, indices, normals, uvs, texture.get()));
            textures.push_back(std::move(texture));
        }

        {//get terrain model
            size_t width;
            size_t height;
            size_t bitPerPixel;
            std::vector<float> uvs;
            std::vector<float> normals;
            std::vector<float> heights;
            std::vector<float> positions;
            std::vector<unsigned int> indices;
            std::vector<unsigned char> pixel_data;
            Texture::loadBMP("../texture/heightmap.bmp", pixel_data, width, height, bitPerPixel);
            Model::getTerrainModel(width, height, bitPerPixel, 0.1f, pixel_data, heights, positions, indices, normals, uvs);
            terrainModel.reset(new Model(positions, indices, normals, uvs));
            terrain.reset(new Terrain(width, height, 0.1f, heights, scene.get(), terrainModel.get(), genericShader.get()));
            Texture::loadBMP("../texture/watermap.bmp", pixel_data, width, height, bitPerPixel);
            Model::getTerrainModel(width, height, bitPerPixel, 0.1f, pixel_data, heights, positions, indices, normals, uvs);
            std::unique_ptr<Texture> texture(new Texture("../texture/water.bmp"));
            waterModel.reset(new Model(positions, indices, normals, uvs, texture.get()));
            textures.push_back(std::move(texture));
            water.reset(new Terrain(width, height, 0.1f, heights, scene.get(), waterModel.get(), genericShader.get(), SceneObject::Water));
        }

        {//build up tanks
            std::shared_ptr<Gun> gun1;
            std::shared_ptr<Gun> gun2;
            std::shared_ptr<Turret> turret1;
            std::shared_ptr<Turret> turret2;

            glm::vec3 player1Pos(-3.0f, terrain->getHeight(-3.0f, 0.0f), 0.0f);
            glm::vec3 player2Pos(+3.0f, terrain->getHeight(+3.0f, 0.0f), 0.0f);

            glm::mat4 tank1M = glm::mat4(1.0f);
            tank1M = glm::rotate(tank1M, 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
            tank1M = glm::translate(tank1M, player1Pos);
            tank1M = glm::rotate(tank1M, 180.0f, glm::vec3(0.0f, 1.0f, 0.0f));

            glm::mat4 tank2M = glm::mat4(1.0f);
            tank2M = glm::rotate(tank2M, 90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
            tank2M = glm::translate(tank2M, player2Pos);

            size_t lastPos = 0;
            Model::loadModels("../texture/Tiger/gun.obj", tankModels);
            gun1.reset(new Gun(scene.get(), tankModels[lastPos].get(), genericShader.get(), glm::mat4(1.0f)));
            gun2.reset(new Gun(scene.get(), tankModels[lastPos].get(), genericShader.get(), glm::mat4(1.0f)));
            for(size_t i = lastPos + 1; i < tankModels.size(); ++i) {
                std::shared_ptr<SceneObject> obj1(new SceneObject(scene.get(), tankModels[i].get(), genericShader.get()));
                std::shared_ptr<SceneObject> obj2(new SceneObject(scene.get(), tankModels[i].get(), genericShader.get()));
                gun1->addChild(obj1);
                gun2->addChild(obj2);
            }
            lastPos = tankModels.size();

            Model::loadModels("../texture/Tiger/turret.obj", tankModels);
            turret1.reset(new Turret(scene.get(), tankModels[lastPos].get(), genericShader.get(), glm::mat4(1.0f)));
            turret2.reset(new Turret(scene.get(), tankModels[lastPos].get(), genericShader.get(), glm::mat4(1.0f)));
            for(size_t i = lastPos + 1; i < tankModels.size(); ++i) {
                std::shared_ptr<SceneObject> obj1(new SceneObject(scene.get(), tankModels[i].get(), genericShader.get()));
                std::shared_ptr<SceneObject> obj2(new SceneObject(scene.get(), tankModels[i].get(), genericShader.get()));
                turret1->addChild(obj1);
                turret2->addChild(obj2);
            }
            lastPos = tankModels.size();

            Model::loadModels("../texture/Tiger/body.obj", tankModels);
            tank1.reset(new Tank(gun1, turret1, scene.get(), tankModels[lastPos].get(), genericShader.get(), tank1M));
            tank2.reset(new Tank(gun2, turret2, scene.get(), tankModels[lastPos].get(), genericShader.get(), tank2M));
            for(size_t i = lastPos + 1; i < tankModels.size(); ++i) {
                std::shared_ptr<SceneObject> obj1(new SceneObject(scene.get(), tankModels[i].get(), genericShader.get()));
                std::shared_ptr<SceneObject> obj2(new SceneObject(scene.get(), tankModels[i].get(), genericShader.get()));
                obj1->setShader(genericShader.get());
                obj2->setShader(genericShader.get());
                tank1->addChild(obj1);
                tank2->addChild(obj2);
            }

            turret1->addChild(std::dynamic_pointer_cast<SceneObject>(gun1));
            turret2->addChild(std::dynamic_pointer_cast<SceneObject>(gun2));

        }

        camera.reset(new Camera(window));
        sun.reset(new Sun(scene.get(), 0, shadowShader.get()));
        wheelTrack.reset(new WheelTrack(1000, scene.get(), 0, particleShader.get()));
        gameLogic.reset(new GameLogic(scene.get(), sphereModel.get(), genericShader.get()));

        gameLogic->addTank(tank1);
        gameLogic->addTank(tank2);
        tank1->setActive(true);

        scene->setSun(sun.get());
        scene->setWindow(window);
        scene->setWater(water.get());
        scene->setTerrain(terrain.get());
        scene->setCamera(camera);
        scene->setTrack(wheelTrack.get());
        scene->pushObject(sun); //front, shade must be computed
        scene->pushObject(camera);
        scene->pushObject(terrain);
        scene->pushObject(tank1);
        scene->pushObject(tank2);
        scene->pushObject(wheelTrack); //back, transparency, draw track underwater
        scene->pushObject(water); //back, transparency

        debug.reset(new Debug(scene.get(), APP_NAME, textures));
    } catch(std::exception& ex) {
        std::cout << "Error on init: " << ex.what() << std::endl;
        glfwTerminate();
        return -1;
    } catch(...) {
        std::cout << "Error on init: Unknown exception received" << std::endl;
        glfwTerminate();
        return -1;
    }

    bool running = true;
    double deltaT = 0.0;
    double lastTime = 0.0;

    glEnable(GL_DEPTH_TEST);

    // Enable blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glViewport(0, 0, width, height);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    while (running && !glfwWindowShouldClose(window)) {
        double time = glfwGetTime();
        deltaT = time - lastTime;
        lastTime = time;

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        debug->update(deltaT);
        gameLogic->update(deltaT);
        scene->update(deltaT);
        scene->draw();

        auto error = glGetError();
        if(error != GL_NO_ERROR)
            std::cerr << "OpenGL Error: " << glewGetErrorString(error) << std::endl;

        glfwSwapBuffers(window);
        glfwPollEvents();
        running = !((glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) || gameLogic->isGameFinished());
    }

    if(gameLogic->isGameFinished())
        std::cout << "Winner is: " << gameLogic->getWinner() + 1 << std::endl;

    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}

