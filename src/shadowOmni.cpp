/*! \file shadowOmni.cpp
 *  \brief OmniShadow effect
 *  \author Felix, Adam
*/

#include "scene.hpp"
#include "model.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "texture.hpp"
#include "shadowOmni.hpp"
#include "particleSystemGPU.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <iostream>

const int sizeX = 1024;
const int sizeY = 1024;

class ShadowOmni::EffectObjectCube : public SceneObject
{
public:
    EffectObjectCube(std::shared_ptr<SceneObject> effectParent)
        : SceneObject(effectParent)
    {
        setShader(scene->getShader("omniShadow"));
    }

    void draw() const {
        if(!(shader && model && scene))
            return;

        shader->use();
        ShadowOmni* effectParent = (ShadowOmni*)scene->getLight().get();

        glm::mat4 M = getGlobalModelMatrix();
        glm::mat4 V = scene->getCamera()->getViewMatrix();
        glm::mat4 MV = V * M;
        glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;
        glm::mat3 N = glm::mat3(glm::inverseTranspose(M));

        glm::vec4 MlightPosition = glm::vec4(scene->getLight()->getPosition(),1.0);
        glm::vec2 near_far = glm::vec2(scene->getCamera()->getNearClip(),scene->getCamera()->getFarClip());

        glUniform2fv(glGetUniformLocation(shader->programHandle(), "near_far"), 1, glm::value_ptr(near_far));
        glUniform4fv(glGetUniformLocation(shader->programHandle(), "MlightPosition"), 1, glm::value_ptr(MlightPosition));
        glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));

        std::vector<std::string> names;
        names.push_back("cm_posx");
        names.push_back("cm_negx");
        names.push_back("cm_posy");
        names.push_back("cm_negy");
        names.push_back("cm_posz");
        names.push_back("cm_negz");
        int i=0;
        for(auto it = effectParent->viewMatrices.begin(); it != effectParent->viewMatrices.end(); ++it,i++) {
            GLuint uniloc = glGetUniformLocation(shader->programHandle(), names[i].data());
            glUniformMatrix4fv(uniloc, 1, GL_FALSE, glm::value_ptr(*it));
        }

        glBindVertexArray(vao);
        glDrawElements(GL_TRIANGLES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
        //glDrawArrays(GL_TRIANGLES, 0, model->getIndexCount());
        glBindVertexArray(0);
    }

    void setShader(Shader* val) {
        shader = val;
        SceneObject::setShader(val);
    }
};

class ShadowOmni::EffectObjectCubeInstanced : public SceneObject
{
public:
    EffectObjectCubeInstanced(std::shared_ptr<SceneObject> effectParent)
        : SceneObject(effectParent)
    {
        setShader(scene->getShader("particleTessGPUOmni"));
    }

    void draw() const {
        if(!(shader && model && scene))
            return;

        std::shared_ptr<ParticleSystemGPU> gpu = std::dynamic_pointer_cast<ParticleSystemGPU>(parent.lock());

        shader->use();
        ShadowOmni* effectParent = (ShadowOmni*)scene->getLight().get();

        glm::mat4 M = getGlobalModelMatrix();
        glm::mat4 V = scene->getCamera()->getViewMatrix();
        glm::mat4 MV = V * M;
        glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;
        glm::mat3 N = glm::mat3(glm::inverseTranspose(M));

        glm::vec4 MlightPosition = glm::vec4(scene->getLight()->getPosition(),1.0);
        glm::vec2 near_far = glm::vec2(scene->getCamera()->getNearClip(),scene->getCamera()->getFarClip());

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_3D, gpu->getDispTexture());
        glUniform1i(glGetUniformLocation(shader->programHandle(), "dispTexture"), 0);

        glUniform1ui(glGetUniformLocation(shader->programHandle(), "tessLevelIn"), gpu->getTessLevelIn());
        glUniform1ui(glGetUniformLocation(shader->programHandle(), "tessLevelOut"), gpu->getTessLevelOut());

        glUniform2fv(glGetUniformLocation(shader->programHandle(), "near_far"), 1, glm::value_ptr(near_far));
        glUniform4fv(glGetUniformLocation(shader->programHandle(), "MlightPosition"), 1, glm::value_ptr(MlightPosition));
        glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));

        std::vector<std::string> names;
        names.push_back("cm_posx");
        names.push_back("cm_negx");
        names.push_back("cm_posy");
        names.push_back("cm_negy");
        names.push_back("cm_posz");
        names.push_back("cm_negz");
        int i=0;
        for(auto it = effectParent->viewMatrices.begin(); it != effectParent->viewMatrices.end(); ++it,i++) {
            GLuint uniloc = glGetUniformLocation(shader->programHandle(), names[i].data());
            glUniformMatrix4fv(uniloc, 1, GL_FALSE, glm::value_ptr(*it));
        }

        glBindVertexArray(vao);
        glVertexAttribDivisor(glGetAttribLocation(shader->programHandle(), "particle"), 1); // positions : one per quad (its center) -> 1
        glVertexAttribDivisor(glGetAttribLocation(shader->programHandle(), "position"), 0); // particles vertices : always reuse the same 4 vertices -> 0
        glVertexAttribDivisor(glGetAttribLocation(shader->programHandle(), "normal"), 0); // particles vertices : always reuse the same 4 vertices -> 0
        glVertexAttribDivisor(glGetAttribLocation(shader->programHandle(), "uv"), 0); // particles vertices : always reuse the same 4 vertices -> 0
        glDrawArraysInstanced(GL_PATCHES, 0, model->getIndexCount(), gpu->getActiveCount());
        glBindVertexArray(0);
    }

    void setShader(Shader* val) {
        shader = val;
        std::shared_ptr<ParticleSystemGPU> gpu = std::dynamic_pointer_cast<ParticleSystemGPU>(parent.lock());

        glDeleteVertexArrays(1, &vao);
        glGenVertexArrays(1, &vao);

        glBindVertexArray(vao);

        GLint pos;
        glBindBuffer(GL_ARRAY_BUFFER, model->getPosition());
        pos = glGetAttribLocation(shader->programHandle(), "position");
        glEnableVertexAttribArray(pos);
        glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, gpu->getParticlePosVBO());
        pos = glGetAttribLocation(shader->programHandle(), "particle");
        glEnableVertexAttribArray(pos);
        glVertexAttribPointer(pos, 4, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, model->getNormals());
        pos = glGetAttribLocation(shader->programHandle(), "normal");
        glEnableVertexAttribArray(pos);
        glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, model->getUVs());
        pos = glGetAttribLocation(shader->programHandle(), "uv");
        glEnableVertexAttribArray(pos);
        glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, 0, 0);

        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
};

ShadowOmni::ShadowOmni(const std::string& name, Scene* scene, Model* model, Shader* shader, const glm::mat4& modelMatrix)
    : Light(name, scene, model, modelMatrix)
{
    ShadowOmni::setShader(shader);

     cubeFaces[0] = GL_TEXTURE_CUBE_MAP_POSITIVE_X;
     cubeFaces[1] = GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
     cubeFaces[2] = GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
     cubeFaces[3] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
     cubeFaces[4] = GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
     cubeFaces[5] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;

     // Create and bind an FBO
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
     
     // depth texture is needed for z-testing
     setUpDepthTexture();
     glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

     glDrawBuffer(GL_NONE);
     glReadBuffer(GL_NONE);

     glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
     glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

ShadowOmni::~ShadowOmni()
{
    glDeleteTextures(1, &depthTexture);
    glDeleteFramebuffers(1, &frameBuffer);
}

void ShadowOmni::setUpDepthTexture()
{
     glGenTextures(1, &depthTexture);
     glBindTexture(GL_TEXTURE_CUBE_MAP, depthTexture);

     // Set up texture mapping parameters      
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
     glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

     // Load Cube Map images
     for(int face = 0; face < 6; face++) {        
        // Load this faces texture map
         glTexImage2D(cubeFaces[face], 
                      0, 
                      GL_DEPTH_COMPONENT32, 
                      sizeX, 
                      sizeY, 
                      0, 
                      GL_DEPTH_COMPONENT, 
                      GL_FLOAT, 
                      0);
     }

}

void ShadowOmni::reset()
{
    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        effect = obj->getEffect("omniShadow");
        if(!effect) {
            if(obj->getName() == "gpu") {
                std::unique_ptr<SceneObject> eff(new EffectObjectCubeInstanced(obj));
                obj->addEffect("omniShadow", eff);
            } else {
                std::unique_ptr<SceneObject> eff(new EffectObjectCube(obj));
                obj->addEffect("omniShadow", eff);
            }
        }
    }
}

void ShadowOmni::update(double)
{
    setViewMatrices();
}

void ShadowOmni::draw() const
{
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_FRONT);
    glClear( GL_DEPTH_BUFFER_BIT );
    glViewport(0, 0, sizeX, sizeY);

    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        effect = obj->getEffect("omniShadow");
        if(effect) {
            effect->draw();
        } else {
            std::cout << "Internal error: No effect for object" << std::endl;
        }
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    int width, height;
    glfwGetWindowSize(scene->getWindow(), &width, &height);
    glViewport(0, 0, width, height);

    // Always check that our framebuffer is ok
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "Sun: " << glewGetErrorString(glGetError()) << std::endl;
        return;
    }
    //glCullFace(GL_BACK);
}

void ShadowOmni::setShader(Shader* val)
{
    shader = val;
}

void ShadowOmni::setViewMatrices() {

        viewMatrices.clear();

        //pos_x
        viewMatrices.push_back(glm::mat4(0,0,-1,0,
                                    0,-1,0,0,
                                    -1,0,0,0,
                                    0,0,0,1));
        //neg_x
        viewMatrices.push_back(glm::mat4(0,0,1,0,
                                    0,-1,0,0,
                                    1,0,0,0,
                                    0,0,0,1));

        //neg_y
        viewMatrices.push_back(glm::mat4(1,0,0,0,
                                    0,0,-1,0,
                                    0,+1,0,0,
                                    0,0,0,1));

        //pos_y
        viewMatrices.push_back(glm::mat4(1,0,0,0,
                                    0,0,1,0,
                                    0,-1,0,0,
                                    0,0,0,1));

        //pos_z
        viewMatrices.push_back(glm::mat4(1,0,0,0,
                                    0,-1,0,0,
                                    0,0,-1,0,
                                    0,0,0,1));
        //neg_z
        viewMatrices.push_back(glm::mat4(-1,0,0,0,
                                    0,-1,0,0,
                                    0,0,1,0,
                                    0,0,0,1));

        glm::mat4 M = getGlobalModelMatrix();
        glm::vec4 position = M * glm::vec4(0,0,0,1);
        glm::mat4 T = glm::translate(glm::mat4(1),glm::vec3(-position));
        glm::mat4 P = glm::perspective(90.0f, sizeX / (float)sizeY, 0.1f, 100.0f);
        for(int i=0;i<6;i++) {
            viewMatrices[i] = P * (viewMatrices[i] * T);
        }
    }
