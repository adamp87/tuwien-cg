/*! \file bubbleCursor.cpp
 *  \brief Display circles to show bubble cursor
 *  \author Felix
*/

#include "bubbleCursor.hpp"
#include "selectorSimple.hpp"
#include "selectorQuadTree.hpp"
#include "selectorGPU.hpp"
#include "scene.hpp"

#include <stdlib.h>

#include <iostream>
#include <functional>
#include <algorithm>
#include <random>
#include <ctime>

#include <glm/gtc/type_ptr.hpp>

//TODO autofocus of window
BubbleCursor::BubbleCursor(Scene* scene, Texture* _texture)
: SceneObject("bubbleCursor", scene)
, texture(_texture)
, enableDrawing(false)
{

}
	

BubbleCursor::~BubbleCursor() {
	glDeleteBuffers(1, &vboPositions);
	glDeleteVertexArrays(1, &vao);
}

void BubbleCursor::updateDistanceState(unsigned int _distance) {
	distance = (int) _distance;
}

void BubbleCursor::setShader(Shader *_shader) {
	shader = _shader;

	//TODO try w/o this but don't know if possible since glDrawArrays... is needed
	float position[] = { 1.0f, 1.0f };

	glDeleteVertexArrays(1, &vao);
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vboPositions);
	glBindBuffer(GL_ARRAY_BUFFER, vboPositions);
	glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(float), position, GL_STATIC_DRAW);
	GLint pos;
	pos = glGetAttribLocation(shader->programHandle(), "position");
	if (pos != -1) {
		glEnableVertexAttribArray(pos);
		glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void BubbleCursor::generateRandomPopulation() {
	int width, height;
	glfwGetWindowSize(scene->getWindow(), &width, &height);

	std::default_random_engine generator(time(NULL));
	std::normal_distribution<double> distributionY(height / 2.0f, height / 5.0f);
	std::normal_distribution<double> distributionX(width / 2.0f, width / 5.0f);
	population.clear();

	for (int i = 0; i < BUBBLE_POINTS; i++) {
		float x = (int) distributionX(generator);
		float y = (int) distributionY(generator);
		population.push_back(glm::vec2(x, y));
	}

}

void BubbleCursor::update(double deltaT) {

	generateRandomPopulation();

}

void BubbleCursor::keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_B && action == GLFW_PRESS) {
		enableDrawing = !enableDrawing;
		update(0);
	}
}

bool BubbleCursor::drawingEnabled() {
	return enableDrawing;
}

void BubbleCursor::draw() {

	if (!drawingEnabled())
		return;

	std::shared_ptr<SceneObject> obj;
	scene->getObject("selectorQuad", obj);
	std::shared_ptr<SelectorQuadTree> sQuad = std::dynamic_pointer_cast<SelectorQuadTree>(obj);
	sQuad->draw();
	sQuad->update(0);

	shader->use();

	int width, height;
	glfwGetWindowSize(scene->getWindow(), &width, &height);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_POINT_SPRITE);
	glEnable(GL_PROGRAM_POINT_SIZE);
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	for (int i = 0; i < BUBBLE_POINTS; ++i) {
		sQuad->pickObject(population[i]);
		updateDistanceState(sQuad->getLastDistance());

		glActiveTexture(GL_TEXTURE0 + 0);
		glBindTexture(GL_TEXTURE_2D, texture->getHandle());
		glUniform1i(glGetUniformLocation(shader->programHandle(), "cursorTexture"), 0);
		glUniform1f(glGetUniformLocation(shader->programHandle(), "size"), (float)distance/300.0f);
		int x = population[i].x;
		int y = population[i].y;
		glUniform2fv(glGetUniformLocation(shader->programHandle(), "mousePos"), 1, glm::value_ptr(glm::vec2( ((float) x) / width, (height - (float)y) / height)));

		glBindVertexArray(vao);
		glDrawArrays(GL_POINTS, 0, 1);
		glBindVertexArray(0);
	}

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
}
