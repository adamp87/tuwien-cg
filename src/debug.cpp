/*! \file debug.cpp
 *  \brief OpenGL debug and performance
 *  \author Adam
*/

#include "debug.hpp"
#include "scene.hpp"
#include "texture.hpp"
#include "shadowMap.hpp"
#include "sceneObject.hpp"
#include "selectorGPU.hpp"
#include "mouseSemantics.hpp"
#include "selectorSimple.hpp"
#include "selectorQuadTree.hpp"
#include "particleSystemGPU.hpp"
#ifndef NOLEGEND
#include "legend.hpp"
#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <sstream>
#include <iostream>
#include <algorithm>

#ifdef _WIN32
#undef APIENTRY
#include <windows.h>
#undef min
#undef max
#endif

Debug::Debug(Scene* scene, const char* windowTitle)
    : scene(scene)
    , frameCount(0)
    , minDeltaT(9)
    , maxDeltaT(0)
    , lastTime(0)
    , lastKeyTime(0)
    , lastTitleTime(0)
    , minFilterMode(3)
    , magFilterMode(1)
    , frameTimeOn(false)
    , wireFrameOn(false)
    , transparencyOn(true)
    , windowTitle(windowTitle)
{

}

Debug::~Debug()
{

}

// keys used: F1, F2, F3, F4, F5, F6, F7, F8, F9
void Debug::update(double time, double deltaT)
{
    if(time < lastTime)
    {//handle restart
        minDeltaT = 9;
        maxDeltaT = 0;
        frameCount = 0;
        lastTime = time;
        lastKeyTime = time;
        lastTitleTime = time;
        return;
    }

    if(0.5 < time - lastKeyTime) {
        if(glfwGetKey(scene->getWindow(), GLFW_KEY_F1) == GLFW_PRESS) {
            //help
            lastKeyTime = time;
            std::cout << "No help" << std::endl;
        } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_F2) == GLFW_PRESS) {
            //frame time
            lastKeyTime = time;
            frameTimeOn = !frameTimeOn;

            minDeltaT = 9;
            maxDeltaT = 0;
            frameCount = 0;
            lastTitleTime = time;
            if(frameTimeOn) {
                std::cout << "Frame time turned on" << std::endl;
            } else {
                std::cout << "Frame time turned off" << std::endl;
                glfwSetWindowTitle(scene->getWindow(), windowTitle.c_str());
            }
        } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_F3) == GLFW_PRESS) {
            //wire frame
            lastKeyTime = time;
            wireFrameOn = !wireFrameOn;
            if(wireFrameOn) {
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                std::cout << "Wire frame turned on" << std::endl;
            } else {
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                std::cout << "Wire frame turned off" << std::endl;
            }
        } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_F4) == GLFW_PRESS) {
            //mag_filter quality
            lastKeyTime = time;
            ++magFilterMode;
            if(magFilterMode == 2)
                magFilterMode = 0;

            if(magFilterMode == 0) {
                std::shared_ptr<ShadowMap> sm = std::dynamic_pointer_cast<ShadowMap>(scene->getLight());
                if(sm) {
                    glBindTexture(GL_TEXTURE_2D, sm->getDepthTexture());
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }

                for(auto it = scene->textures.begin(); it != scene->textures.end(); ++it) {
                    Texture* t = it->second.get();

                    glBindTexture(GL_TEXTURE_2D, t->getHandle());
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
                std::cout << "MAG_FILTER is GL_NEAREST" << std::endl;
            } else {
                std::shared_ptr<ShadowMap> sm = std::dynamic_pointer_cast<ShadowMap>(scene->getLight());
                if(sm) {
                    glBindTexture(GL_TEXTURE_2D, sm->getDepthTexture());
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }

                for(auto it = scene->textures.begin(); it != scene->textures.end(); ++it) {
                    Texture* t = it->second.get();

                    glBindTexture(GL_TEXTURE_2D, t->getHandle());
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
                std::cout << "MAG_FILTER is GL_LINEAR" << std::endl;
            }
        } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_F5) == GLFW_PRESS) {
            //min_filter quality
            lastKeyTime = time;
            ++minFilterMode;
            if(minFilterMode == 4)
                minFilterMode = 0;

            if(minFilterMode == 0) {
                std::shared_ptr<ShadowMap> sm = std::dynamic_pointer_cast<ShadowMap>(scene->getLight());
                if(sm) {
                    glBindTexture(GL_TEXTURE_2D, sm->getDepthTexture());
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
                for(auto it = scene->textures.begin(); it != scene->textures.end(); ++it) {
                    Texture* t = it->second.get();

                    glBindTexture(GL_TEXTURE_2D, t->getHandle());
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
                std::cout << "MIN_FILTER is GL_NEAREST" << std::endl;
            } else if(minFilterMode == 1) {
                std::shared_ptr<ShadowMap> sm = std::dynamic_pointer_cast<ShadowMap>(scene->getLight());
                if(sm) {
                    glBindTexture(GL_TEXTURE_2D, sm->getDepthTexture());
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
                for(auto it = scene->textures.begin(); it != scene->textures.end(); ++it) {
                    Texture* t = it->second.get();

                    glBindTexture(GL_TEXTURE_2D, t->getHandle());
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
                std::cout << "MIN_FILTER is GL_LINEAR" << std::endl;
            } else if(minFilterMode == 2) {
                std::shared_ptr<ShadowMap> sm = std::dynamic_pointer_cast<ShadowMap>(scene->getLight());
                if(sm) {
                    glBindTexture(GL_TEXTURE_2D, sm->getDepthTexture());
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
                for(auto it = scene->textures.begin(); it != scene->textures.end(); ++it) {
                    Texture* t = it->second.get();

                    glBindTexture(GL_TEXTURE_2D, t->getHandle());
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
                std::cout << "MIN_FILTER is GL_NEAREST_MIPMAP_NEAREST" << std::endl;
            } else {
                std::shared_ptr<ShadowMap> sm = std::dynamic_pointer_cast<ShadowMap>(scene->getLight());
                if(sm) {
                    glBindTexture(GL_TEXTURE_2D, sm->getDepthTexture());
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
                for(auto it = scene->textures.begin(); it != scene->textures.end(); ++it) {
                    Texture* t = it->second.get();

                    glBindTexture(GL_TEXTURE_2D, t->getHandle());
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
                std::cout << "MIN_FILTER is GL_LINEAR_MIPMAP_LINEAR" << std::endl;
            }
        } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_F6) == GLFW_PRESS) {
            //change tessellation level
            std::shared_ptr<SceneObject> node;
            if(!scene->getObject("gpu", node)) {
                std::cout << "GPU particle sys not found" << std::endl;
                return;
            }

            std::shared_ptr<ParticleSystemGPU> gpu = std::dynamic_pointer_cast<ParticleSystemGPU>(node);
            gpu->setTessLevelIn(gpu->getTessLevelIn() + 1);
            gpu->setTessLevelOut(gpu->getTessLevelOut() + 1);

            if(gpu->getTessLevelIn() > 15)
               gpu->setTessLevelIn(0);
            if(gpu->getTessLevelOut() > 15)
               gpu->setTessLevelOut(0);

            std::cout << "TessLevelIn: " << gpu->getTessLevelIn()
                      << " TessLevelOut: " << gpu->getTessLevelOut() << std::endl;

            //get number of atomic counters
            GLint max;
            glGetIntegerv(GL_MAX_ATOMIC_COUNTER_BUFFER_SIZE, &max);
            std::cout << "Max Atomic Counter: " << max << std::endl;

            lastKeyTime = time;
        } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_F7) == GLFW_PRESS) {
            //change mode and speed test of object selection

            std::shared_ptr<SceneObject> obj;
            scene->getObject("selectorGPU", obj);
            std::shared_ptr<SelectorGPU> sGPU = std::dynamic_pointer_cast<SelectorGPU>(obj);
            scene->getObject("selectorSimple", obj);
            std::shared_ptr<SelectorSimple> sImp = std::dynamic_pointer_cast<SelectorSimple>(obj);
            scene->getObject("selectorQuad", obj);
            std::shared_ptr<SelectorQuadTree> sQuad = std::dynamic_pointer_cast<SelectorQuadTree>(obj);
            scene->getObject("mouse", obj);
            std::shared_ptr<MouseSemantics> mouse = std::dynamic_pointer_cast<MouseSemantics>(obj);

            sImp->changeMode();
            sGPU->changeMode();
            sQuad->changeMode();

            std::cout << "  Start of speed test " << std::endl;
            const int runCount = 100;

            double avg = 0;
            double min = std::numeric_limits<double>::max();
            double max = -std::numeric_limits<double>::max();
            for(int i = 0; i < runCount; ++i) {
                double start = glfwGetTime();
                sGPU->draw();
                start = glfwGetTime() - start;
                avg += start;
                min = std::min(min, start);
                max = std::max(max, start);
            }
            avg /= (double)runCount;
            std::cout << "SelectorDraw"
                      << " X: " << sQuad->getBufferWidth()
                      << " Y: " << sQuad->getBufferHeight()
                      << " Avg: " << avg * 1000
                      << " Min: " << min * 1000
                      << " Max: " << max * 1000
                      << " ms" << std::endl;

            int width, height;
            sGPU->getPixelCount(width, height);

#if 0
			//we get better results, this is a sync
			glfwSwapBuffers(scene->getWindow());
			Sleep(800);
#endif

            avg = 0;
            min = std::numeric_limits<double>::max();
            max = -std::numeric_limits<double>::max();
            for(int i = 0; i < runCount; ++i) {
                double start = glfwGetTime();
                sGPU->pickObject(mouse->getMousePos());
                start = glfwGetTime() - start;
                avg += start;
                min = std::min(min, start);
                max = std::max(max, start);
            }
            avg /= (double)runCount;
            std::cout << "SelectorGPU" << " X: " << width << " Y: " << height
                      << " Avg: " << avg * 1000
                      << " Min: " << min * 1000
                      << " Max: " << max * 1000
                      << " ms" << std::endl;

            GLuint testTexture;
            std::vector<GLushort> data(width * height, 1);

            glGenTextures(1, &testTexture);
            glBindTexture(GL_TEXTURE_2D, testTexture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_R16UI, width, height, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, 0);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

            avg = 0;
            min = std::numeric_limits<double>::max();
            max = -std::numeric_limits<double>::max();
            for(int i = 0; i < runCount; ++i) {
                double start = glfwGetTime();
                glGetTexImage(GL_TEXTURE_2D, 0, GL_RED_INTEGER, GL_UNSIGNED_SHORT, data.data());
                start = glfwGetTime() - start;
                avg += start;
                min = std::min(min, start);
                max = std::max(max, start);
            }
            glBindTexture(GL_TEXTURE_2D, 0);
            glDeleteTextures(1, &testTexture);
            avg /= (double)runCount;
            std::cout << "MemCopy" << " X: " << width << " Y: " << height
                      << " Avg: " << avg * 1000
                      << " Min: " << min * 1000
                      << " Max: " << max * 1000
                      << " ms" << std::endl;

            sQuad->draw();
            avg = 0;
            min = std::numeric_limits<double>::max();
            max = -std::numeric_limits<double>::max();
            for(int i = 0; i < runCount; ++i) {
                double start = glfwGetTime();
                sQuad->update(0);
                start = glfwGetTime() - start;
                avg += start;
                min = std::min(min, start);
                max = std::max(max, start);
            }
            avg /= (double)runCount;
            std::cout << "QuadTree"
                      << " X: " << sQuad->getBufferWidth()
                      << " Y: " << sQuad->getBufferHeight()
                      << " Avg: " << avg * 1000
                      << " Min: " << min * 1000
                      << " Max: " << max * 1000
                      << " ms" << std::endl;

            avg = 0;
            min = std::numeric_limits<double>::max();
            max = -std::numeric_limits<double>::max();
            for(int i = 0; i < runCount; ++i) {
                double start = glfwGetTime();
                sQuad->pickObject(mouse->getMousePos());
                start = glfwGetTime() - start;
                avg += start;
                min = std::min(min, start);
                max = std::max(max, start);
            }
            avg /= (double)runCount;
            std::cout << "QuadTreePick"
                      << " Avg: " << avg * 1000
                      << " Min: " << min * 1000
                      << " Max: " << max * 1000
                      << " ms" << std::endl;

            avg = 0;
            min = std::numeric_limits<double>::max();
            max = -std::numeric_limits<double>::max();
            sImp->draw();
            for(int i = 0; i < runCount; ++i) {
                double start = glfwGetTime();
                sImp->pickObject(mouse->getMousePos());
                start = glfwGetTime() - start;
                avg += start;
                min = std::min(min, start);
                max = std::max(max, start);
            }
            avg /= (double)runCount;
            std::cout << "SimplePick"
                      << " Avg: " << avg * 1000
                      << " Min: " << min * 1000
                      << " Max: " << max * 1000
                      << " ms" << std::endl;

            std::cout << "  End of speed test " << std::endl;
            lastKeyTime = time;
        } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_F8) == GLFW_PRESS) {
            //frustum
            lastKeyTime = time;
            std::cout << "Frustum culling is not implemented" << std::endl;
        } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_F9) == GLFW_PRESS) {
            //transparency
            lastKeyTime = time;
            transparencyOn = !transparencyOn;
            std::cout << "No transparency" << std::endl;
    #if 0
            if(transparencyOn) {
                scene->getTrack()->setShader(particleA.get());
                scene->getWater()->setColorMode(SceneObject::Water);
                std::cout << "Transparency turned on" << std::endl;
            } else {
                scene->getTrack()->setShader(particleNoA.get());
                scene->getWater()->setColorMode(SceneObject::Texture);
                std::cout << "Transparency turned off" << std::endl;
            }
    #endif
        } else if(glfwGetKey(scene->getWindow(), GLFW_KEY_F11) == GLFW_PRESS) {
            //switch selector
            lastKeyTime = time;

            std::shared_ptr<SceneObject> obj;
            scene->getObject("selectorGPU", obj);
            std::shared_ptr<SelectorGPU> sGPU = std::dynamic_pointer_cast<SelectorGPU>(obj);
            scene->getObject("selectorSimple", obj);
            std::shared_ptr<SelectorSimple> sImp = std::dynamic_pointer_cast<SelectorSimple>(obj);
            scene->getObject("selectorQuad", obj);
            std::shared_ptr<SelectorQuadTree> sQuad = std::dynamic_pointer_cast<SelectorQuadTree>(obj);

            if(scene->getActiveSelector() == sImp) {
                std::cout << "Switched to QuadTree selector" << std::endl;
                scene->setActiveSelector(sQuad);
                sQuad->draw();
                sQuad->update(0);
#ifndef NOLEGEND
				scene->getLegend()->setActiveSelector("QuadTree");
#endif
            } else if(scene->getActiveSelector() == sQuad) {
                std::cout << "Switched to GPU selector" << std::endl;
                scene->setActiveSelector(sGPU);
                sGPU->draw();
                sGPU->update(0);
#ifndef NOLEGEND
                scene->getLegend()->setActiveSelector("ComputeShader");
#endif
            } else {
                std::cout << "Switched to Simple selector" << std::endl;
                scene->setActiveSelector(sImp);
                sImp->draw();
                sImp->update(0);
#ifndef NOLEGEND
                scene->getLegend()->setActiveSelector("Simple");
#endif
            }
        }
    }

    if(frameTimeOn) {
        ++frameCount;
        minDeltaT = std::min(minDeltaT, deltaT);
        maxDeltaT = std::max(maxDeltaT, deltaT);

        if(0.5 < (time - lastTitleTime)) {
            //write avg in every half sec
            //setWindowTitle function is slow on Linux
            double avgDeltaT = (time - lastTitleTime) / (double)frameCount;
            lastTitleTime = time;

            std::stringstream ss;
            ss << windowTitle << " Frametime";
            ss << " AVG:" << avgDeltaT * 1000 << " ms";
            ss << " MIN:" << minDeltaT * 1000 << " ms";
            ss << " MAX:" << maxDeltaT * 1000 << " ms";
            ss << " FPS:" << frameCount*2;
            glfwSetWindowTitle(scene->getWindow(), ss.str().c_str());
            frameCount = 0;
        }
    }

    lastTime = time;
}

#ifdef _WIN32
//Found by Felix
static std::string FormatDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, const char* msg) {
    std::stringstream stringStream;
    std::string sourceString;
    std::string typeString;
    std::string severityString;

    // The AMD variant of this extension provides a less detailed classification of the error,
    // which is why some arguments might be "Unknown".
    switch (source) {
        case GL_DEBUG_CATEGORY_API_ERROR_AMD:
        case GL_DEBUG_SOURCE_API: {
            sourceString = "API";
            break;
        }
        case GL_DEBUG_CATEGORY_APPLICATION_AMD:
        case GL_DEBUG_SOURCE_APPLICATION: {
            sourceString = "Application";
            break;
        }
        case GL_DEBUG_CATEGORY_WINDOW_SYSTEM_AMD:
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM: {
            sourceString = "Window System";
            break;
        }
        case GL_DEBUG_CATEGORY_SHADER_COMPILER_AMD:
        case GL_DEBUG_SOURCE_SHADER_COMPILER: {
            sourceString = "Shader Compiler";
            break;
        }
        case GL_DEBUG_SOURCE_THIRD_PARTY: {
            sourceString = "Third Party";
            break;
        }
        case GL_DEBUG_CATEGORY_OTHER_AMD:
        case GL_DEBUG_SOURCE_OTHER: {
            sourceString = "Other";
            break;
        }
        case 131185:
            return "ignore";
        default: {
            sourceString = "Unknown";
            break;
        }
    }

    switch (type) {
        case GL_DEBUG_TYPE_ERROR: {
            typeString = "Error";
            break;
        }
        case GL_DEBUG_CATEGORY_DEPRECATION_AMD:
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: {
            typeString = "Deprecated Behavior";
            break;
        }
        case GL_DEBUG_CATEGORY_UNDEFINED_BEHAVIOR_AMD:
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: {
            typeString = "Undefined Behavior";
            break;
        }
        case GL_DEBUG_TYPE_PORTABILITY_ARB: {
            typeString = "Portability";
            break;
        }
        case GL_DEBUG_CATEGORY_PERFORMANCE_AMD:
        case GL_DEBUG_TYPE_PERFORMANCE: {
            typeString = "Performance";
            break;
        }
        case GL_DEBUG_CATEGORY_OTHER_AMD:
        case GL_DEBUG_TYPE_OTHER: {
            typeString = "Other";
            break;
        }
        default: {
            typeString = "Unknown";
            break;
        }
    }

    switch (severity) {
        case GL_DEBUG_SEVERITY_HIGH: {
            severityString = "High";
            break;
        }
        case GL_DEBUG_SEVERITY_MEDIUM: {
            severityString = "Medium";
            break;
        }
        case GL_DEBUG_SEVERITY_LOW: {
            severityString = "Low";
            break;
        }
        default: {
            severityString = "Unknown";
            break;
        }
    }

    stringStream << "OpenGL Error: " << msg;
    stringStream << " [Source = " << sourceString;
    stringStream << ", Type = " << typeString;
    stringStream << ", Severity = " << severityString;
    stringStream << ", ID = " << id << "]";

    return stringStream.str();
}

static void APIENTRY DebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam) {
    switch (id) {
        case 131185: {
           return;
        }
        default: {
            break;
        }
    }

    std::string error = FormatDebugOutput(source, type, id, severity, message);
    std::cout << error << std::endl;
}
#endif //_WIN32

void Debug::enableWindowsDebug()
{
#ifdef _WIN32
    // Create a debug OpenGL context or tell your OpenGL library (GLFW, SDL) to do so.
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

     // Query the OpenGL function to register your callback function.
    PFNGLDEBUGMESSAGECALLBACKPROC _glDebugMessageCallback = (PFNGLDEBUGMESSAGECALLBACKPROC) wglGetProcAddress("glDebugMessageCallback");
    PFNGLDEBUGMESSAGECALLBACKARBPROC _glDebugMessageCallbackARB = (PFNGLDEBUGMESSAGECALLBACKARBPROC) wglGetProcAddress("glDebugMessageCallbackARB");

    // Register your callback function.
    if (_glDebugMessageCallback != NULL) {
        _glDebugMessageCallback(DebugCallback, NULL);
    }
    else if (_glDebugMessageCallbackARB != NULL) {
        _glDebugMessageCallbackARB(DebugCallback, NULL);
    }

    // Enable synchronous callback. This ensures that your callback function is called
    // right after an error has occurred. This capability is not defined in the AMD
    // version.
    if ((_glDebugMessageCallback != NULL) || (_glDebugMessageCallbackARB != NULL)) {
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    }
#endif
}
