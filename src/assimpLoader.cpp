/*! \file assimpLoader.cpp
 *  \brief %Scene detail loader
 *  \author Adam
*/

#include "light.hpp"
#include "shadowMap.hpp"
#include "shadowOmni.hpp"
#include "volumetricLight.hpp"
#include "scene.hpp"
#include "model.hpp"
#include "camera.hpp"
#include "lightCamera.hpp"
#include "texture.hpp"
#include "sceneObject.hpp"
#include "cubeMap.hpp"
#include "assimpLoader.hpp"
#include "meshUtil.hpp"
#include "tessellation.hpp"
#include "particleSystemCPU.hpp"
#include "particleSystemGPU.hpp"
#include "tank.hpp"
#include "selectorGPU.hpp"
#include "selectorQuadTree.hpp"

#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include <iostream>

#ifdef USE_OMNI_SHADOW
#define DEFAULT_COLOR_SHADER "omniColor"
#define DEFAULT_TEXTURE_SHADER "omniTexture"
#elif defined USE_SHADOW_MAP
#error Need shaders
#define DEFAULT_COLOR_SHADER "Color"
#define DEFAULT_TEXTURE_SHADER "Texture"
#else
#define DEFAULT_COLOR_SHADER "phongColor"
#define DEFAULT_TEXTURE_SHADER "phongTexture"
#endif

enum RenderIDX {
    EmptyIDX         = Scene::SentinelPre + 0,
    CameraIDX        = Scene::SentinelPre + 1,
    LampIDX          = Scene::SentinelPre + 2,
    EmptyLightCamIDX = Scene::SentinelPre + 3,
    LightCamIDX      = Scene::SentinelPre + 4,
};

AssimpLoader::AssimpLoader(const std::string& scenePath, Scene* scene)
    : scene(scene)
    , iscene(nullptr)
    , importer()
{
    iscene = importer.ReadFile(scenePath, 0);
    if(!iscene)
        throw std::runtime_error(std::string("Assimp: ") + importer.GetErrorString());
    std::string rootPath(scenePath, 0, scenePath.find_last_of('/'));

    //load lightsource
    if(iscene->HasLights()) { //TODO: should we have more lihgts?
        aiLight* ilight = iscene->mLights[0];
        aiNode* node = iscene->mRootNode->FindNode(ilight->mName);
        glm::mat4 M = aiMat2glmMat(node->mTransformation);

#ifdef USE_OMNI_SHADOW
        std::shared_ptr<Light> light(new ShadowOmni("Lamp", scene, 0, 0, M));
#elif defined USE_SHADOW_MAP
        std::shared_ptr<Light> light(new ShadowMap("Lamp", scene, 0, M));
#else
        std::shared_ptr<Light> light(new Light("Lamp", scene, 0, M));
#endif
        light->setPower(glm::vec3(45));
        light->setAmbient(aiVec2glmVec(ilight->mColorAmbient));
        light->setDiffuse(aiVec2glmVec(ilight->mColorDiffuse));
        light->setSpecular(aiVec2glmVec(ilight->mColorSpecular));
        scene->setLight(light);
    }

    //load camera
    if(iscene->HasCameras()) { //TODO: lightcamera effect not implemented at the moment
        for(unsigned i = 0; i < iscene->mNumCameras; ++i) {
            aiCamera* icamera = iscene->mCameras[i];
            std::string camName = icamera->mName.C_Str();

            glm::vec3 up(icamera->mUp.x, icamera->mUp.y, icamera->mUp.z);
            glm::vec3 eye(icamera->mLookAt.x, icamera->mLookAt.y, icamera->mLookAt.z);
            glm::vec3 pos(icamera->mPosition.x, icamera->mPosition.y, icamera->mPosition.z);
            if(camName == "Camera") {
                std::shared_ptr<Camera> camera(new Camera(camName, scene, pos, up, eye, icamera->mHorizontalFOV, icamera->mClipPlaneNear, icamera->mClipPlaneFar));
                scene->setCamera(camera);
            }
        }
    }
    
    std::cout << "Loading meshes with materials and textures: " << std::endl; 

    //load models
    for(unsigned i = 0; i < iscene->mNumMeshes; ++i) {
        aiMesh* mesh = iscene->mMeshes[i];
        std::unique_ptr<Model> model;
        std::string name(mesh->mName.C_Str());

        std::cout << "  " << name << std::endl;
        
        //load model
        loadModel(mesh, model);

        //get material
        float shininess;
        aiColor4D color;
        aiString texName;
        aiMaterial* material = iscene->mMaterials[mesh->mMaterialIndex];
        if(aiReturn_SUCCESS == material->Get(AI_MATKEY_COLOR_AMBIENT, color))
            model->setAmbient(glm::vec4(glm::vec3(0.2), 1.0));//aiVec2glmVec(color));//TODO: set in blender
        if(aiReturn_SUCCESS == material->Get(AI_MATKEY_COLOR_DIFFUSE, color))
            model->setDiffuse(aiVec2glmVec(color));
        if(aiReturn_SUCCESS == material->Get(AI_MATKEY_COLOR_SPECULAR, color))
            model->setSpecular(aiVec2glmVec(color));
        if(aiReturn_SUCCESS == material->Get(AI_MATKEY_SHININESS, shininess))
            model->setShininess(shininess);
        if(aiReturn_SUCCESS == material->Get(AI_MATKEY_TEXTURE_DIFFUSE(0), texName)) {
            //set texture, if any
            //NOTE: we allow only one texture per type
            std::string texturePath(rootPath);
            std::string textureName(texName.C_Str());
            texturePath.append("/texture/");
            texturePath.append(textureName);

            std::cout << "   Texture:" << textureName << std::endl;
            std::unique_ptr<Texture> texture(new Texture(texturePath));
            model->setTexture(texture.get());
            scene->textures[textureName] = std::move(texture);
        }

        scene->models[name] = std::move(model);

#if 0
        //debug, enumerate all properites
        for(unsigned i = 0; i < material->mNumProperties; ++i) {
            aiMaterialProperty* mp = material->mProperties[i];
            std::string key(mp->mKey.C_Str());

            std::vector<char> data(mp->mDataLength);
            std::copy(mp->mData, mp->mData + mp->mDataLength, data.begin());
        }
#endif
    }

    std::cout << "Meshes loaded" << std::endl;

    //load objects and their hierarchy
    if(!iscene->mRootNode)
        throw std::runtime_error("Root Node is NULL");
    std::shared_ptr<SceneObject> root(new SceneObject("root", scene, 0, aiMat2glmMat(iscene->mRootNode->mTransformation)));
    scene->setRootNode(root);

    nextNode(iscene->mRootNode, root);

    //load animations
    if(!iscene->HasAnimations())
        std::cout << "No animations in file" << std::endl;

    for(unsigned i = 0; i < iscene->mNumAnimations; ++i) {
        aiAnimation* anim = iscene->mAnimations[i];

        double fiveMS = 0.005;
        if(anim->mTicksPerSecond != 0)
            fiveMS /= anim->mTicksPerSecond;

        for(unsigned i = 0; i < anim->mNumChannels; ++i) {
            aiNodeAnim* nodeAnim = anim->mChannels[i];
            std::string nodeName(nodeAnim->mNodeName.C_Str());

            if(nodeAnim->mNumPositionKeys != nodeAnim->mNumRotationKeys || nodeAnim->mNumPositionKeys != nodeAnim->mNumScalingKeys)
                throw std::runtime_error("Animation keys are expected to have the same count");

            SceneObject::Animation animation;
            for(unsigned i = 1; i < nodeAnim->mNumPositionKeys; ++i) {
                aiVectorKey& pos1 = nodeAnim->mPositionKeys[i-1];
                aiVectorKey& scl1 = nodeAnim->mScalingKeys[i-1];
                aiQuatKey& rot1 = nodeAnim->mRotationKeys[i-1];
                aiVectorKey& pos2 = nodeAnim->mPositionKeys[i];
                aiVectorKey& scl2 = nodeAnim->mScalingKeys[i];
                aiQuatKey& rot2 = nodeAnim->mRotationKeys[i];

                glm::vec3 p1(pos1.mValue.x, pos1.mValue.y, pos1.mValue.z);
                glm::vec3 s1(scl1.mValue.x, scl1.mValue.y, scl1.mValue.z);
                glm::quat r1(rot1.mValue.w, rot1.mValue.x, rot1.mValue.y, rot1.mValue.z);
                glm::vec3 p2(pos2.mValue.x, pos2.mValue.y, pos2.mValue.z);
                glm::vec3 s2(scl2.mValue.x, scl2.mValue.y, scl2.mValue.z);
                glm::quat r2(rot2.mValue.w, rot2.mValue.x, rot2.mValue.y, rot2.mValue.z);

                for(double time = pos1.mTime; time < pos2.mTime; time += fiveMS) {
                    float ratio = (float)((time - pos1.mTime) / (pos2.mTime - pos1.mTime));

                    //lerp a+(b-a)*t
                    glm::vec3 p = p1+(p2-p1)*ratio;
                    glm::vec3 s = s1+(s2-s1)*ratio;
                    glm::quat r = glm::slerp(r1, r2, ratio);

                    glm::mat4 R = glm::mat4_cast(r);
                    glm::mat4 S = glm::scale(glm::mat4(1), s);
                    glm::mat4 T = glm::translate(glm::mat4(1), p);

                    glm::mat4 M = T * R * S;
                    animation.push_back(std::make_pair(time, M));
                }
            }
            std::shared_ptr<SceneObject> object;
            if(!scene->getObject(nodeName, object))
                throw std::runtime_error("Inconsistent node name");
            if(!object->setAnimation(animation))
                throw std::runtime_error("Object already has an animation");
        }
    }

    //set special objects
#if 0
    std::shared_ptr<SceneObject> psCPU(new ParticleSystemCPU(5000, "cpu", scene, scene->models["Plane.002"].get(), glm::scale(glm::mat4(1), glm::vec3(0.1f))));
    root->addChild(psCPU);
    scene->pushRender(psCPU, Scene::SentinelPost);

    std::shared_ptr<SceneObject> psGPU(new ParticleSystemGPU("gpu", scene, scene->models["Cube.001"].get()));
    root->addChild(psGPU);
    scene->pushRender(psGPU, Scene::SentinelDraw);

    std::shared_ptr<SceneObject> tank;
    scene->getObject("Main_Body", tank);
    tank->reset();
#endif
}

glm::vec3 AssimpLoader::aiVec2glmVec(const aiColor3D& v) {
    return glm::vec3(v[0], v[1], v[2]);
}

glm::vec4 AssimpLoader::aiVec2glmVec(const aiColor4D& v) {
    return glm::vec4(v[0], v[1], v[2], v[3]);
}

glm::mat4 AssimpLoader::aiMat2glmMat(const aiMatrix4x4& m) {
    glm::mat4 M;
    M[0][0] = m.a1; M[1][0] = m.a2; M[2][0] = m.a3; M[3][0] = m.a4;
    M[0][1] = m.b1; M[1][1] = m.b2; M[2][1] = m.b3; M[3][1] = m.b4;
    M[0][2] = m.c1; M[1][2] = m.c2; M[2][2] = m.c3; M[3][2] = m.c4;
    M[0][3] = m.d1; M[1][3] = m.d2; M[2][3] = m.d3; M[3][3] = m.d4;
    return M;
}

void AssimpLoader::nextNode(aiNode* iparent, std::shared_ptr<SceneObject> parent) {
    for(unsigned i = 0; i < iparent->mNumChildren; ++i) {
        aiNode* node = iparent->mChildren[i];
        aiMatrix4x4& m = node->mTransformation;
        std::string name(node->mName.C_Str());

        glm::mat4 M = aiMat2glmMat(m);
        std::shared_ptr<SceneObject> object;

        if(name == "Camera") {
            std::shared_ptr<Camera> camera = scene->getCamera();
            object = camera;
            parent->addChild(object);
            camera->setViewMatrix(M);
			camera->initializeLookupVector();
            scene->pushRender(object, CameraIDX);
        } else if(name == "Lamp") {
            object = scene->getLight();
            parent->addChild(object);
            scene->pushRender(object, LampIDX);
        } else if(name == "Empty") {
            //Should be placed in front of camera
            object = std::shared_ptr<SceneObject>(new SceneObject(name, scene, 0, M));
            parent->addChild(object);
            scene->pushRender(object, EmptyIDX);
        } else if(name == "EmptyLightCam") {
            object = std::shared_ptr<SceneObject>(new SceneObject(name, scene, 0, M));
            parent->addChild(object);
            scene->pushRender(object, EmptyLightCamIDX);
        } else {
            if(node->mNumMeshes != 1)
                throw std::runtime_error("Each object should contain only one mesh");

            std::string meshName(iscene->mMeshes[node->mMeshes[0]]->mName.C_Str());
            Model* model = scene->models[meshName].get();
            if(name == "CMObject") { // object on which the cubemap will be drawn onto
                //object knows its shaders
                std::shared_ptr<CubeMap> cmObject;
                cmObject = std::shared_ptr<CubeMap>(new CubeMap(name, scene, model, M));
                cmObject->setShader(scene->getShader("CMReflect"));
                scene->setCubeMap(cmObject);
                object = cmObject;
            } else if(name == "SunVolumetric") {
                std::shared_ptr<VolumetricLight> sunVolumetric;
                sunVolumetric = std::shared_ptr<VolumetricLight>(new VolumetricLight(name, scene, model, M));
                sunVolumetric->setShader(scene->getShader("volumetricFirstPassSun"));
                scene->setVolumetricLight(sunVolumetric);
                object = sunVolumetric;
#if 0 //minikrieg tank loading(has only one tank, handle with special classes)
            } else if(name == "Main_Body") {
                std::shared_ptr<Tank> tank;
                tank = std::shared_ptr<Tank>(new Tank(name, scene, model, M));
                object = tank;
                object->setShader(scene->getShader(DEFAULT_TEXTURE_SHADER));
            } else if(name == "Turret") {
                std::shared_ptr<Tank::Turret> turret;
                turret = std::shared_ptr<Tank::Turret>(new Tank::Turret(name, scene, model, M));
                object = turret;
                object->setShader(scene->getShader(DEFAULT_TEXTURE_SHADER));
            } else if(name == "gun") {
                std::shared_ptr<Tank::Gun> gun;
                gun = std::shared_ptr<Tank::Gun>(new Tank::Gun(name, scene, model, M));
                object = gun;
                object->setShader(scene->getShader(DEFAULT_TEXTURE_SHADER));
#endif
            } else { //default objects
                object = std::shared_ptr<SceneObject>(new SceneObject(name, scene, model, M));
                if(model->getTexture() == 0) {
                    object->setShader(scene->getShader(DEFAULT_COLOR_SHADER));
                } else {
                    object->setShader(scene->getShader(DEFAULT_TEXTURE_SHADER));
                }
            }
            parent->addChild(object);

            if(meshName == "Tank_mainbody")
                scene->pushRender(object, Scene::SentinelDrawPickable); //selectable object for selektron
            else
                scene->pushRender(object, Scene::SentinelDraw);

            //NOTE: objects should be ordered also according to model and texture, would remove swapping between memory
        }

        nextNode(node, object);
    }
}

/**
 * Adjacent vertex selector for ShadowVolumes.
 * \author Felix
 */
void AssimpLoader::selectAdjacent(const aiMesh* mesh, std::vector<unsigned int>& indices) {

    std::map<aiVector3D, int, CompareVectors> vertex_posMap;   
    std::map<Edge, Neighbors, CompareEdges> edge_neigborMap;
    std::vector<Face> m_uniqueFaces;
     // Step 1 - find the two triangles that share every edge
    for (int i = 0 ; i < mesh->mNumFaces ; i++) {
        const aiFace& face = mesh->mFaces[i];

        Face Unique;
        
        // If a position vector is duplicated in the VB we fetch the 
        // index of the first occurrence.
        for (int j = 0 ; j < 3 ; j++) {            
            int Index = face.mIndices[j];
            aiVector3D& v = mesh->mVertices[Index];
            
            if (vertex_posMap.find(v) == vertex_posMap.end()) {
                vertex_posMap[v] = Index;
            }
            else {
                Index = vertex_posMap[v];
            }           
            
            Unique.Indices[j] = Index;
        }
        
        m_uniqueFaces.push_back(Unique);
        
        Edge e1(Unique.Indices[0], Unique.Indices[1]);
        Edge e2(Unique.Indices[1], Unique.Indices[2]);
        Edge e3(Unique.Indices[2], Unique.Indices[0]);
        
        edge_neigborMap[e1].AddNeigbor(i);
        edge_neigborMap[e2].AddNeigbor(i);
        edge_neigborMap[e3].AddNeigbor(i);
    }   

    // Step 2 - build the index buffer with the adjacency info
    for (int i = 0 ; i < mesh->mNumFaces ; i++) {        
        const Face& face = m_uniqueFaces[i];
        
        for (int j = 0 ; j < 3 ; j++) {            
            Edge e(face.Indices[j], face.Indices[(j + 1) % 3]);
            assert(edge_neigborMap.find(e) != edge_neigborMap.end());
            Neighbors n = edge_neigborMap[e];
            int OtherTri = n.GetOther(i);
            
            //assert(OtherTri != -1);
            if(OtherTri == -1) {
                OtherTri = i;
            }

            const Face& OtherFace = m_uniqueFaces[OtherTri];
            int OppositeIndex = OtherFace.GetOppositeIndex(e);
         
            indices.push_back(face.Indices[j]);
            indices.push_back(OppositeIndex);             
        }
    }    
}

void AssimpLoader::loadModel(const aiMesh* mesh, std::unique_ptr<Model>& model)
{
    std::vector<float> uvs;
    std::vector<float> normals;
    std::vector<float> positions;
    std::vector<unsigned int> indices;
    std::vector<unsigned int> adjacentIndices;

    selectAdjacent(mesh, adjacentIndices);

    if(!mesh->HasFaces())
        throw std::runtime_error("Mesh has no faces");
    if(!mesh->HasNormals())
        throw std::runtime_error("Mesh has no normals");
    if(!mesh->HasPositions())
        throw std::runtime_error("Mesh has no IDX");
    if(!mesh->HasTextureCoords(0))
        std::cout << "Mesh has no UV" << std::endl;

    for(unsigned int j = 0; j < mesh->mNumFaces; ++j) {
        aiFace& face = mesh->mFaces[j];

        for(unsigned int k = 0; k < face.mNumIndices; ++k) {
            indices.push_back(face.mIndices[k]);
        }
    }

    for(unsigned int j = 0; j < mesh->mNumVertices; ++j) {
        aiVector3D& normal = mesh->mNormals[j];
        aiVector3D& vertex = mesh->mVertices[j];

        positions.push_back(vertex.x);
        positions.push_back(vertex.y);
        positions.push_back(vertex.z);

        normals.push_back(normal.x);
        normals.push_back(normal.y);
        normals.push_back(normal.z);

        aiVector3D& uv = mesh->mTextureCoords[0][j];
        for(unsigned int k = 0; k < mesh->mNumUVComponents[0]; ++k) {
            uvs.push_back(uv[k]);
        }
    }
    model.reset(new Model(positions, indices, adjacentIndices, normals, uvs));
}
