/*! \file shader.cpp
 *  \brief %Shader handling
 *  \author Adam
*/

#include "shader.hpp"
#include <GL/glew.h>

#include <memory>
#include <fstream>

//! Helper class to manage one %Shader object file in RAII style.
class Shader::ShaderObj
{
public:
    ShaderObj(const std::vector<char>& shaderSrc, GLenum shaderType) {
        shaderID = Shader::compileShader(shaderSrc, shaderType);
    }

    ~ShaderObj() {
        glDeleteShader(shaderID);
    }

    GLuint getShaderID() { return shaderID; }

private:
    GLuint shaderID;
};

Shader::Shader(const std::vector<std::pair<GLenum, std::string> >& shaderPaths)
{
    program_handle = glCreateProgram();
    if(program_handle == 0)
        throw std::runtime_error("Could not create program");

    std::vector<std::unique_ptr<ShaderObj> > shaders;
    for(auto it = shaderPaths.begin(); it != shaderPaths.end(); ++it) {
        GLenum shaderType = it->first;
        const std::string& shaderPath = it->second;

        std::vector<char> shaderSrc;
        loadTextFile(shaderPath, shaderSrc);
        std::unique_ptr<ShaderObj> obj(new ShaderObj(shaderSrc, shaderType));
        shaders.push_back(std::move(obj));
    }

    for(auto it = shaders.begin(); it != shaders.end(); ++it) {
        ShaderObj* s = it->get();

        glAttachShader(program_handle, s->getShaderID());
    }

    GLint linked;
    glLinkProgram(program_handle);
    glGetProgramiv(program_handle, GL_LINK_STATUS, &linked);

    if(!linked) {
        GLint logSize = 0;
        glGetProgramiv(program_handle, GL_INFO_LOG_LENGTH, &logSize);
        if(logSize == 0)
            throw std::runtime_error("Could not compile shader: no log");

        GLint written = 0;
        std::vector<char> logData(logSize);
        glGetProgramInfoLog(program_handle, logSize, &written, logData.data());

        std::string errStr("Shader not compiled.\n");
        errStr.append(logData.begin(), logData.end());
        throw std::runtime_error(errStr);
    }
}

Shader::~Shader()
{
    glDeleteProgram(program_handle);
}

GLuint Shader::compileShader(const std::vector<char>& shaderSrc, GLenum shaderType)
{
    GLuint shaderHandle = glCreateShader(shaderType);

    if(shaderHandle == 0)
        throw std::runtime_error("Could not create shader");

    GLint size = shaderSrc.size();
    const char* srcPtr = shaderSrc.data();
    glShaderSource(shaderHandle, 1, &srcPtr, &size);

    GLint compiled;
    glCompileShader(shaderHandle);
    glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compiled);

    if(!compiled) {
        GLint logSize = 0;
        glGetShaderiv(shaderHandle, GL_INFO_LOG_LENGTH, &logSize);
        if(logSize == 0)
            throw std::runtime_error("Could not compile shader: no log");

        GLint written = 0;
        std::vector<char> logData(logSize);
        glGetShaderInfoLog(shaderHandle, logSize, &written, logData.data());

        std::string errStr("Shader not compiled.\n");
        errStr.append(logData.begin(), logData.end());
        throw std::runtime_error(errStr);
    }

    return shaderHandle;
}

void Shader::loadTextFile(const std::string& filename, std::vector<char>& data)
{
    std::streampos size;
    std::ifstream file(filename, std::ios::in|std::ios::binary|std::ios::ate);
    if(!file.is_open())
        throw std::runtime_error(std::string("Could not open file:") + filename);

    size = file.tellg();
    data.resize(static_cast<size_t>(size));
    file.seekg(0, std::ios::beg);
    file.read(data.data(), size);
    file.close();
}
