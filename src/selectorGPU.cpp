/*! \file selectorGPU.cpp
 *  \brief Mouse selection handling
 *  \author Adam
*/

#include "scene.hpp"
#include "model.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "selectorGPU.hpp"

#include <iostream>
#include <algorithm>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>

const GLuint byte_max = std::numeric_limits<GLuint>::max();

SelectorGPU::SelectorGPU(const std::string& name, Scene* scene)
    : SelectorBase(name, scene)
{
    glGenBuffers(1, &objectBuffer);

    mode = 0;
    changeMode();

    computeShader = scene->getShader("selectorGPU");
}

SelectorGPU::~SelectorGPU()
{
    glDeleteBuffers(1, &objectBuffer);
}

void SelectorGPU::changeMode()
{
    if(mode == 0) {
        windowX = 32;
        windowY = 32;
        ++mode;
    } else if(mode == 1) {
        windowX = 64;
        windowY = 64;
        ++mode;
    } else if(mode == 2) {
        windowX = 128;
        windowY = 64;
        ++mode;
    } else if(mode == 3) {
        windowX = 256;
        windowY = 128;
        mode = 0;
    }
    std::cout << "SelektronGPU pixel count changed to X: "
              << windowX * KernelSize << ", Y: " << windowY * KernelSize
              << std::endl;

    buffer.resize(1, std::numeric_limits<GLuint>::max()); //use when there is only one atomic
    //buffer.resize(windowX * windowY, std::numeric_limits<GLuint>::max()); //use when each workgroup has an atomic

    // The SSBO containing the minimum distance and the id of the object
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, objectBuffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, buffer.size() * sizeof(GLuint), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void SelectorGPU::pickObject(const glm::ivec2& mouse)
{
    computeShader->use();

    // Bind the SSBO
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, objectBuffer);

    // Zero values
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, objectBuffer);
    glClearBufferData(GL_SHADER_STORAGE_BUFFER, GL_R8UI, GL_RED, GL_UNSIGNED_INT, &byte_max);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    glUniform2i(0, width, height);
    glUniform2i(2, mouse[0], mouse[1]);
    glBindImageTexture(1, idTexture, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R16UI);

    glDispatchCompute(windowX, windowY, 1);

    //get values
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, objectBuffer);
    glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(GLuint) * buffer.size(), buffer.data());
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);

    //get minimum from buffer
    GLuint min = *(std::min_element(buffer.begin(), buffer.end()));

    //unpack values
    lastSelected = (min & 0xFFFF);
    lastDistance = (min & 0xFFFF0000) >> 16;
}
