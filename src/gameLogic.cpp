/*! \file gameLogic.cpp
 *  \brief %GameLogic of SteelWorms
 *  \author Adam
*/

#include "tank.hpp"
#include "scene.hpp"
#include "model.hpp"
#include "texture.hpp"
#include "gameLogic.hpp"

#include <stdexcept>
#include <GLFW/glfw3.h>
#include <glm/gtx/norm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

//! Implements the bullet movement on CPU
class GameLogic::Bullet : public SceneObject
{
public:
    Bullet(const glm::mat4& shootModelMatrix, Scene* scene, Model* model, Shader* shader)
      : SceneObject("Bullet", scene, model, glm::scale(shootModelMatrix, glm::vec3(0.1f)))
      , shot(false)
      , power(0.0f)
      , shootOrigin(modelMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0))
    {
        Bullet::setShader(shader);
    }

    virtual ~Bullet() {}

    void update(double deltaT) {
        const double g = 9.81f; //gravity
        const double fi = 0.6f; // shoot angle(currently a constant, will depend on the turret)

        glm::vec4 pos = modelMatrix * glm::vec4(0.0, 0.0, 0.0, 1.0);
        double zo = shootOrigin.z;
        double v = power * 10.0f;
        double d = glm::l2Norm(glm::vec3(shootOrigin.x, shootOrigin.y, 0.0f), glm::vec3(pos.x, pos.y, 0.0f));
        double z = zo + d * tan(fi) - g*pow(d, 2) / (2*pow(v*cos(fi), 2)); //wikipedia: ballistic trajectory

        double deltaX = -10.0 * deltaT;
        modelMatrix = glm::translate(modelMatrix, glm::vec3(deltaX, z - pos.z, 0.0f));
    }

    bool isShot() { return shot; }
    void setShot() { shot = true; }
    void increasePower(double val) { power += val; }

private:
    bool shot;
    double power;
    glm::vec4 shootOrigin;
};

GameLogic::Terrain::Terrain(size_t dimX,
                            size_t dimY,
                            float cellSize,
                            const std::vector<float>& heights,
                            const std::string& name,
                            Scene* scene,
                            Model* model,
                            Shader* shader)
    : SceneObject(name, scene, model, glm::mat4(1))
    , dimX(dimX)
    , dimY(dimY)
    , cellSize(cellSize)
    , heights(heights)
{
    Terrain::setShader(shader);
}

float GameLogic::Terrain::getHeight(float x, float y)
{
    /*
     *Idea described in book
     *Introduction to 3D game programming with DirectX 9.0
     *By Rod Lopez, Frank D. Luna
     *PAGE 228-231
     */
    x = dimX / 2.0f + x / cellSize;
    y = dimY / 2.0f + y / cellSize;

    size_t row = static_cast<size_t>(floor(y));
    size_t col = static_cast<size_t>(floor(x));

    if(dimX < row || dimY < col)
        return std::numeric_limits<float>::infinity();

    size_t posTL = row * dimX + col;
    size_t posTR = posTL + 1;
    size_t posBL = posTL + dimX;
    size_t posBR = posBL + 1;

    float dx = x - col;
    float dy = y - row;

    float A = heights[posTL];
    float B = heights[posTR];
    float C = heights[posBL];
    float D = heights[posBR];

    float height;
    if(dy < 1.0f - dx)
    {// upper triangle ABC
        float uz = B - A; // A->B
        float vz = C - A; // A->C
        height = A + lerp(0.0f, uz, dx) + lerp(0.0f, vz, dy);
    }
    else
    {// lower triangle DCB
        float uz = C - D; // D->C
        float vz = B - D; // D->B
        height = D + lerp(0.0f, uz, 1.0f - dx) + lerp(0.0f, vz, 1.0f - dy);
    }
    return height;
}

float GameLogic::Terrain::lerp(float a, float b, float t)
{//Linear interpolation
    return a - (a*t) + (b*t);
}

GameLogic::GameLogic(Scene* scene)
    : SceneObject("GameLogic", scene)
    , turnTime(0.0f)
    , activeTank(0)
    , gameFinished(false)
{
    size_t width;
    size_t height;
    size_t bitPerPixel;
    std::vector<float> uvs;
    std::vector<float> normals;
    std::vector<float> heights;
    std::vector<float> positions;
    std::vector<unsigned int> indices;
    std::vector<unsigned char> pixel_data;

    Texture::loadBMP("../texture/heightmap.bmp", pixel_data, width, height, bitPerPixel);
    Model::getTerrainModel(width, height, bitPerPixel, 0.1f, pixel_data, heights, positions, indices, normals, uvs);
    terrainModel.reset(new Model(positions, indices, indices, normals, uvs));
    terrain.reset(new Terrain(width, height, 0.1f, heights, "Terrain", scene, terrainModel.get(), scene->getShader("omniColor")));

    Texture::loadBMP("../texture/watermap.bmp", pixel_data, width, height, bitPerPixel);
    Model::getTerrainModel(width, height, bitPerPixel, 0.1f, pixel_data, heights, positions, indices, normals, uvs);
    waterTexture.reset(new Texture("../texture/water.bmp"));
    waterModel.reset(new Model(positions, indices, indices, normals, uvs, waterTexture.get()));
    water.reset(new Terrain(width, height, 0.1f, heights, "Water", scene, waterModel.get(), scene->getShader("omniTexture")));

    std::shared_ptr<SceneObject> object = std::dynamic_pointer_cast<SceneObject>(terrain);
    scene->pushRender(object, Scene::SentinelDraw);
    object = std::dynamic_pointer_cast<SceneObject>(water);
    scene->pushRender(object, Scene::SentinelPost);
}

GameLogic::~GameLogic()
{

}

void GameLogic::addTank(std::shared_ptr<SceneObject> tank)
{
    std::shared_ptr<Tank> ptr = std::dynamic_pointer_cast<Tank>(tank);
    if(!ptr)
        throw std::runtime_error("SceneObject is not a Tank");
    tanks.push_back(ptr);
}

void GameLogic::update(double deltaT)
{
    const double maxTurnTime = 60.0f;
    if(!bullet) {
        glm::vec4 pos = tanks[activeTank]->getModelMatrix() * glm::vec4(glm::vec3(0.0), 1.0f);
        float waterZ = water->getHeight(pos.x, pos.y);
        float deltaZ = pos.z - waterZ;
        if(deltaZ <= 0) {
            double newhp = tanks[activeTank]->getHP() + (5.0 * deltaZ * deltaT);
            tanks[activeTank]->setHP(newhp);
            if(newhp <= 0.0)
                turnTime = maxTurnTime;
        }
        if(pos.z == std::numeric_limits<float>::infinity()) { //tank off track
            tanks[activeTank]->setHP(-1);
            turnTime = maxTurnTime;
        }

        turnTime += deltaT;
        if(maxTurnTime < turnTime || glfwGetKey(scene->getWindow(), GLFW_KEY_ENTER) == GLFW_PRESS) {
            if(1.0f < turnTime) { //protect against two enter after each other
                //end turn, activate next player
                turnTime = 0.0f;
                tanks[activeTank]->setActive(false);

                size_t tanksAlive = 0;
                for(auto it = tanks.begin(); it != tanks.end(); ++it) {
                    Tank* tank = it->get();

                    if(0.0f < tank->getHP())
                        ++tanksAlive;
                }

                if(tanksAlive == 0) {
                    //game is drawn
                    gameFinished = true;
                } else {
                    do { //set active tank
                        ++activeTank;
                        if(activeTank == tanks.size())
                            activeTank = 0;
                    } while(tanks[activeTank]->getHP() <= 0.0f);
                    if(tanksAlive == 1) {//active tank is the winner
                        gameFinished = true;
                    } else { //game still runs
                        tanks[activeTank]->setActive(true);
                    }
                }
            }
        }
        if(glfwGetKey(scene->getWindow(), GLFW_KEY_SPACE) == GLFW_PRESS) {
            //construct bullet, deactivate player
            tanks[activeTank]->setActive(false);
            bullet.reset(new Bullet(tanks[activeTank]->getShootOrigin(), scene, 0, 0));
            /// \todo Set model for bullet
        }
    } else {
        if(glfwGetKey(scene->getWindow(), GLFW_KEY_SPACE) == GLFW_PRESS) {
            //increase shoot power
            if(!bullet->isShot()) {
                bullet->increasePower(deltaT);
            }
        }
        if(glfwGetKey(scene->getWindow(), GLFW_KEY_SPACE) == GLFW_RELEASE) {
            //shoot bullet
            if(!bullet->isShot()) {
                std::shared_ptr<SceneObject> object = std::dynamic_pointer_cast<SceneObject>(bullet);
                scene->pushRender(object, Scene::SentinelDraw);
                bullet->setShot();
            }
        }
        { // verify bullet height
            if(bullet->isShot()) {
                glm::vec4 bulletXYZ = bullet->getModelMatrix() * glm::vec4(glm::vec3(0.0), 1.0);
                float terrainZ = terrain->getHeight(bulletXYZ.x, bulletXYZ.y);

                float deltaZ = bulletXYZ.z - terrainZ;
                if(deltaZ < -0.05f) {//bullet impact
                    //decrease HP if needed

                    for(auto it = tanks.begin(); it != tanks.end(); ++it) {
                        Tank* tank = it->get();

                        glm::vec4 tankXYZ = tank->getModelMatrix() * glm::vec4(glm::vec3(0.0), 1.0);
                        float d = glm::l2Norm(glm::vec3(bulletXYZ.x, bulletXYZ.y, bulletXYZ.z),
                                              glm::vec3(tankXYZ.x, tankXYZ.y, tankXYZ.z));

                        if(d < 1.0f) {
                            tank->setHP(tank->getHP() - (1.0f - d));
                        }
                    }

                    //end turn, delete bullet
                    turnTime = maxTurnTime;
                    std::shared_ptr<SceneObject> object = std::dynamic_pointer_cast<SceneObject>(bullet);
                    scene->remRender(object);
                    bullet.reset();
                }
            }
        }
    }
}
