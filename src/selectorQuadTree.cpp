/*! \file selectorQuadTree.cpp
 *  \brief Mouse selection handling
 *  \author Adam
*/

#include "scene.hpp"
#include "model.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "selectorQuadTree.hpp"

#include <deque>
#include <iostream>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>
#include <algorithm>

class SelectorQuadTree::Node {
public:
    enum { Empty = 0, Overlap = 0xFFFF };

    size_t childs[4]; //!< indices of childs
    unsigned int val; //!< id of object in node
    unsigned int bb[4]; //!< Bounding box - minX, minY, maxX, maxY

    Node(int val)
        : val(val)
    {
        bb[2] = 0;
        bb[3] = 0;
        bb[0] = 0xFFFFFFFF;
        bb[1] = 0xFFFFFFFF;
        std::fill(childs, childs + 4, 0);
    }

private:
    Node() {}
};

SelectorQuadTree::SelectorQuadTree(const std::string& name, Scene* scene)
    : SelectorBase(name, scene)
{
    maskBit = 10;
    treeSize = 2048; //TODO: adapt to window size, if resize is allowed
    screen.resize(width * height); //TODO: adapt to window size, if resize is allowed

    //TODO: adapt to window size, if resize is allowed
    if(width > treeSize || height > treeSize)
        throw std::runtime_error("Window is larger then maximum tree size");
}

SelectorQuadTree::~SelectorQuadTree()
{

}

void SelectorQuadTree::changeMode()
{
    std::cout << "SelektronQuadTree pixel count is X: " << width << ", Y: " << height << std::endl;
}

void SelectorQuadTree::update(double)
{
    //retrieve texture
    glBindTexture(GL_TEXTURE_2D, idTexture);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RED_INTEGER, GL_UNSIGNED_SHORT, screen.data());
    glBindTexture(GL_TEXTURE_2D, 0);

    nodes.clear();
    nodes.push_back(Node(0)); //root node

    for(unsigned int y = 0; y < height; ++y) {
        int pY = (height - y - 1) * width;
        for(unsigned int x = 0; x < width; ++x) {
            unsigned int id = screen[pY + x];
            if(id == Node::Empty)
                continue;

            size_t node = 0;
            unsigned int bit = maskBit;
            unsigned int mask = treeSize >> 1;
            do {//iterate from top-bottom
                unsigned int childX = (x & mask) >> bit;
                unsigned int childY = (y & mask) >> bit;
                unsigned int childKey = childY * 2 + childX;

                size_t child = nodes[node].childs[childKey];
                if(nodes[node].childs[childKey] == Node::Empty) {
                    //new node, add it
                    child = nodes.size();
                    nodes.push_back(Node(id));
                    nodes[node].childs[childKey] = child;
                }

                Node& childNode = nodes[child];
                if(childNode.val != id) {
                    //existing node containing an object with different id
                    childNode.val = Node::Overlap;
                }

                //compute bounding box
                childNode.bb[0] = std::min(childNode.bb[0], x);
                childNode.bb[1] = std::min(childNode.bb[1], y);
                childNode.bb[2] = std::max(childNode.bb[2], x);
                childNode.bb[3] = std::max(childNode.bb[3], y);

                //next level
                --bit;
                mask >>= 1;
                node = child;
            } while(mask != 0);
        }
    }
}

void SelectorQuadTree::pickObject(const glm::ivec2 &mouse)
{
    glm::vec2 mouseF(mouse);
    std::deque<size_t> nextLevelQueue;

    nextLevelQueue.push_back(0);
    lastSelected = lastDistance = 0xFFFF;
    float minD = std::numeric_limits<float>::max();
    float maxD = std::numeric_limits<float>::max();

    while(!nextLevelQueue.empty()) {
        std::deque<size_t> levelQueue(nextLevelQueue);
        nextLevelQueue.clear();

        minD = std::numeric_limits<float>::max();
        maxD = std::numeric_limits<float>::max();

        while(!levelQueue.empty()) {//loop on one level
            Node& node = nodes[levelQueue.front()];
            levelQueue.pop_front();

            for(int i = 0; i < 4; ++i) {
                if(node.childs[i] == Node::Empty)
                    continue;

                Node& childNode = nodes[node.childs[i]];
                float nodeMin = std::numeric_limits<float>::max();
                float nodeMax = -std::numeric_limits<float>::max();

                //get distances from the four edges of the bounding box

                //min x, min y
                float dist = glm::distance(glm::vec2(childNode.bb[0], childNode.bb[1]), mouseF);
                nodeMax = std::max(nodeMax, dist);
                nodeMin = std::min(nodeMin, dist);

                //min x, max y
                dist = glm::distance(glm::vec2(childNode.bb[0], childNode.bb[3]), mouseF);
                nodeMax = std::max(nodeMax, dist);
                nodeMin = std::min(nodeMin, dist);

                //max x, min y
                dist = glm::distance(glm::vec2(childNode.bb[2], childNode.bb[1]), mouseF);
                nodeMax = std::max(nodeMax, dist);
                nodeMin = std::min(nodeMin, dist);

                //max x, max y
                dist = glm::distance(glm::vec2(childNode.bb[2], childNode.bb[3]), mouseF);
                nodeMax = std::max(nodeMax, dist);
                nodeMin = std::min(nodeMin, dist);

                //minimum distance of node is larger then smallest maximum distance of this level
                if(maxD < nodeMin)
                    continue;

                nextLevelQueue.push_back(node.childs[i]);

                if(nodeMin < minD) {
                    minD = nodeMin;
                    lastDistance = (size_t)minD;
                    lastSelected = childNode.val;
                }

                maxD = std::min(nodeMax, maxD);
            }
        }
    }
}
