/*! \file selectorSimple.cpp
 *  \brief Mouse selection handling
 *  \author Adam
*/

#include "scene.hpp"
#include "model.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "selectorSimple.hpp"

#include <iostream>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>

#include <algorithm>

SelectorSimple::SelectorSimple(const std::string& name, Scene* scene)
    : SelectorBase(name, scene)
{
    mode = 0;
    changeMode();
}

SelectorSimple::~SelectorSimple()
{

}

void SelectorSimple::changeMode()
{
    if(mode == 0) {
        windowX = 256;
        windowY = 256;
        ++mode;
    } else if(mode == 1) {
        windowX = 512;
        windowY = 512;
        ++mode;
    } else if(mode == 2) {
        windowX = 1024;
        windowY = 512;
        ++mode;
    } else if(mode == 3) {
        windowX = 2048;
        windowY = 1024;
        mode = 0;
    }
    std::cout << "SelektronSimple pixel count changed to X: " << windowX << ", Y: " << windowY << std::endl;
}

void SelectorSimple::pickObject(const glm::ivec2 &mouse)
{
    int bb[4];

    bb[0] = std::max(0, mouse[0] - windowX / 2);
    bb[1] = std::max(0, mouse[1] - windowY / 2);
    bb[2] = std::min(width, mouse[0] + windowX / 2);
    bb[3] = std::min(height, mouse[1] + windowY / 2);

    screen.resize(width * height);

    //retrieve texture
    glBindTexture(GL_TEXTURE_2D, idTexture);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RED_INTEGER, GL_UNSIGNED_SHORT, screen.data());
    glBindTexture(GL_TEXTURE_2D, 0);

    lastSelected = 0xFFFF;
    lastDistance = 0xFFFF;
    for(int y = bb[1]; y < bb[3]; ++y) {
        int pY = (height - y - 1) * width;
        for(int x = bb[0]; x < bb[2]; ++x) {
            unsigned int id = screen[pY + x];

            if(id == 0)
                continue;

            unsigned int distance = (unsigned int)glm::distance(glm::vec2(x, y), glm::vec2(mouse));
            if(lastDistance <= distance)
                continue;

            lastSelected = id;
            lastDistance = distance;
        }
    }
}
