/*! \file model.cpp
 *  \brief %Model handling
 *  \author Adam
*/

#include "model.hpp"
#include "texture.hpp"
#include "assimpLoader.hpp"

#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <stdexcept>

Model::Model(const std::vector<float>& positions,
             const std::vector<unsigned int>& indices,
             const std::vector<unsigned int>& adjacentIndices,
             const std::vector<float>& normals,
             const std::vector<float>& uvs,
             Texture* texture)
    : texture(texture)
    , diffuse(1)
    , ambient(1)
    , specular(1)
{
    indexCount = indices.size();
    indexAdjCount = adjacentIndices.size();

    //VBOs
    glGenBuffers(VBO_COUNT, vbo);
    //position
    glBindBuffer(GL_ARRAY_BUFFER, vbo[VBO_POSITIONS]);
    glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(float), positions.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //normals
    glBindBuffer(GL_ARRAY_BUFFER, vbo[VBO_NORMALS]);
    glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), normals.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //uvs
    glBindBuffer(GL_ARRAY_BUFFER, vbo[VBO_UVS]);
    glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(float), uvs.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[VBO_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[VBO_ADJ_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, adjacentIndices.size() * sizeof(unsigned int), adjacentIndices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

Model::~Model()
{
    glDeleteBuffers(VBO_COUNT, vbo);
}

void Model::loadModels(const std::string& modelPath, std::vector<std::unique_ptr<Model> >& models)
{
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(modelPath, 0);// aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_SortByPType);
    if(!scene)
        throw std::runtime_error(importer.GetErrorString());

    for(unsigned int i = 0; i < scene->mNumMeshes; ++i) {
        aiMesh* mesh = scene->mMeshes[i];
        std::unique_ptr<Model> model;

        AssimpLoader::loadModel(mesh, model);
        models.push_back(std::move(model));
    }
}

void Model::getQuadModel(std::vector<float>& positions,
                         std::vector<unsigned int>& indices,
                         std::vector<float>& uvs,
                         float width,
                         float height)
{

    uvs.clear();
    indices.clear();
    positions.clear();

//    float[] vertices = {
//        -0.5f, 0.5f, 0f,    // Left top         ID: 0
//        -0.5f, -0.5f, 0f,   // Left bottom      ID: 1
//        0.5f, -0.5f, 0f,    // Right bottom     ID: 2
//        0.5f, 0.5f, 0f  // Right left       ID: 3
//};

    //quadface
    positions.push_back(-width); positions.push_back( height); positions.push_back(0); //left top
    positions.push_back(-width); positions.push_back(-height); positions.push_back(0); //left bottom
    positions.push_back( width); positions.push_back(-height); positions.push_back(0); //right bottom
    positions.push_back( width); positions.push_back( height); positions.push_back(0); //right top

    indices.push_back(0); indices.push_back(1); indices.push_back(2);
    indices.push_back(2); indices.push_back(3); indices.push_back(0);
    
    uvs.push_back(0.0f); uvs.push_back(1.0f);
    uvs.push_back(0.0f); uvs.push_back(0.0f);
    uvs.push_back(1.0f); uvs.push_back(0.0f);
    uvs.push_back(1.0f); uvs.push_back(1.0f);
   
}


void Model::getCubeModel(std::vector<float>& positions,
                         std::vector<unsigned int>& indices,
                         std::vector<float>& normals,
                         std::vector<float>& uvs)
{
    float size = 1.0f;
    float center[3] = {0.0f, 0.0f, 0.0f};

    uvs.clear();
    normals.clear();
    indices.clear();
    positions.clear();
    //back
    positions.push_back(center[0] - size); positions.push_back(center[1] - size); positions.push_back(center[2] - size);
    positions.push_back(center[0] - size); positions.push_back(center[1] + size); positions.push_back(center[2] - size);
    positions.push_back(center[0] + size); positions.push_back(center[1] + size); positions.push_back(center[2] - size);
    positions.push_back(center[0] + size); positions.push_back(center[1] - size); positions.push_back(center[2] - size);

    //front
    positions.push_back(center[0] - size); positions.push_back(center[1] - size); positions.push_back(center[2] + size);
    positions.push_back(center[0] + size); positions.push_back(center[1] - size); positions.push_back(center[2] + size);
    positions.push_back(center[0] + size); positions.push_back(center[1] + size); positions.push_back(center[2] + size);
    positions.push_back(center[0] - size); positions.push_back(center[1] + size); positions.push_back(center[2] + size);

    //top
    positions.push_back(center[0] + size); positions.push_back(center[1] + size); positions.push_back(center[2] + size);
    positions.push_back(center[0] + size); positions.push_back(center[1] + size); positions.push_back(center[2] - size);
    positions.push_back(center[0] - size); positions.push_back(center[1] + size); positions.push_back(center[2] - size);
    positions.push_back(center[0] - size); positions.push_back(center[1] + size); positions.push_back(center[2] + size);

    //bottom
    positions.push_back(center[0] + size); positions.push_back(center[1] - size); positions.push_back(center[2] + size);
    positions.push_back(center[0] - size); positions.push_back(center[1] - size); positions.push_back(center[2] + size);
    positions.push_back(center[0] - size); positions.push_back(center[1] - size); positions.push_back(center[2] - size);
    positions.push_back(center[0] + size); positions.push_back(center[1] - size); positions.push_back(center[2] - size);

    //right
    positions.push_back(center[0] + size); positions.push_back(center[1] + size); positions.push_back(center[2] + size);
    positions.push_back(center[0] + size); positions.push_back(center[1] - size); positions.push_back(center[2] + size);
    positions.push_back(center[0] + size); positions.push_back(center[1] - size); positions.push_back(center[2] - size);
    positions.push_back(center[0] + size); positions.push_back(center[1] + size); positions.push_back(center[2] - size);

    //left
    positions.push_back(center[0] - size); positions.push_back(center[1] + size); positions.push_back(center[2] + size);
    positions.push_back(center[0] - size); positions.push_back(center[1] + size); positions.push_back(center[2] - size);
    positions.push_back(center[0] - size); positions.push_back(center[1] - size); positions.push_back(center[2] - size);
    positions.push_back(center[0] - size); positions.push_back(center[1] - size); positions.push_back(center[2] + size);

    indices.push_back(0); indices.push_back(1); indices.push_back(2);
    indices.push_back(0); indices.push_back(2); indices.push_back(3);

    indices.push_back(4); indices.push_back(5); indices.push_back(6);
    indices.push_back(4); indices.push_back(6); indices.push_back(7);

    indices.push_back(8); indices.push_back( 9); indices.push_back(10);
    indices.push_back(8); indices.push_back(10); indices.push_back(11);

    indices.push_back(12); indices.push_back(13); indices.push_back(14);
    indices.push_back(12); indices.push_back(14); indices.push_back(15);

    indices.push_back(16); indices.push_back(17); indices.push_back(18);
    indices.push_back(16); indices.push_back(18); indices.push_back(19);

    indices.push_back(20); indices.push_back(21); indices.push_back(22);
    indices.push_back(20); indices.push_back(22); indices.push_back(23);

    normals.push_back(0.0f); normals.push_back(0.0f); normals.push_back(-1.0f);
    normals.push_back(0.0f); normals.push_back(0.0f); normals.push_back(-1.0f);
    normals.push_back(0.0f); normals.push_back(0.0f); normals.push_back(-1.0f);
    normals.push_back(0.0f); normals.push_back(0.0f); normals.push_back(-1.0f);

    normals.push_back(0.0f); normals.push_back(0.0f); normals.push_back(1.0f);
    normals.push_back(0.0f); normals.push_back(0.0f); normals.push_back(1.0f);
    normals.push_back(0.0f); normals.push_back(0.0f); normals.push_back(1.0f);
    normals.push_back(0.0f); normals.push_back(0.0f); normals.push_back(1.0f);

    normals.push_back(0.0f); normals.push_back(1.0f); normals.push_back(0.0f);
    normals.push_back(0.0f); normals.push_back(1.0f); normals.push_back(0.0f);
    normals.push_back(0.0f); normals.push_back(1.0f); normals.push_back(0.0f);
    normals.push_back(0.0f); normals.push_back(1.0f); normals.push_back(0.0f);

    normals.push_back(0.0f); normals.push_back(-1.0f); normals.push_back(0.0f);
    normals.push_back(0.0f); normals.push_back(-1.0f); normals.push_back(0.0f);
    normals.push_back(0.0f); normals.push_back(-1.0f); normals.push_back(0.0f);
    normals.push_back(0.0f); normals.push_back(-1.0f); normals.push_back(0.0f);

    normals.push_back(1.0f); normals.push_back(0.0f); normals.push_back(0.0f);
    normals.push_back(1.0f); normals.push_back(0.0f); normals.push_back(0.0f);
    normals.push_back(1.0f); normals.push_back(0.0f); normals.push_back(0.0f);
    normals.push_back(1.0f); normals.push_back(0.0f); normals.push_back(0.0f);

    normals.push_back(-1.0f); normals.push_back(0.0f); normals.push_back(0.0f);
    normals.push_back(-1.0f); normals.push_back(0.0f); normals.push_back(0.0f);
    normals.push_back(-1.0f); normals.push_back(0.0f); normals.push_back(0.0f);
    normals.push_back(-1.0f); normals.push_back(0.0f); normals.push_back(0.0f);

    uvs.push_back(1.0f); uvs.push_back(0.0f);
    uvs.push_back(1.0f); uvs.push_back(1.0f);
    uvs.push_back(0.0f); uvs.push_back(1.0f);
    uvs.push_back(0.0f); uvs.push_back(0.0f);

    uvs.push_back(0.0f); uvs.push_back(0.0f);
    uvs.push_back(1.0f); uvs.push_back(0.0f);
    uvs.push_back(1.0f); uvs.push_back(1.0f);
    uvs.push_back(0.0f); uvs.push_back(1.0f);

    uvs.push_back(0.0f); uvs.push_back(0.0f);
    uvs.push_back(1.0f); uvs.push_back(0.0f);
    uvs.push_back(1.0f); uvs.push_back(1.0f);
    uvs.push_back(0.0f); uvs.push_back(1.0f);

    uvs.push_back(0.0f); uvs.push_back(0.0f);
    uvs.push_back(1.0f); uvs.push_back(0.0f);
    uvs.push_back(1.0f); uvs.push_back(1.0f);
    uvs.push_back(0.0f); uvs.push_back(1.0f);

    uvs.push_back(1.0f); uvs.push_back(1.0f);
    uvs.push_back(1.0f); uvs.push_back(0.0f);
    uvs.push_back(0.0f); uvs.push_back(0.0f);
    uvs.push_back(0.0f); uvs.push_back(1.0f);

    uvs.push_back(0.0f); uvs.push_back(1.0f);
    uvs.push_back(1.0f); uvs.push_back(1.0f);
    uvs.push_back(1.0f); uvs.push_back(0.0f);
    uvs.push_back(0.0f); uvs.push_back(0.0f);
}

void Model::getTerrainModel(size_t width,
                            size_t height,
                            size_t bitPerPixel,
                            float cellSize,
                            std::vector<unsigned char>& data,
                            std::vector<float>& heights,
                            std::vector<float>& positions,
                            std::vector<unsigned int>& indices,
                            std::vector<float>& normals,
                            std::vector<float>& uvs)
{
    if(bitPerPixel != 8)
        throw std::runtime_error("Only 8bit images supported for terrain");

    uvs.clear();
    normals.clear();
    indices.clear();
    heights.clear();
    positions.clear();

    for(auto it = data.begin(); it != data.end(); ++it) {
        unsigned char p = *it;
        heights.push_back(p / 255.0f);
    }

    unsigned int idx = 0;
    float offsetX = width * cellSize / 2.0f;
    float offsetY = height * cellSize / 2.0f;
    for(size_t i = 0; i < height - 1; ++i) {
        for(size_t j = 0; j < width - 1; ++j) {
            size_t posTL = i * width + j;
            size_t posTR = posTL + 1;
            size_t posBL = posTL + width;
            size_t posBR = posBL + 1;

            glm::vec3 vecTL(j       * cellSize - offsetX, i       * cellSize - offsetY, heights[posTL]);
            glm::vec3 vecTR((j + 1) * cellSize - offsetX, i       * cellSize - offsetY, heights[posTR]);
            glm::vec3 vecBL(j       * cellSize - offsetX, (i + 1) * cellSize - offsetY, heights[posBL]);
            glm::vec3 vecBR((j + 1) * cellSize - offsetX, (i + 1) * cellSize - offsetY, heights[posBR]);

            glm::vec3 norTL = glm::normalize(glm::cross(vecTL - vecTR, vecTR - vecBL));
            glm::vec3 norTR = glm::normalize(glm::cross(vecTL - vecTR, vecTR - vecBL));
            glm::vec3 norBL = glm::normalize(glm::cross(vecTL - vecTR, vecTR - vecBL));
            glm::vec3 norBR = glm::normalize(glm::cross(vecTR - vecBR, vecBR - vecBL));

            normals.push_back(norTL[0]); normals.push_back(norTL[1]); normals.push_back(norTL[2]);
            normals.push_back(norTR[0]); normals.push_back(norTR[1]); normals.push_back(norTR[2]);
            normals.push_back(norBL[0]); normals.push_back(norBL[1]); normals.push_back(norBL[2]);
            normals.push_back(norBR[0]); normals.push_back(norBR[1]); normals.push_back(norBR[2]);

            positions.push_back(vecTL[0]); positions.push_back(vecTL[1]); positions.push_back(vecTL[2]);
            positions.push_back(vecTR[0]); positions.push_back(vecTR[1]); positions.push_back(vecTR[2]);
            positions.push_back(vecBL[0]); positions.push_back(vecBL[1]); positions.push_back(vecBL[2]);
            positions.push_back(vecBR[0]); positions.push_back(vecBR[1]); positions.push_back(vecBR[2]);

            uvs.push_back(0.0f); uvs.push_back(1.0f);
            uvs.push_back(1.0f); uvs.push_back(1.0f);
            uvs.push_back(0.0f); uvs.push_back(0.0f);
            uvs.push_back(1.0f); uvs.push_back(0.0f);

            indices.push_back(idx + 0);
            indices.push_back(idx + 1);
            indices.push_back(idx + 2);

            indices.push_back(idx + 1);
            indices.push_back(idx + 2);
            indices.push_back(idx + 3);
            idx += 4;
        }
    }
}

void Model::getSphereModel(std::vector<float>& positions,
                           std::vector<unsigned int>& indices,
                           std::vector<float>& normals,
                           std::vector<float>& uvs)
{
    uvs.clear();
    normals.clear();
    indices.clear();
    positions.clear();

    const float Radius = 1.0f;
    const int Resolution = 200;
    const float PI = 3.14159f;

    std::vector<glm::vec3> v;
    std::vector<glm::vec2> t;
    std::vector<glm::vec3> n;

    // iniatiate the variable we are going to use
    unsigned int idx = 0;
    float t1, t2, t3, t4;
    float X1,Y1,X2,Y2,Z1,Z2;
    float inc1,inc2,inc3,inc4,Radius1,Radius2;

    for(int w = 0; w < Resolution; ++w) {
        for(int h = (-Resolution/2); h < (Resolution/2); ++h){

            t1 = (w/(float)Resolution);
            t2 = ((w+1)/(float)Resolution);
            t3 = (h/(float)Resolution);
            t4 = ((h+1)/(float)Resolution);
            inc1 = t1*2*PI;
            inc2 = t2*2*PI;
            inc3 = t3*PI;
            inc4 = t4*PI;

            X1 = sin(inc1);
            Y1 = cos(inc1);
            X2 = sin(inc2);
            Y2 = cos(inc2);

            // store the upper and lower radius, remember everything is going to be drawn as triangles
            Radius1 = Radius*cos(inc3);
            Radius2 = Radius*cos(inc4);

            Z1 = Radius*sin(inc3);
            Z2 = Radius*sin(inc4);

            // insert the triangle coordinates
            v.push_back(glm::vec3(Radius1*X1,Z1,Radius1*Y1));
            v.push_back(glm::vec3(Radius1*X2,Z1,Radius1*Y2));
            v.push_back(glm::vec3(Radius2*X2,Z2,Radius2*Y2));

            v.push_back(glm::vec3(Radius1*X1,Z1,Radius1*Y1));
            v.push_back(glm::vec3(Radius2*X2,Z2,Radius2*Y2));
            v.push_back(glm::vec3(Radius2*X1,Z2,Radius2*Y1));

            // insert the texture map
            t.push_back(glm::vec2(t1, t3));
            t.push_back(glm::vec2(t1, t4));
            t.push_back(glm::vec2(t2, t3));

            t.push_back(glm::vec2(t2, t4));
            t.push_back(glm::vec2(t1, t4));
            t.push_back(glm::vec2(t2, t3));

            // insert the normal data
            n.push_back(glm::vec3(X1,Z1,Y1)/ glm::length(glm::vec3(X1,Z1,Y1)));
            n.push_back(glm::vec3(X2,Z1,Y2)/ glm::length(glm::vec3(X2,Z1,Y2)));
            n.push_back(glm::vec3(X2,Z2,Y2)/ glm::length(glm::vec3(X2,Z2,Y2)));

            n.push_back(glm::vec3(X1,Z1,Y1)/ glm::length(glm::vec3(X1,Z1,Y1)));
            n.push_back(glm::vec3(X2,Z2,Y2)/ glm::length(glm::vec3(X2,Z2,Y2)));
            n.push_back(glm::vec3(X1,Z2,Y1)/ glm::length(glm::vec3(X1,Z2,Y1)));

            indices.push_back(idx++);
            indices.push_back(idx++);
            indices.push_back(idx++);

            indices.push_back(idx++);
            indices.push_back(idx++);
            indices.push_back(idx++);
        }
    }

    for(size_t i = 0; i < v.size(); ++i) {
        glm::vec3& vec = v[i];
        positions.push_back(vec.x);
        positions.push_back(vec.y);
        positions.push_back(vec.z);
    }

    for(size_t i = 0; i < t.size(); ++i) {
        glm::vec2& uv = t[i];
        uvs.push_back(uv.x);
        uvs.push_back(uv.y);
    }

    for(size_t i = 0; i < n.size(); ++i) {
        glm::vec3& norm = n[i];
        normals.push_back(norm.x);
        normals.push_back(norm.y);
        normals.push_back(norm.z);
    }
}
