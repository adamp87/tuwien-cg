/*! \file texture.cpp
 *  \brief %Texture handling
 *  \author Adam
*/

#include "texture.hpp"

#include <stdexcept>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

Texture::Texture(const std::string& filename,bool readalpha)
{
    size_t width;
    size_t height;
    size_t bitPerPixel;
    std::vector<unsigned char> data;
    loadBMP(filename, data, width, height, bitPerPixel, readalpha);

    if(bitPerPixel != 24)
        throw std::runtime_error("Currently only BGR images are supported for textures");

    glGenTextures(1, &handle);
    glBindTexture(GL_TEXTURE_2D, handle);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, data.data());
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::~Texture()
{
    glDeleteTextures(1, &handle);
}

void Texture::loadBMP(const std::string& filename, std::vector<unsigned char>& data, size_t& width, size_t& height, size_t& bitPerPixel, bool readalpha)
{
#if 1
	cv::Mat img;
	//advanced img loading with OpenCV
	if (readalpha)
		img = cv::imread(filename, -1);
	else
		img = cv::imread(filename);

	if (img.cols == 0 || img.rows == 0)
		throw std::runtime_error(std::string("Image could not be loaded") + filename);

	//cv::namedWindow("Display window", cv::WINDOW_AUTOSIZE);// Create a window for display.
	//cv::imshow("Display window", img);

	width = img.cols;
	height = img.rows;
	if (img.channels() == 1) {
		bitPerPixel = 8;
		for (auto it = img.begin<unsigned char>(); it != img.end<unsigned char>(); ++it) {
			data.push_back(*it);
		}
	}
	else if (img.channels() == 3) {
		bitPerPixel = 24;
		for (auto it = img.begin<cv::Vec3b>(); it != img.end<cv::Vec3b>(); ++it) {
			cv::Vec3b& pixel = *it;
			data.push_back(pixel[0]);
			data.push_back(pixel[1]);
			data.push_back(pixel[2]);
			data.push_back(1.0f);
		}
	}
	else if (img.channels() == 4) {
		bitPerPixel = 24;
		for (auto it = img.begin<cv::Vec<uchar, 4>>(); it != img.end<cv::Vec<uchar, 4>>(); ++it) {
			cv::Vec<uchar, 4>& pixel = *it;
			data.push_back(pixel[0]);
			data.push_back(pixel[1]);
			data.push_back(pixel[2]);
			data.push_back(pixel[3]);
		}
	}
	else
        throw std::runtime_error("Unsupported channel count in image");

#else
    //Simple BMP loading method
    std::fstream file(filename, std::ios::in | std::ios::binary);
    if(!file.is_open())
        throw std::runtime_error("File not found");

    data.resize(54);
    file.read((char*)data.data(), 54);

    if(data[0] != 'B' && data[1] != 'M')
        throw std::runtime_error("Invalid file format. Bitmap required!");

    bitPerPixel =  data[28];
    width = data[18] + (data[19] << 8);
    height = data[22] + (data[23] << 8);
    size_t offset = data[10] + (data[11] << 8);
    size_t rowSize = ((bitPerPixel* width + 31) / 32) * 4;
    size_t pixelArraysize = rowSize * height;

    data.resize(pixelArraysize);
    file.seekg(offset, std::ios::beg);
    file.read((char*)data.data(), pixelArraysize);
    file.close();
#endif
}
