/*! \file shadowMap.cpp
 *  \brief Shadow Map effect
 *  \author Adam
*/

#include "model.hpp"
#include "scene.hpp"
#include "shader.hpp"
#include "shadowMap.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

const int sizeX = 1024;
const int sizeY = 1024;

class ShadowMap::EffectObject : public SceneObject
{
public:
    EffectObject(std::shared_ptr<SceneObject> effectParent)
        : SceneObject(effectParent)
    {
        setShader(scene->getShader("shadowMap"));
    }

    void draw() const {
        if(!model)
            return;

        shader->use();

        glm::mat4 M = getGlobalModelMatrix();

        glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));

        glBindVertexArray(vao);
        glDrawElements(GL_TRIANGLES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    }

    void setShader(Shader* val) {
        shader = val;
        SceneObject::setShader(val);
    }
};

ShadowMap::ShadowMap(const std::string& name, Scene* scene, Model* model, const glm::mat4& modelMatrix)
    : Light(name, scene, model, modelMatrix)
{
    ShadowMap::setShader(scene->getShader("shadowMap"));

    // The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
    glGenFramebuffers(1, &frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

    // Depth texture. Slower than a depth buffer, but you can sample it later in your shader
    glGenTextures(1, &depthTexture);
    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, sizeX, sizeY, 0,GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

ShadowMap::~ShadowMap()
{
    glDeleteTextures(1, &depthTexture);
    glDeleteFramebuffers(1, &frameBuffer);
}

void ShadowMap::draw() const
{
    shader->use();

    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "depthMVP"), 1, GL_FALSE, glm::value_ptr(depthMVP));

    //glEnable(GL_DEPTH_TEST);
    glViewport(0, 0, sizeX, sizeY);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glCullFace(GL_FRONT); // Cull front-facing triangles
    glDrawBuffer(GL_NONE); // No color buffer is drawn to.

    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        effect = obj->getEffect("shadowMap");
        if(effect) {
            effect->draw();
        } else {
            std::cout << "Internal error: No effect for object" << std::endl;
        }
    }

    glCullFace(GL_BACK); // Cull back-facing triangles
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glBindTexture(GL_TEXTURE_2D, depthTexture);
    glGenerateMipmap(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);

    int width, height;
    glfwGetWindowSize(scene->getWindow(), &width, &height);
    glViewport(0, 0, width, height);

    // Always check that our framebuffer is ok
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "ShadowMap: " << glewGetErrorString(glGetError()) << std::endl;
        return;
    }
}

void ShadowMap::reset()
{
    for(auto it = scene->begin(Scene::SentinelDraw), ite = scene->end(Scene::SentinelPost); it != ite; ++it) {
        SceneObject* effect;
        std::shared_ptr<SceneObject> obj = Scene::get(it);

        effect = obj->getEffect("shadowMap");
        if(!effect) {
            if(obj->getName() == "gpu") {
                /// \todo Add instanced effect for shadow map.
                //std::unique_ptr<SceneObject> eff(new EffectObjectInstanced(obj));
                //obj->addEffect("shadowMap", eff);
            } else {
                std::unique_ptr<SceneObject> eff(new EffectObject(obj));
                obj->addEffect("shadowMap", eff);
            }
        }
    }
}

void ShadowMap::update(double /*deltaT*/)
{
    //TODO: untested
    glm::mat4 biasMatrix(
    0.5, 0.0, 0.0, 0.0,
    0.0, 0.5, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.5, 0.5, 0.5, 1.0);

    glm::mat4 M = getGlobalModelMatrix();
    glm::mat4 P = glm::perspective(90.0f, sizeX / (float)sizeY, 0.1f, 100.0f);
    depthMVP = P * glm::inverse(M);
    depthBiasMVP = biasMatrix * depthMVP;
}

void ShadowMap::setShader(Shader* val)
{
    shader = val;
}
