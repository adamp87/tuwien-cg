/*! \file tessellation.cpp
 *  \brief %Tessellation effect
 *  \author Adam
*/

#include "scene.hpp"
#include "model.hpp"
#include "camera.hpp"
#include "shader.hpp"
#include "tessellation.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <opencv2/imgproc/imgproc.hpp>

void computeDispTexture(std::vector<float>& data) {
    //Compute a 2D Gaussian for displacement mapping
    for(int i = 0; i < 64; ++i) {
        cv::Mat x = cv::getGaussianKernel(16, 0.8 + (i/127.0)*1.6, CV_32F);
        cv::Mat y = cv::getGaussianKernel(16, 0.8 + (i/127.0)*1.6, CV_32F);
        cv::Mat xy = (x * y.t()) * 8;
        data.insert(data.end(), xy.begin<float>(), xy.end<float>());
    }
}

Tessellation::Tessellation(const std::string& name, Scene* scene, Model* model, const glm::mat4& modelMatrix)
    : SceneObject(name, scene, model, modelMatrix)
    , dispTime(0)
    , tessLevelIn(0)
    , tessLevelOut(0)
{
    Tessellation::setShader(scene->getShader("tessellation"));

    std::vector<float> data;
    computeDispTexture(data);

    glGenTextures(1, &dispTexture);
    glBindTexture(GL_TEXTURE_3D, dispTexture);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_R32F, 16, 16, 64, 0, GL_RED, GL_FLOAT, data.data());

    // Set up texture mapping parameters
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_MIRRORED_REPEAT);

    glBindTexture(GL_TEXTURE_3D, 0);
}

Tessellation::~Tessellation()
{
    glDeleteTextures(1, &dispTexture);
}

void Tessellation::draw() const
{
    //NOTE: ParticleSystemGPU uses a separate draw() function
    glDepthMask(GL_TRUE);

    shader->use();

    glm::mat4 M = getGlobalModelMatrix();
    glm::mat4 V = scene->getCamera()->getViewMatrix();
    glm::mat4 MV = V * M;
    glm::mat4 MVP = scene->getCamera()->getProjectionMatrix() * MV;
    glm::mat3 N = glm::mat3(glm::inverseTranspose(M));

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_3D, dispTexture);
    glUniform1i(glGetUniformLocation(shader->programHandle(), "dispTexture"), 0);

    glUniform1f(glGetUniformLocation(shader->programHandle(), "dispTime"), dispTime);
    glUniform1ui(glGetUniformLocation(shader->programHandle(), "tessLevelIn"), tessLevelIn);
    glUniform1ui(glGetUniformLocation(shader->programHandle(), "tessLevelOut"), tessLevelOut);
    glUniformMatrix3fv(glGetUniformLocation(shader->programHandle(), "N"), 1, GL_FALSE, glm::value_ptr(N));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "M"), 1, GL_FALSE, glm::value_ptr(M));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "V"), 1, GL_FALSE, glm::value_ptr(V));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MV"), 1, GL_FALSE, glm::value_ptr(MV));
    glUniformMatrix4fv(glGetUniformLocation(shader->programHandle(), "MVP"), 1, GL_FALSE, glm::value_ptr(MVP));

    glBindVertexArray(vao);
    glDrawElements(GL_PATCHES, model->getIndexCount(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

bool Tessellation::animate(double time)
{
    dispTime = static_cast<float>(time / 2);
    return SceneObject::animate(time);
}

void Tessellation::update(double deltaT)
{
    dispTime += static_cast<float>(deltaT / 2);
}

void Tessellation::setShader(Shader* val)
{
    shader = val;

    glDeleteVertexArrays(1, &vao);
    glGenVertexArrays(1, &vao);

    glBindVertexArray(vao);

    GLint pos;
    glBindBuffer(GL_ARRAY_BUFFER, model->getPosition());
    pos = glGetAttribLocation(shader->programHandle(), "position");
    if(pos != -1) {
        glEnableVertexAttribArray(pos);
        glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }

    glBindBuffer(GL_ARRAY_BUFFER, model->getNormals());
    pos = glGetAttribLocation(shader->programHandle(), "normal");
    if(pos != -1) {
        glEnableVertexAttribArray(pos);
        glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }

    if(model->getTexture()) {
        glBindBuffer(GL_ARRAY_BUFFER, model->getUVs());
        pos = glGetAttribLocation(shader->programHandle(), "uv");
        if(pos != -1) {
            glEnableVertexAttribArray(pos);
            glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, 0, 0);
        }
    }
       
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->getIndices());

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
