#include "light.hpp"
#include "scene.hpp"
#include "camera.hpp"
#include "shader.hpp"
#include "sceneObject.hpp"
#include "debug.hpp"
#include "cubeMap.hpp"
#include "volumetricLight.hpp"
#include "selectorSimple.hpp"
#include "selectorQuadTree.hpp"
#include "selectorGPU.hpp"
#include "mouseSemantics.hpp"
#include "bubbleCursor.hpp"

#ifndef NOLEGEND
#include "legend.hpp"
#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>
#include <iostream>
#include <stdexcept>
#include <sstream>

#define APP_NAME "Selektron 09.06.2015"
#define DEBUG 1 //TODO: discuss this
#define MOUSE_MIN_MOVE 0.3
#define MOUSE_MAX_MOVE 3.5
#define MOUSE_CUTOFF 200

int main(int argc, char** argv)
{
    int width = 1920;
    int height = 1080;

    GLFWwindow* window = NULL;
    std::unique_ptr<Scene> scene;
    std::unique_ptr<Debug> debug;
    std::shared_ptr<MouseSemantics> mouseSemantics;
	std::shared_ptr<BubbleCursor> bubbleCursor;

#ifndef NOLEGEND
	std::shared_ptr<Legend> legend;
#endif

    try {
        {//window creation
            int refresh = 60;
            bool fullscreen = true;
            if(argc == 2) {
                if(argv[1][0] == 'h') {
                    std::cout << APP_NAME << std::endl << "Arg list: widht, height, refresh, (0-windowed, 1-fullscreen)" << std::endl;
                    return 0;
                }
                /*width = 1280;
                height = 720;*/
                fullscreen = false;
            }

            if(argc == 5) {
                width = std::strtol(argv[1], 0, 10);
                height = std::strtol(argv[2], 0, 10);
                refresh = std::strtol(argv[3], 0, 10);
                fullscreen = std::strtol(argv[4], 0, 10) != 0;
            }

            if(!glfwInit())
                throw std::runtime_error("Error on glfwInit");

            GLFWmonitor* monitor = NULL;
            if(fullscreen) {
                monitor = glfwGetPrimaryMonitor();
                auto screen = glfwGetVideoMode(monitor);
                if(refresh == 0)
                    refresh = screen->refreshRate;
                glfwWindowHint(GLFW_REFRESH_RATE, refresh);
            }

            if(!(window = glfwCreateWindow(width, height, APP_NAME, monitor, NULL)))
                throw std::runtime_error("Error on glfwCreateWindow");
            glfwMakeContextCurrent(window);
            if(glewInit() != GLEW_OK)
                throw std::runtime_error("Error on glewInit");
            if(!GLEW_VERSION_4_3)
                throw std::runtime_error("OpenGL 4.3 is not supported");
			//TODO: seperate from main
#if 0
			double px;
			double py;
			glfwSetWindowUserPointer(window, mouseSemantics);
			glfwSetCursorPosCallback(window, mouseCallback);
			glfwSetCursorEnterCallback(window, cursorEnterCallback);
#endif
			//TODO: discuss this
            #if DEBUG
            Debug::enableWindowsDebug();
            #endif
        }

        scene.reset(new Scene(window));

        std::cout << "Compile shaders " << std::endl;
        {//compile shaders
            std::unique_ptr<Shader> shader;
            std::vector<std::pair<GLenum, std::string> > shaderPaths;
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/selectorDraw.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/selectorDraw.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("selectorDraw", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_COMPUTE_SHADER, "../shader/selectorGPU.cs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("selectorGPU", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/shadowMap.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/shadowMap.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("shadowMap", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/particleCPU.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/particleCPU.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("particleCPU", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/particleTessGPU.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_TESS_CONTROL_SHADER, "../shader/particleTessGPU.tc.glsl"));
            shaderPaths.push_back(std::make_pair(GL_TESS_EVALUATION_SHADER, "../shader/particleTessGPU.te.glsl"));
            shaderPaths.push_back(std::make_pair(GL_GEOMETRY_SHADER, "../shader/particleTessGPU.gs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/phongColorOmni.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("particleTessGPU", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/particleTessGPU.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_TESS_CONTROL_SHADER, "../shader/particleTessGPU.tc.glsl"));
            shaderPaths.push_back(std::make_pair(GL_TESS_EVALUATION_SHADER, "../shader/particleTessGPUOmni.te.glsl"));
            shaderPaths.push_back(std::make_pair(GL_GEOMETRY_SHADER, "../shader/OmniFirst.gs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/OmniFirst.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("particleTessGPUOmni", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_COMPUTE_SHADER, "../shader/particleSystemGPU.cs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("particleSystemGPU", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/generic.vert"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/generic.frag"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("generic", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/SV.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/phongTexture.frag"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("phongTexture", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/CMTexture.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_GEOMETRY_SHADER, "../shader/CMTexture.gs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/CMTexture.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("CMTexture", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/CMColor.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_GEOMETRY_SHADER, "../shader/CMColor.gs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/CMColor.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("CMColor", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/SV.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/CMReflect.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("CMReflect", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/SV.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/phongColor.frag"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("phongColor", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/OmniFirst.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_GEOMETRY_SHADER, "../shader/OmniFirst.gs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/OmniFirst.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("omniShadow", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/phongColorOmni.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/phongColorOmni.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("omniColor", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/phongTextureOmni.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/phongTextureOmni.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("omniTexture", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/SV.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/SVAmbientColor.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("SVambientColor", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/SV.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/SVAmbientTexture.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("SVambientTexture", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/SV.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_GEOMETRY_SHADER, "../shader/SV.gs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/SV.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("SV", shader);
            shaderPaths.clear();
            //TODO: use for advanced volumetric lightning
            /*shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/SV.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/OmniFirst.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("VolumeLightShadowPass", shader);
            shaderPaths.clear();*/
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/SV.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/volumetricFirstPass.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("volumetricFirstPass", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/SV.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/volumetricFirstPassSun.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("volumetricFirstPassSun", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/volumetricPostProcess.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/volumetricPostProcess.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("volumetricPostProcess", shader);
            shaderPaths.clear();
            shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/tessellation.vs.glsl"));
            shaderPaths.push_back(std::make_pair(GL_TESS_CONTROL_SHADER, "../shader/tessellation.tc.glsl"));
            shaderPaths.push_back(std::make_pair(GL_TESS_EVALUATION_SHADER, "../shader/tessellation.te.glsl"));
            shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/tessellation.fs.glsl"));
            shader.reset(new Shader(shaderPaths));
            scene->addShader("tessellation", shader);
            shaderPaths.clear();
			shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/mouseSprite.vs.glsl"));
			shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/mouseSprite.fs.glsl"));
			shader.reset(new Shader(shaderPaths));
			scene->addShader("mouseSprite", shader);
			shaderPaths.clear();
			shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/selectedOmni.vs.glsl"));
			shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/selectedOmni.fs.glsl"));
			shader.reset(new Shader(shaderPaths));
			scene->addShader("selectedOmni", shader);
			shaderPaths.clear();
			shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/textShader.vs.glsl"));
			shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/textShader.fs.glsl"));
			shader.reset(new Shader(shaderPaths));
			scene->addShader("textShader", shader);
			shaderPaths.clear();
			shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/textShader.vs.glsl"));
			shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/textShaderTextureExt.fs.glsl"));
			shader.reset(new Shader(shaderPaths));
			scene->addShader("textShaderTextured", shader);
			shaderPaths.clear();
			shaderPaths.push_back(std::make_pair(GL_VERTEX_SHADER, "../shader/bubbleSprite.vs.glsl"));
			shaderPaths.push_back(std::make_pair(GL_FRAGMENT_SHADER, "../shader/bubbleSprite.fs.glsl"));
			shader.reset(new Shader(shaderPaths));
			scene->addShader("bubbleSprite", shader);
			shaderPaths.clear();
        }
        std::cout << "Shader compilation finished" << std::endl;

        scene->init("../demo.dae");
        debug.reset(new Debug(scene.get(), APP_NAME));

        std::shared_ptr<SceneObject> sGPU(new SelectorGPU("selectorGPU", scene.get()));
        std::shared_ptr<SceneObject> sImp(new SelectorSimple("selectorSimple", scene.get()));
        std::shared_ptr<SceneObject> sQuad(new SelectorQuadTree("selectorQuad", scene.get()));
		std::shared_ptr<SceneObject> mouse(new MouseSemantics(scene.get() , MOUSE_MAX_MOVE, MOUSE_MIN_MOVE, MOUSE_CUTOFF, new Texture("../texture/pointer-hi.png",true)));
		bubbleCursor.reset(new BubbleCursor(scene.get(), new Texture("../texture/tpcircle.png", true)));

		mouse->setShader(scene->getShader("mouseSprite"));
		bubbleCursor->setShader(scene->getShader("bubbleSprite"));
		bubbleCursor->update(0);
		scene->setBubbleCursor(bubbleCursor);

        sGPU->reset();
        sImp->reset();
        sQuad->reset();

        scene->getRootNode()->addChild(sGPU);
        scene->getRootNode()->addChild(sImp);
        scene->getRootNode()->addChild(sQuad);
        scene->getRootNode()->addChild(mouse);

        mouseSemantics = std::dynamic_pointer_cast<MouseSemantics>(mouse);
        scene->setMouseSemantics(mouseSemantics);
        scene->setActiveSelector(std::dynamic_pointer_cast<SelectorSimple>(sImp));

#ifndef NOLEGEND
		legend.reset(new Legend(scene.get(), mouseSemantics.get(), new Texture("../texture/hudplaster.jpg", false), 8,30,0,15));
		legend->setShader(scene->getShader("textShader"));
		legend->setActiveSelector("Simple");
		scene->setLegend(legend);
#endif

    } catch(std::exception& ex) {
        std::cout << "Error on init: " << ex.what() << std::endl;
        glfwTerminate();
        return -1;
    } catch(...) {
        std::cout << "Error on init: Unknown exception received" << std::endl;
        glfwTerminate();
        return -1;
    }

    glfwSetWindowUserPointer(window, scene.get());
    //glfwSetCursorEnterCallback(window, cursorEnterCallback);

    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_TEXTURE_3D);

    //init effects
    scene->getLight()->reset();
    scene->getLight()->update(0);
    scene->getLight()->draw();

    {//static scene, no need to render
        std::shared_ptr<SceneObject> light;
        scene->getObject("Lamp", light);
        scene->remRender(light);
    }

    glViewport(0, 0, width, height);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    bool paused = false;
    bool running = true;
    double deltaT = 0.0;
    double lastTime = 0.0;
    while (!glfwWindowShouldClose(window) && glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS)
    {//restart
#if 0 //no animation in this project currently
        lastTime = 0;
        running = true;
        firstTimeCube = true;
        glfwSetTime(0.5);
        
        for(auto it = scene->begin(); it != scene->end(); ++it) {
            std::shared_ptr<SceneObject> object = Scene::get(it);

            object->setAnimationTime(0);
        }
#endif
    //main loop
    while (running && !glfwWindowShouldClose(window)) {
        double time = glfwGetTime();
        deltaT = time - lastTime;
        lastTime = time;

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        scene->getCamera()->update(deltaT); //debug

#if 0 //no animation in this project currently
        running = paused;
        //animate all objects, if not paused
        for(auto it = scene->begin(); !paused && it != scene->end(); ++it) {
            std::shared_ptr<SceneObject> object = Scene::get(it);

            //must be bitwise OR,
            //when false function would not be called
            running = running | object->animate(time);
        }
#endif
        
        //draw all objects
        for(auto it = scene->begin(); it != scene->end(); ++it) {
            std::shared_ptr<SceneObject> object = Scene::get(it);

            object->draw();
        }

        {//handle mouse semantics and object selection

			if (!bubbleCursor->drawingEnabled()) {

				size_t prevSelected;
				size_t lastSelected;
				std::shared_ptr<SelectorBase> activeSelector = scene->getActiveSelector();

				if (scene->getCamera()->movement()) {
					activeSelector->draw();
					activeSelector->update(0);
				}

				prevSelected = activeSelector->getLastSelected();
				activeSelector->pickObject(mouseSemantics->getMousePos());
				lastSelected = activeSelector->getLastSelected();
				mouseSemantics->updateDistanceState(activeSelector->getLastDistance());
				mouseSemantics->update(deltaT);
				mouseSemantics->draw();

				//highlight closest object
				if (prevSelected != lastSelected) {
					std::shared_ptr<SceneObject> object;
					std::shared_ptr<SceneObject> child;
					int idx = 0;
					if (prevSelected != 0xFFFF) { //deselect
						scene->getRender(prevSelected, object);
						object->setShader(scene->getShader("omniTexture"));
						while (object->getChild(idx, child)) {
							child->setShader(scene->getShader("omniTexture"));
							idx++;
						}
					} //TODO: shaders should be set using assimp.cpp define
					idx = 0;
					if (lastSelected != 0xFFFF) { //select
						scene->getRender(lastSelected, object);
						object->setShader(scene->getShader("selectedOmni"));
						while (object->getChild(idx, child)) {
							child->setShader(scene->getShader("selectedOmni"));
							idx++;
						}
					}
				}
			}
		}

		bubbleCursor->draw();

#ifndef NOLEGEND
		legend->update(deltaT);
		legend->draw();
#endif

        //handle pausing
        paused = paused | (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS);
        paused = paused & (glfwGetKey(window, GLFW_KEY_ENTER) != GLFW_PRESS);

        //fast-forward is disabled
#if 0
        if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
            static double lastSkip = time;
            if(time - lastSkip < 0)
                lastSkip = time; //handle restart
            if(time - lastSkip > 0.1) {
                if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
                    time = time + 100*deltaT;
                else
                    time = time - 200*deltaT;

                glfwSetTime(time);
                for(auto it = scene->begin(); it != scene->end(); ++it) {
                    std::shared_ptr<SceneObject> object = Scene::get(it);

                    object->setAnimationTime(time);
                }
                lastSkip = time;
            }
        }
#endif
        auto error = glGetError();
        if(error != GL_NO_ERROR)
            std::cerr << "OpenGL Error: " << glewGetErrorString(error) << std::endl;

        glfwSwapBuffers(window);
        glfwPollEvents();
        running = running && glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS;//can be logical AND, if false we exit anyway
		debug->update(time, deltaT);
    } //main loop
    } //restart

    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}

