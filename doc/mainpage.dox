/**
\mainpage ComputerGraphik TU WIEN

\tableofcontents

\section selekt Project Selektron 09.06.2015

The project was done for the course Visualisierung 2 VU - MSC.
\author Adam Papp 1327381 066 932
\author Felix König 0917104 066 932

\subsection selektdesc Description in the Paper

As current hardware allows people to utilize high-resolution displays that can portray a variety of small objects within a game or a visual simulation it becomes more and more difficult to select the elements via a mouse in such applications. It is a comprehensible thought to try to simplify the selection of small elements via adapting the speed of the mouse movement. The included user study confirms higher accuracy when semantic 3D pointing is used. The technique utilizes the information about the location and size of possible targets in the scene and their distance to the current cursor position in order to adapt the speed of mouse movement.

\sa http://dl.acm.org/citation.cfm?id=1375714.1375755 - Semantic Pointing for Object Picking in Complex 3D Environments

\subsection selectImp Implementation 

\subsubsection selectSum Summary

The implementation sticks very close to the suggested method which was described in the paper but we also implemented an alternative solution. The idea in the paper was to calculate the distance of the nearest object to the mouse position using a spacial hierarchy data structure, a QuadTree to be precise. Every pickable object is rendered into a Id Buffer that is a separate texture in our case.  The result is a screen space representation of all pickable objects. The task is now to determine the nearest pixel to the mouse that is nonzero, meaning it lies on the nearest pickable object. The paper suggests to represent the whole screen space texture in a quadtree and then query the quadtree for the distance information. In addition to using a quadtree, we also implemented a compute shader that calculates the distance on the GPU. The compute shader outperforms the quadtree approach. Please read the following sections for a more detailed describtion of our implemented methods.

\subsubsection selektmouse Semantic Object Picking
\copydetails MouseSemantics

\subsubsection selektlegend Information Visualization
\copydetails Legend

\subsubsection selektbubble Random Bubble Cursors
\copydetails BubbleCursor

\subsubsection selektquad QuadTree Based Solution of Object Selection
\copydetails SelectorQuadTree

\subsubsection selektgpu Compte Shader Based Solution of Object Selection
\copydetails SelectorGPU

\subsection selekttest Results

In this subsection, the results of the different selector algorithms are compared. The SelectorSimple was written to compare it's speed with the SelectorQuadTree::update(). It must be noted, that these tests were done separate from the main render loop and each function was executed 100 times. The SelectorGPU seems to be faster by this table then the SelectorSimple, but when running in the main loop in this application, there are more processing done on the GPU then on the CPU, it is slower with window size 256 x 256. For static objects the SelectorQuadTree has an impressive speed increase. The first table shows the speed results of the different functions separate from the rendering loop, the second table shows speed results of the selection methods in the rendering loop.

The implementation was tested on the following configuration: Intel Core i7-4700HQ 2.40 GHz * 4, RAM 8 GB, NVIDIA GTX 860M.

<table border="1">
 <tr BGCOLOR="#99CCFF">
  <th>Pixel Count</th>
  <th colspan="3">256 x 256</th>
  <th colspan="3">512 x 512</th>
  <th colspan="3">1024 x 512</th>
  <th colspan="3">1920 x 1080(*) / 2048 x 1024(**)</th>
 </tr>
 <tr BGCOLOR="#99CCFF">
  <th>Time</th>
  <th>Min (ms)</th> <th>Avg (ms)</th> <th>Max (ms)</th>
  <th>Min (ms)</th> <th>Avg (ms)</th> <th>Max (ms)</th>
  <th>Min (ms)</th> <th>Avg (ms)</th> <th>Max (ms)</th>
  <th>Min (ms)</th> <th>Avg (ms)</th> <th>Max (ms)</th>
 </tr>
 <tr BGCOLOR="#FFFFFF" align="center">
  <td>SelectorSimple::pickObject() **</td>
  <td>0.496078</td> <td>0.578992</td> <td>4.31417</td> <!--256x256-->
  <td>0.710333</td> <td>0.806508</td> <td>4.38345</td> <!--512x512-->
  <td>0.957944</td> <td>1.055110</td> <td>4.64175</td> <!--1024x512-->
  <td>2.066840</td> <td>2.197720</td> <td>6.86467</td> <!--2048x1024-->
 </tr>
 <tr BGCOLOR="#D8D8D8" align="center">
  <td>SelectorQuadTree::update() *</td>
  <td>0.162508</td> <td>0.176214</td> <td>0.809546</td> <!--256x256-->
  <td>0.620951</td> <td>0.668921</td> <td>2.419660</td> <!--512x512-->
  <td>0.972053</td> <td>1.031520</td> <td>3.057290</td> <!--1024x512-->
  <td>4.439880</td> <td>4.952960</td> <td>12.06710</td> <!--1920x1080-->
 </tr>
 <tr BGCOLOR="#FFFFFF" align="center">
  <td>SelectorQuadTree::pickObject() *</td>
  <td>0.087241</td> <td>0.0921123</td> <td>0.1552380</td> <!--256x256-->
  <td>0.072701</td> <td>0.0756049</td> <td>0.0957944</td> <!--512x512-->
  <td>0.183891</td> <td>0.1924740</td> <td>0.2416240</td> <!--1024x512-->
  <td>0.137704</td> <td>0.1419980</td> <td>0.2065570</td> <!--1920x1080-->
 </tr>
 <tr BGCOLOR="#D8D8D8" align="center">
  <td>SelectorGPU::pickObject() **</td>
  <td>0.0474696</td> <td>0.053632</td> <td>0.189451</td> <!--256x256-->
  <td>0.0679969</td> <td>0.0857317</td> <td>0.41183</td> <!--512x512-->
  <td>0.104347</td> <td>0.140283</td> <td>0.455879</td> <!--1024x512-->
  <td>0.32801</td> <td>0.346553</td> <td>0.488808</td> <!--2048x1024-->
 </tr>
 <tr BGCOLOR="#FFFFFF" align="center">
  <td>SelectorBase::draw() *</td>
  <td>N/A</td> <td>N/A</td> <td>N/A</td> <!--256x256-->
  <td>N/A</td> <td>N/A</td> <td>N/A</td> <!--512x512-->
  <td>N/A</td> <td>N/A</td> <td>N/A</td> <!--1024x512-->
  <td>0.0372059</td> <td>0.0443006</td> <td>0.217248</td> <!--1920x1080-->
 </tr>
 <tr BGCOLOR="#D8D8D8" align="center">
  <td>glGetTexture() **</td>
  <td>0.0115466</td> <td>0.0130007</td> <td>0.0534567</td> <!--256x256-->
  <td>0.0363506</td> <td>0.0380997</td> <td>0.168496</td> <!--512x512-->
  <td>0.0684246</td> <td>0.0744374</td> <td>0.34084</td> <!--1024x512-->
  <td>0.472985</td> <td>0.520775</td> <td>1.6037</td> <!--2048x1024-->
 </tr>
</table>

<table border="1">
 <tr BGCOLOR="#99CCFF">
  <th>Pixel Count</th>
  <th>256 x 256</th>
  <th>512 x 512</th>
  <th>1024 x 512</th>
  <th>1920 x 1080(*) / 2048 x 1024(**)</th>
 </tr>
 <tr BGCOLOR="#99CCFF">
  <th>Frames</th>
  <th>Avg (FPS)</th>
  <th>Avg (FPS)</th>
  <th>Avg (FPS)</th>
  <th>Avg (FPS)</th>
 </tr>
 <tr BGCOLOR="#FFFFFF" align="center">
  <td>SelectorSimple::pickObject() **</td>
  <td>220</td> <!--256x256-->
  <td>210</td> <!--512x512-->
  <td>210</td> <!--1024x512-->
  <td>180</td> <!--2048x1024-->
 </tr>
 <tr BGCOLOR="#FFFFFF" align="center">
  <td>SelectorQuadTree::pickObject() *</td>
  <td>256</td> <!--256x256-->
  <td>256</td> <!--512x512-->
  <td>256</td> <!--1024x512-->
  <td>256</td> <!--1920x1080-->
 </tr>
 <tr BGCOLOR="#D8D8D8" align="center">
  <td>SelectorGPU::pickObject() **</td>
  <td>210</td> <!--256x256-->
  <td>205</td> <!--512x512-->
  <td>205</td> <!--1024x512-->
  <td>198</td> <!--2048x1024-->
 </tr>
</table>

\section mini Project MiniKrieg 29.01.2015

The project was done for the course Echtzeitgraphik VU - MSC.
\author Adam Papp 1327381 066 932
\author Felix König 0917104 066 932

\subsection minidesc Description

The project was to create a real-time animation with effects using at least OpenGL 3.3. In the demo scene, there is a tank placed in a museum room. The tank is moving forward and leaves a trail behind, here the ParticleSystemCPU is used. The %Tank shoots bullets in 360 degrees. These bullets are moved by the ParticleSystemGPU and rendered using tessellated spheres with LOD and animation using displacement mapping with Tessellation. While the tank and it's effects are shown, Omni Direction Shadow Mapping is used with ShadowOmni. Afterwards, the shadows are rendered using ShadowVolumes. Then the CubeMap effect can be seen on the ellipsoid. Finally, the camera will look outside of the room showing the VolumetricLight effect. Before the demo is finished, the shadow of the tank is shown once more using ShadowVolumes.

\subsection ministart Starting the Demo

The demo can be started in full screen mode in 1920 * 1080 with 60Hz refresh rate by double clicking on the exe. If it is started from command line, it must be started from the bin directory. In this case arguments can be used in the following format: 1920(w) 1080(h) 60(r) 0(windowed).

\note CMake must be set on configuration either to %Debug or Release. This can be done in the CMake-GUI at the CMAKE_CONFIGURATION_TYPES variable.

The shader and texture folders are placed next to the bin folder in the zip file. The exe finds files using the following structure:
 - {any directory}/MiniKrieg.exe
 - shader/shader.vert
 - texture/texture.bmp
 - demo.dae

Controls:
 - ESC - exit
 - SPACE - pause
 - ENTER - continue
 - A - rewind
 - D - fast-forward
 - MOUSE - camera control (when paused)
 - ARROWS - camera control (when paused)
 - F2 - Frame time in window title
 - F3 - Wire-Frame
 - F4 - %Texture Mag filter
 - F5 - %Texture Min filter
 - F6 - Change tessellation level(0-dynamic)

\section steel Project SteelWorms 27.06.2014

The project was done for the course Computergraphik UE - BSC.
\author Adam Papp 1327381 066 932

\subsection steeldesc Description

The project was to write a game, with effects using OpenGL 3.3. The game can be started in full screen mode with 60Hz refresh rate by double clicking on the exe. The resolution is always 800 * 600. If it is started from command line, it must be started from the bin directory. In this case a dummy argument like {windowed} can be used to start in windowed mode. The game is only a two-player version, but the algorithm can handle any number of player. The shader and texture folder are placed next to the bin folder. The exe finds files using the following structure:
 - {any directory}/SteelWorms.exe
 - shader/shader.vert
 - texture/texture.bmp
 
\note The Assimp Loader sometimes fails to load the Tiger model. Revision 340b94f9a54132f71f72331b3787b0c9cdf2de88 was used with MSVC10.
 
\subsection steelgame Game Play

The game is a turn-based artillery strategy game, with ShadowMap and ParticleSystemCPU effects. In one turn only one tank can be moved. A turn ends in one minute or when the bullet reaches the terrain. After the bullet is shot, the tank can not move. A bullet can be shot by holding the SPACE key. The longer it is hold the bigger power it will be used to shoot. There is water under and above the track. If a tank reaches water it's health will decrease. If a tank goes off track it dies. When only one tank lives, the game ends and the result is written out in the console. The user inputs are handled by the GLFW library. The camera can be moved either with the keyboard or with the mouse. The keyboard controls are the arrows for moving and 1 to zoom out and 2 to zoom in. The right mouse button can be used to move the camera, the left mouse button can be used to rotate the camera and the middle mouse button is to zoom. A tank can be moved with the WASD buttons. The turret and the gun can be moved by the IJKL buttons. A bullet can be shot with the SPACE button, the power of the shoot depends on the amount of SPACE. A turn can be ended with the ENTER key. The ESC key can be used to exit the game.

\section ext External Libraries

 - GLFW - http://www.glfw.org/
 - GLEW - http://glew.sourceforge.net/
 - GLM  - http://glm.g-truc.net/
 - Assimp - http://assimp.sourceforge.net/
 - OpenCV - https://www.willowgarage.com/pages/software/opencv
 - FreeType - https://www.freetype.org/

*/
