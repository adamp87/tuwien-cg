# README #

This repository is the work done for three computer graphics related courses at TU Wien.

master is in documented state, but only Selektron compiles.

Generated doxygen documentation: https://bitbucket.org/adamp87/tuwien-cg/downloads/Doxygen.zip

Tag: SteelWorms

* Author: Adam
* Brief: Write a game with lighting, and simple special effects with OpenGL > 3
* Video: https://www.cg.tuwien.ac.at/courses/CG23/HallOfFame/2014/video/steelworms.avi

Tag: MiniKrieg

* Author: Adam, Felix
* Brief: Implement advanced real-time effects with OpenGL > 3
* Video: https://www.cg.tuwien.ac.at/courses/Realtime/HallOfFame/2014/video/minikrieg.mp4

Tag: Selektron

* Author: Adam, Felix
* Brief: Implement mouse interaction
