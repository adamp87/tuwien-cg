/*! \file gameLogic.hpp
 *  \brief %GameLogic of SteelWorms
 *  \author Adam
*/

#ifndef GAMELOGIC_HPP
#define GAMELOGIC_HPP

#include <vector>
#include <memory>

#include "sceneObject.hpp"

class Tank;
class Scene;
class Model;
class Shader;
class Bullet;
class Texture;
class SceneObject;

//! Implements the game logic of SteelWorms.
/*!
 * \brief The GameLogic class
 * \author Adam
 * \details
 * \par Terrain
 * \parblock
 * A triangle is built up from 3 pixels, so 4 neighboring pixel represents two triangles.
 * The pixel values(scaled to 0..1) are stored in a lookup table.
 * With the function getHeight() it is possible to get the height value for any x and y coordinate.
 * The idea of the algorithm which computes the heights is from the
 * "Introduction to 3D game programming with DirectX 9.0" book page 228-231
 * written by "Rod Lopez, Frank D. Luna".
 * The algorithm computes the position of the 4 pixel values surrounded by
 * the given x and y coordinate and then interpolates the height value from the corresponding coordinates.
 * \endparblock
 * \par Bullet
 * \parblock
 * The trajectory of a bullet is computed by the following equation:
 *
 * $z = z_0 + d \tan \theta - \frac {gd^2}{2(v\cos\theta)^2}$
 *
 * This equation computes the height of the bullet.
 * The value v in the equation is the velocity.
 * The longer the SPACE key is held to shoot, the higher this value will be.
 * The value d is the distance from the shoot point.
 * Theta is the angle, which is currently 45 degrees. Value g is the gravity, set to 9.81.
 * The collision is detected by comparing the height of the bullet with the
 * height of the terrain at the x and y coordinate of the bullet.
 * If the difference is negative, then the bullet has reached the ground.
 * Then the distance of the impact point and all the tanks are compared.
 * If the distance is under a threshold, then the health of the actual
 * tank is decreased according to the distance.
 * \sa Wikipedia (Ballistic_trajectory)
 * \endparblock
 * \par Tank
 * \parblock
 * Each player controls one tank.
 * The color of the tank shows the health of a tank.
 * By controlling the turret and the gun the shooting direction
 * and angle can be modified.
 * Holding the space longer will make the bullet to fly further.
 * The closer the bullet reaches the center of an other tank the more damage it does.
 * The health of the tanks will decrease if they are in
 * standing in water in they current turn.
 * The deeper the water the more they loose health.
 * \endparblock
 * \par Transparency
 * To achieve transparency, a water surface is constructed.
 * The Terrain class is used to display the water.
 * It is textured with 0.5 transparency.
 * The tank trail is rendered using ParticleSystemCPU, which is sorting
 * the particles based on the camera distance.
 * It is rendered before the water to see it under the water.
 * The track's transparency is decreased with time until it reaches zero.
 * \parblock
 * \endparblock
 */
class GameLogic : public SceneObject
{
public:
    //! Implements terrain height query
    class Terrain : public SceneObject{
    public:
        Terrain(size_t dimX,
       size_t dimY,
       float cellSize,
       const std::vector<float>& heights,
       const std::string& name,
       Scene* scene = 0, Model* model = 0, Shader* shader = 0);
        virtual ~Terrain() {}

        float getHeight(float x, float y);

    private:
        size_t dimX;
        size_t dimY;
        float cellSize;
        std::vector<float> heights;

        float lerp(float a, float b, float t);
    };

public:
    GameLogic(Scene* scene);
    ~GameLogic();

    void update(double deltaT);
    void addTank(std::shared_ptr<SceneObject> tank);

    bool isGameFinished() const { return gameFinished; }
    size_t getWinner() const { return activeTank; }
    std::shared_ptr<Terrain> getTerrain() const { return terrain; }

private:
    class Bullet;

    double turnTime;
    size_t activeTank;
    bool gameFinished;
    std::unique_ptr<Model> waterModel;
    std::unique_ptr<Model> terrainModel;
    std::unique_ptr<Texture> waterTexture;
    std::shared_ptr<Bullet> bullet;
    std::shared_ptr<Terrain> water;
    std::shared_ptr<Terrain> terrain;
    std::vector<std::shared_ptr<Tank> > tanks;
};

#endif //GAMELOGIC_HPP
