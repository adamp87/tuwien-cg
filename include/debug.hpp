/*! \file debug.hpp
 *  \brief OpenGL debug and performance
 *  \author Adam
*/

#ifndef DEBUG_HPP
#define DEBUG_HPP

#include <string>

class Scene;
class Shader;
class Texture;

//! The switches to experiment OpenGL performance and quality are implemented here.
class Debug {
public:
    Debug(Scene* scene, const char* windowTitle);
    ~Debug();

    void update(double time, double deltaT);

    static void enableWindowsDebug();

private:
    Scene* scene;

    size_t frameCount;
    double minDeltaT;
    double maxDeltaT;
    double lastTime;
    double lastKeyTime;
    double lastTitleTime;

    int minFilterMode;
    int magFilterMode;
    bool frameTimeOn;
    bool wireFrameOn;
    bool transparencyOn;
    std::string windowTitle;
};

#endif //DEBUG_HPP
