/*! \file assimpLoader.hpp
 *  \brief %Scene detail loader
 *  \author Adam
*/

#ifndef ASSIMPLOADER_HPP
#define ASSIMPLOADER_HPP

#include <glm/mat4x4.hpp>

#include <assimp/mesh.h>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>

#include <string>
#include <memory>

class Scene;
class Model;
class SceneObject;

//! The class which is responsible for loading all the details to build up a scene.
/*!
 * \brief The AssimpLoader class
 * \author Adam
 * \sa Scene
 */
class AssimpLoader {
public:
    AssimpLoader(const std::string& scenePath, Scene* scene);

    static void loadModel(const aiMesh* mesh, std::unique_ptr<Model>& model);
    static void selectAdjacent(const aiMesh* mesh, std::vector<unsigned int>& indices);

private:
    Scene* scene;
    const aiScene* iscene;
    Assimp::Importer importer;

    glm::vec3 aiVec2glmVec(const aiColor3D& v);
    glm::vec4 aiVec2glmVec(const aiColor4D& v);
    glm::mat4 aiMat2glmMat(const aiMatrix4x4& m);
    void nextNode(aiNode* iparent, std::shared_ptr<SceneObject> parent);
};

#endif //ASSIMPLOADER_HPP
