/*! \file shadowOmni.hpp
 *  \brief OmniShadow effect
 *  \author Felix, Adam
*/

#ifndef SHADOWOMNI_HPP
#define SHADOWOMNI_HPP

#include <vector>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include "light.hpp"

//! Implements the Omni Direction Shadow effect.
/*!
* \author Felix
* \details The Sun is a derivation of the Light class which is itself a SceneObject.
* In addition to the methods from SceneObject, a Sun has methods that use different shadow mapping techniques.
* The method drawOmni supports Omnidirectional Shadow mapping where every SceneObject is rendered into a cubemap texture.
* This texture will then be used for the shadow map lookup. In addition, we implemented Shadow volumes and therefore 2 renderpasses are used.
* The method ambientPass and SVPass compute the shadow volumes using each SceneObject in the 2 seperate renderpasses.
* For each seperate renderpass, effectobjects are used that are passed onwards to the individual SceneObjects that will be rendered.
* Framebuffer Objects are used for storing different shadow textures.
* \todo fix documentation
*/
class ShadowOmni : public Light
{
public:
    ShadowOmni(const std::string& name, Scene* scene = 0, Model* model = 0, Shader* shader = 0, const glm::mat4& modelMatrix = glm::mat4(1));
    virtual ~ShadowOmni();

    void draw() const;
    void reset();
    void update(double deltaT);
    void setShader(Shader* val);

    GLuint getDepthTexture() const { return depthTexture; }

private:
    class EffectObjectCube;
    class EffectObjectCubeInstanced;

    GLuint frameBuffer;
    GLuint depthTexture;
    GLenum cubeFaces[6];
    std::vector<glm::mat4> viewMatrices;

    void setViewMatrices();
    void setUpDepthTexture();
};

#endif //SHADOWOMNI_HPP
