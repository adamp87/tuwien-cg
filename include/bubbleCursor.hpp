/*! \file bubbleCursor.hpp
 *  \brief Display circles to show bubble cursor
 *  \author Felix
*/

#ifndef BUBBLECURSOR_HPP
#define BUBBLECURSOR_HPP

#include <glm/vec2.hpp>
#include <glm/glm.hpp> 
#include <glm/gtx/epsilon.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "sceneObject.hpp"
#include "shader.hpp"
#include "texture.hpp"

#define MOUSE_MIN_MOVE 0.3
#define MOUSE_MAX_MOVE 3.5
#define MOUSE_CUTOFF 200
#define BUBBLE_POINTS 50

//! Visualization of multiple circles that are randomly placed on the screen space, the radius of the circle is as big as the distance to the nearest tank.
/*!
* \details The class BubbleCursor implements the rendering of circles (=bubbles in the paper) that are randomly placed on the screen with the radius as big as the distance to the nearest object. 
*
* In the method generateRandomPopulation() 50 points are generated at random that are distributed across the screen space.
* These 2d points are generated using two Gaussian distributions centered at windowWidth/2 and windowHeight/2 (middle of the screen), with a sigma value of windowWidth/5 and windowHeight/5.
* The objects used were: 
* 
* \code
* std::default_random_engine generator(time(NULL));
* std::normal_distribution<double> distributionY(height / 2.0f, height / 5.0f);
* std::normal_distribution<double> distributionX(width / 2.0f, width / 5.0f);
* \endcode
*
* for further details see the function generateRandomPopulation().
* 
* In the draw() function the positions of those points are then forwarded to the pickObject method in the class SelectorQuadTree where the distance to the closest object (for each point separately) is computed. A point sprite renderer is then used to draw a transparent circle onto the screen. The radius of the circle is scaled to conicide with the distance to the nearest object (in pixels).
*
* The user can enable/disable the display of these bubbles with the key B, everytime the display of the bubbles is disabled and enabled another random population is created. Rendering all bubbles consumes a huge amount
* of CPU load since for each of the 50 points the quadtree needs to be queried.
*
* The result looks like:
*
* \image html bubble1scaled.png "bubble cursor"
* \image html bubble2scaled.png "bubble cursor"
*
* \author Felix Koenig
*/
class BubbleCursor : public SceneObject {
private:
	glm::vec2 latestPos;
	int distance;
	GLFWwindow *window;
	GLuint vboPositions;
	Texture *texture;
	std::vector<glm::vec2> population;
	bool enableDrawing;

public:
	BubbleCursor(Scene* scene, Texture* texture);
	virtual ~BubbleCursor();
	
	void draw();
	void setShader(Shader *shader);
	void update(double deltaT);

	void updateDistanceState(unsigned int _distance);
	void generateRandomPopulation();
	void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode);

	bool drawingEnabled();
	unsigned int getDistance() { return distance; };
};

#endif //MOUSESEMANTICS_HPP
