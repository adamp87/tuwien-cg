/*! \file camera.hpp
 *  \brief %Camera handling
 *  \author Adam, Felix
*/

#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include "sceneObject.hpp"

struct GLFWwindow;

//! Implements the camera movement with user input.
/*!
 * \author Adam, Felix
 * \sa http://www.opengl-tutorial.org/beginners-tutorials/tutorial-6-keyboard-and-mouse
*/
class Camera : public SceneObject {
public:
    Camera(const std::string& name,
           Scene* scene,
           const glm::vec3& eye = glm::vec3(0, -10, 5),
           float vAngle = -3.14f,
           float hAngle = 3.14f,
           float fov = 60.0f,
           float near = 0.01f,
           float far = 20.0f);

    Camera(const std::string& name,
           Scene* scene,
           glm::vec3& pos,
           glm::vec3& up,
           glm::vec3& eye,
           float fov,
           float near,
           float far);

    virtual ~Camera();

    void draw() const;
    bool animate(double time);
    void update(double deltaT);
    void setShader(Shader*) {}

    bool movement();
    void initializeLookupVector();
    float getNearClip() { return nearClip; }
    float getFarClip() { return farClip; } 
    void setViewMatrix(const glm::mat4& modelMatrix);
    glm::mat4 getViewMatrix() const { return viewMatrix; }
    glm::mat4 getProjectionMatrix() const { return projectionMatrix; }

protected:
    double cursorX;
    double cursorY;

    float fov;
    float farClip;
    float nearClip;
    glm::vec3 up;
    glm::vec3 eye;
    glm::vec3 pos;
    glm::mat4 viewMatrix;
    glm::mat4 projectionMatrix;

private:
    bool moved;
    bool initState;
};

#endif //CAMERA_HPP
