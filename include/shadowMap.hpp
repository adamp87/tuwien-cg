/*! \file shadowMap.hpp
 *  \brief Shadow Map effect
 *  \author Adam
*/

#ifndef SHADOWMAP_HPP
#define SHADOWMAP_HPP

#include <vector>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include "light.hpp"

//! Implementation of the shadow map effect.
/*!
 * \author Adam
 * \details The scene is being rendered from the light source into a FrameBuffer, which has a texture attached to it.
 *          The texture will contain at each position the distance from the light source.
 *          When rendering the scene using the camera, the same distance is computed and compared with the one in the texture.
 *          If it is less, then the fragment is in shadow. To improve the quality of the shadows, PCF is being used.
 *          This is achived by using sampler2Dshadow in the fragment shader.
 *          The comparison must be enabled in texture parameters.
 *          To requesting a value from the sampler2Dshadow,
 *          the x and y texture coordinates and the light distance in the z are given.
 *          The sampler will compare also neighboring texels and gives back a value between 0 and 1,
 *          the ratio of the texels, which were less then the requested z.
 * \sa http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-16-shadow-mapping/
*/
class ShadowMap : public Light
{
public:
    ShadowMap(const std::string& name, Scene* scene = 0, Model* model = 0, const glm::mat4& modelMatrix = glm::mat4(1));
    virtual ~ShadowMap();

    void draw() const;
    void reset();
    void update(double deltaT);
    void setShader(Shader* val);

    glm::mat4 getDepthMVP() const { return depthMVP; }
    glm::mat4 getDepthBiasMVP() const { return depthBiasMVP; }
    GLuint getDepthTexture() const { return depthTexture; }

private:
    class EffectObject;

    GLuint frameBuffer;
    GLuint depthTexture;
    glm::mat4 depthMVP;
    glm::mat4 depthBiasMVP;
};

#endif //SHADOWMAP_HPP
