/*! \file light.hpp
 *  \brief %Light handling
 *  \author Adam
*/

#ifndef LIGHT_HPP
#define LIGHT_HPP

#define USE_OMNI_SHADOW
//#define USE_SHADOW_MAP

#include <glm/vec3.hpp>
#include "sceneObject.hpp"

//! Base class for lighting
/*!
*/
class Light : public SceneObject
{
public:
    Light(const std::string& name, Scene* scene = 0, Model* model = 0, const glm::mat4& modelMatrix = glm::mat4(1))
        : SceneObject(name, scene, model, modelMatrix)
    {
    }

    virtual ~Light() { }

    glm::vec3 getPower() const { return power; }
    glm::vec3 getDiffuse() const { return diffuse; }
    glm::vec3 getAmbient() const { return ambient; }
    glm::vec3 getSpecular() const { return specular; }
    glm::vec3 getPosition() const { return glm::vec3(getGlobalModelMatrix() * glm::vec4(0, 0, 0, 1)); }

    void setPower(const glm::vec3& val) { power = val; }
    void setDiffuse(const glm::vec3& val) { diffuse = val; }
    void setAmbient(const glm::vec3& val) { ambient = val; }
    void setSpecular(const glm::vec3& val) { specular = val; }

protected:
    glm::vec3 power;
    glm::vec3 diffuse;
    glm::vec3 ambient;
    glm::vec3 specular;
};

#endif //LIGHT_HPP
