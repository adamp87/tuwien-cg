/*! \file selectorSimple.hpp
 *  \brief Mouse selection handling
 *  \author Adam
*/

#ifndef SELECTORSIMPLE_HPP
#define SELECTORSIMPLE_HPP

#include <string>
#include <vector>

#include "selectorBase.hpp"

//! Implements a simple object selection
/*!
 * \details First it copies the complete pick buffer,
 * then in a window centered at the mouse position, it
 * searches the closest object.
 * \author Adam
*/
class SelectorSimple : public SelectorBase {
public:
    SelectorSimple(const std::string& name, Scene* scene);
    ~SelectorSimple();

    void changeMode(); //!< debug, change window size
    void pickObject(const glm::ivec2& mouse); //!< Select closest object to mouse

private:
    int mode; //!< debug, help to set window
    int windowX; //!< number of pixels in window
    int windowY; //!< number of pixels in window
    std::vector<GLshort> screen; //!< Buffer to store texture on CPU
};

#endif //SELECTORSIMPLE_HPP
