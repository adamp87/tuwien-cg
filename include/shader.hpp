/*! \file shader.hpp
 *  \brief %Shader handling
 *  \author Adam
*/

#ifndef SHADER_HPP
#define SHADER_HPP

#include <string>
#include <vector>
#include <GL/glew.h>

//! Encapsulates a shader program.
class Shader {
public:
    Shader(const std::vector<std::pair<GLenum, std::string> >& shaderPaths);
    ~Shader();

    void use() const { glUseProgram(program_handle); }
    GLuint programHandle() const { return program_handle; }

    static void loadTextFile(const std::string& filename, std::vector<char>& data);

private:
    GLuint program_handle;

    class ShaderObj;
    friend class ShaderObj;
    static GLuint compileShader(const std::vector<char>& shaderSrc, GLenum shaderType);
};

#endif //SHADER_HPP
