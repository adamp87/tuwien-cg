/*! \file selectorQuadTree.hpp
 *  \brief Mouse selection handling
 *  \author Adam
*/

#ifndef SELECTORQUADTREE_HPP
#define SELECTORQUADTREE_HPP

#include <string>
#include <vector>

#include "selectorBase.hpp"

//! Implements the QuadTree based object selection
/*!
 * \details
 * The SelectorQuadTree class implements a quadtree based approach based on
 * http://dl.acm.org/citation.cfm?id=1375714.1375755 .
 * In the scene there are objects that can be selected.
 * These objects are rendered, in screen space, into a texture, which holds the semantic information.
 * The datatype of this texture is 16bit integer, to store the render id of the objects.
 * After rendering with the draw() function, the quadtree is built up by the update() function.
 * In the first step, the complete texture is copied from GPU to CPU.
 * Then each pixel, which contains an object id, is used to build up a quadtree.
 * The quadtree is selected to be a fix size of 2048*2048 pixels.
 * The SelectorQuadTree::Node objects store the childs as indices of the SelectorQuadTree::nodes vector container.
 * The tree is built up from top to bottom, where an occupied pixel will be present in the bottom level.
 * The following pseudocode demonstrates the build up process:
 * \code
 * //the build up process of the quadtree
 *
 * for(y to screensize.y)
 *  for(x to screensize.x)
 *   id = texture[x, y]
 *   if(id == 0)
 *    continue
 *
 *   //size = 2048
 *   shift = 10
 *   bitmask = 0x400
 *   node = root;
 *   do
 *    //decide in which child the pixel should be placed to
 *    //example: Assume x is 1024, then it's bitsample is 0x400.
 *    //         In the top level it is masked with 0x400 and the result will be 0x400,
 *    //         which is then shifted with 10 bits and will be 1, so the high child will be selected in the X axis.
 *    //         At level 9 it is masked with 0x200 and it will be zero, then the low child is selected.
 *    //         In this example all the lower levels will be placed in the low child.
 *    childX = (x & bitmask) >> shift
 *    childY = (y & bitmask) >> shift
 *    child = node.child(childX, childY)
 *
 *    if child not in container
 *     create new child and add to nodes
 *
 *    child.id = id
 *    child.updateBB(x, y)
 *
 *    //move one level down
 *    node = child
 *    shift = shift - 1
 *    bitmask = bitmask >> 1
 *   while(bitmask != 0)
 * \endcode
 * Since the scene contains only static objects, it is sufficient to build up the tree only when the camera is moved.
 * The quadtree then enables the fast selection of these static objects using the pickObject() function.
 * The following pseudocode demonstrates the travelsal of the quadtree:
 * \code
 * //find closest object to mouse in quadtree
 *
 * //initialization
 * id = 0xFFFF
 * minD = maxD = FLOAT_MAX
 * nextLevelQueue.push(ROOT)
 *
 * while(!nextLevelQueue.empty())
 *  //prepare loop on one level
 *  queue = nextLevelQueue
 *  nextLevelQueue.clear()
 *  minD = maxD = FLOAT_MAX
 *
 *  while(!queue.empty())
 *   Node = queue.pop();
 *
 *   for i 1..4
 *    if(!node.hasChild(i))
 *     continue
 *
 *    //get minimum and maxmimum distance between child node and mouse
 *    //the distance is computed from the four corner points of the bounding box
 *    [nodeMin nodeMax] = node.child[i].getDistance(mouse)
 *
 *    //minimum distance of node is larger then smallest maximum distance of this level
 *    if(maxD < nodeMin)
 *     continue;
 *
 *    //add child to next level loop
 *    nextLevelQueue.push(node.child[i])
 *    //update minimum distance
 *    minD = std::min(nodeMin, minD);
 *    //update smallest maximum distance
 *    maxD = std::min(nodeMax, maxD);
 *    //store id
 *    if minD is updated
 *     id = node.id
 *
 * \endcode
 * \author Adam
*/
class SelectorQuadTree : public SelectorBase {
public:
    SelectorQuadTree(const std::string& name, Scene* scene);
    ~SelectorQuadTree();

    void changeMode(); //!< debug, nothing
    void update(double deltaT); //!< build up quadtree
    void pickObject(const glm::ivec2& mouse); //!< select closest object to mouse

private:
    class Node;

    int maskBit; //!< 2^maskbit = treeSize
    int treeSize; //!< size of tree
    std::vector<Node> nodes; //!< container to store quadtree nodes
    std::vector<GLshort> screen; //!< buffer to store the pick buffer
};

#endif //SELECTORQUADTREE_HPP
