/*! \file selectorGPU.hpp
 *  \brief Mouse selection handling
 *  \author Adam
*/

#ifndef SELECTORGPU_HPP
#define SELECTORGPU_HPP

#include <string>
#include <vector>

#include "selectorBase.hpp"

class Shader;

//! Implements the GPU based object selection
/*!
 * \details
 * The SelectorGPU class implements an alternative selection method.
 * The selectable objects are rendered into a texture the same way as in the quadtree based method.
 * After rendering a compute shader is executed on the texture, returning the id and the distance
 * of the closest object to the mouse. This way memory transfer and cpu time can be saved.
 * By selecting different count of kernels, the size of the screen ROI can be modified.
 * The following pseudocode demonstrates the object selection by the compute shader:
 * \code
 * //the compute shader, executed for each pixel
 *
 * ivec2 pixel = compute pixel position from thread id
 *
 * if pixel out of pick buffer bounds
 *  return;
 *
 * //load object id at pixel position
 * uint id = imageLoad(pickBuffer, pixel)
 *
 * //no object at pixel
 * if id == 0
 *  return;
 *
 * //compute distance, pack id and store if less
 * uint dist = (distance(mouse, pixel) << 16) | id;
 * atomicMin(buffer, dist);
 * \endcode
 * \author Adam
*/
class SelectorGPU : public SelectorBase {

    enum { KernelSize = 8 }; //! Defined in compute shader

public:
    SelectorGPU(const std::string& name, Scene* scene);
    ~SelectorGPU();

    void changeMode(); //!< debug, change kernel count
    void pickObject(const glm::ivec2& mouse); //!< select closest object to mouse

    void getPixelCount(int& width, int& height) const { width = windowX * KernelSize; height = windowY * KernelSize; }

private:
    int mode; //!< debug, help to set window
    int windowX; //!< number of kernels used in compute shader
    int windowY; //!< number of kernels used in compute shader

    GLuint objectBuffer; //!< buffer to store compute shader result on GPU
    Shader* computeShader; //!< the compute shader to use
    std::vector<GLuint> buffer; //!< buffer to store compute shader result on CPU
};

#endif //SELECTORGPU_HPP
