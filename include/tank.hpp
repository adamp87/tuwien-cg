/*! \file tank.hpp
 *  \brief %Tank handling for SteelWorms
 *  \author Adam
*/

#ifndef TANK_HPP
#define TANK_HPP

#include "sceneObject.hpp"

class GameLogic;
class ParticleSystemCPU;
class ParticleSystemGPU;

//! Implements the tank functionalities.
/*!
 * \brief The Tank class
 * \author Adam
 */
class Tank : public SceneObject {
public:
    class Gun : public SceneObject
    {
    public:
        Gun(const std::string& name, Scene* scene = 0, Model* model = 0, const glm::mat4& modelMatrix = glm::mat4(1));
        virtual ~Gun();

        bool animate(double time);
        void update(double deltaT);

    private:
        double lastAnimate;
    };

    class Turret : public SceneObject
    {
    public:
        Turret(const std::string& name, Scene* scene = 0, Model* model = 0, const glm::mat4& modelMatrix = glm::mat4(1));
        virtual ~Turret();

        void update(double deltaT);
    };

public:
    Tank(const std::string& name, Scene* scene = 0, Model* model = 0, const glm::mat4& modelMatrix = glm::mat4(1));
    virtual ~Tank();

    void reset();
    bool animate(double time);
    void update(double deltaT);

    double getHP() const { return hp; }
    void setHP(double val) { hp = val; }

    bool getActive() const { return active; }
    void setActive(bool val) { active = val; }

    glm::mat4 getShootOrigin() const;

private:
    double hp;
    bool active;
    double lastCPU;
    double lastGPU;

    std::shared_ptr<SceneObject> gun;
    std::shared_ptr<SceneObject> turret;
    std::shared_ptr<SceneObject> trackL;
    std::shared_ptr<SceneObject> trackR;
    std::shared_ptr<SceneObject> shootOrigin;

    std::shared_ptr<GameLogic> gameLogic;
    std::shared_ptr<ParticleSystemCPU> psCPU;
    std::shared_ptr<ParticleSystemGPU> psGPU;
};

#endif //TANK_HPP
