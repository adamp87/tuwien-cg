/*! \file particleSystemGPU.hpp
 *  \brief Particle system effect
 *  \author Adam
*/

#ifndef PARTICLESYSTEMGPU_HPP
#define PARTICLESYSTEMGPU_HPP

#include <glm/vec2.hpp>
#include "tessellation.hpp"

//! Implementation of GPU particle system.
/*!
 * \author Adam
 * \details For the moving of animated flying bullets effect, a GPU particle system was implemented using the Compute %Shader.
 *          This way the moving of the particles are computed on the GPU and it could handle even more bullets, then what appears in the demo.
 *          Two %Shader Storage Buffer Objects and one Atomic Counter Buffer Object are used.
 *          In the first buffer particles can have arbitrary order, in this buffer all the attributes of a bullet are stored.
 *          The second buffer stores the position and life of the particles aligned, this is the render buffer.
 *          A GPU thread is executed on one particle. If it lives, then its new position is computed,
 *          the active particle atomic counter is incremented and data is placed into the render buffer at the
 *          position retrieved from the atomic operation.
 *          When a particle is dead and a new particle should be inserted, an atomic increment is done.
 *          If the prefix from the atomic operation is smaller then the number of new particles,
 *          then the particle at the prefix is loaded by the current thread.
 *          There is an article about atomic counter on LightHouse 3D, according to this article,
 *          when atomic counters are supported by hardware, the cost of atomic functions are just like any other function call.
 *          Finally, the value of the atomic counter of active particles is read back to the CPU to be able to render with instancing.
 * \sa https://www.opengl.org/wiki/Compute_Shader
*/
class ParticleSystemGPU : public Tessellation {
public:
    //! Helper class to upload new particles
    struct NewParticle {
        glm::vec3 pos;
        glm::vec3 dir;
        glm::vec3 atr;//power, angle, dummy
    };

public:
    ParticleSystemGPU(const std::string& name, Scene* scene = 0, Model* model = 0, const glm::mat4& modelMatrix = glm::mat4(1.0f));
    virtual ~ParticleSystemGPU();

    //! Add a new particle to upload in next frame
    /*!
     * \note Currently only one particle can be uploaded as an uniform by frame.
     */
    void push(const NewParticle& pos);
    
    GLuint getActiveCount() const { return counters[0]; }
    GLuint getParticlePosVBO() const { return particlesPosVBO; }

    void draw() const;
    bool animate(double time);
    void update(double deltaT);
    void setShader(Shader* val);

private:
    double lastUpdate;
    double lastAnimate;
    Shader* computeShader;

    GLuint countersVBO;
    GLuint particlesVBO;
    GLuint particlesPosVBO;
    std::vector<GLuint> counters;
    std::vector<NewParticle> newParticles;
};

#endif //PARTICLESYSTEMGPU_HPP
