/*! \file legend.hpp
 *  \brief Mouse legend
 *  \author Felix
*/

#ifndef LEGEND_HPP
#define LEGEND_HPP

#include <glm/vec2.hpp>
#include <glm/glm.hpp> 
#include <glm/gtx/epsilon.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "sceneObject.hpp"
#include "shader.hpp"
#include "texture.hpp"
#include "mouseSemantics.hpp"

#include <ft2build.h>
#include FT_FREETYPE_H

#define MOUSE_MIN_MOVE 0.3
#define MOUSE_MAX_MOVE 3.5
#define MOUSE_CUTOFF 200
#define STANDARD_TEXTSIZE 15.0f

//! Displays a %Legend that contains text
/*!
* \par Design
* \parblock
* The Legend uses the freetype class in order to support rendering of text.
* The freetype library allows loading of characters as textures that can be rendered.
* An additional class LegendBackground is defined and used by the legend in order to render the background.
* Since different shaders are needed for rendering textures and background, the background rendering is encapsulated
* in a seperate class.
* \endparblock
*
* \par Implementation
* \parblock
* In the constructor of legend, images of all ascii characters are loaded and saved as textures on the gpu. This allows
* for selection of corresponding textures during the runtime instead of loading them at runtime, suffering hughe
* performance losses. Textures of characters are loaded with the FT_Library ft and are stored as FT_Face face. Each
* face defines the parameters with which a texture should be rendered. These parameters specify the offset from the
* base, its with and height. These parameters are contained in a FT_GlyphSlot. Therefore, the struct Glyph that
* corresponds to such a slot has been created. Here, the parameters are inserted and for each character a entry in the
* std::map<char,Glyph> charGlyphMap will be created. This map will be used together with
* std::map<char, GLuint charTexHandleMap to find corresponding glyphs and texture handles for each character during
* the runtime. If one want's to render additional lines of text, the draw function in legend.cpp must be modified.
* Currently, following crucial information is displayed:
*   - Cursor speed: this is the computed scaling of the mouse that lies in the range [m,M] as defined in MouseSemantics
*   - Scale factor: a scalar that is multiplied with the scale factor from above. The user can adjust this value if the mouse is too slow/fast
*   - Scale function: the standard or inverse scale function from the paper can be used.
*   - Selection mode: defines if a compute shader or a quad tree is used for calculating the distance.
*	- Distance: the current distance between the mouse and the nearest object is displayed.
*   - FPS: frames per second
* \author Felix
* \endparblock
*/
class Legend : public SceneObject {
    //! Glyph is used by Legend and contains positional information about characters.
    /*!
    * \author Felix
    * \par Implementation
    * \parblock
    * The Glyph struct holds the most important parameters from FT_GlyphSlot. This struct is used in std::map<char, Glyph> charGlyphMap; in order to look up the corresponding Glyph for a character. The memory in FT_GlyphSlot is automatically deallocated so we need an additional container, that is the reason why this struct exists.
    * \endparblock
    */
    struct Glyph {
            Glyph(FT_Pos _ax, FT_Pos _ay, unsigned int _bw, unsigned int _bh, FT_Int _bl, FT_Int _bt) {
                    ax = _ax;
                    ay = _ay;
                    bw = _bw;
                    bh = _bh;
                    bl = _bl;
                    bt = _bt;
            }

            FT_Pos ax;
            FT_Pos ay;
            unsigned int bw;
            unsigned int bh;
            FT_Int bl;
            FT_Int bt;
    };

    //! Renders a transparent image that can be used as background containing text.
    /*!
    * \author Felix
    * \par Implementation
    * \parblock
    * has a renderBackground function that does the rendering of the image onto the screen (as a quad).
    * This class is used by the Legend to display the background
    * \endparblock
    */
    class Background : public SceneObject {
    private:
            GLuint hudTex;
            GLuint vboPositions;
            Texture *texture;

    public:
            Background(Scene *_scene, Texture *_texture);
            virtual ~Background();

            void draw();
            void setShader(Shader *_shader);
            void renderBackground(float x, float y, float sx, float sy);
    };

private:

	//Text, HUD alignment
	float upperleftx;
	float upperlefty;
	float ystep;
	float xstep;
	float xreset;
	float yreset;
	float maxXposText;
	float maxYposText;
	float offsetX;
	float offsetY;

	std::string selectorText;

	//Engine
	glm::vec2 latestPos;
	GLFWwindow *window;
	MouseSemantics *mouse;
        Background *background;
	int frameCount;
	double accumulatedTime;
	int FPS;

	//Font
	FT_Library ft;
	FT_Face face;

	//Opengl
	std::map<char, GLuint> charTexHandleMap;
	std::map<char, Glyph> charGlyphMap;
	GLuint vboPositions;
	Texture *texture;

	float calculateScaleFactor();
	void renderText(const char *text, float x, float y, float sx, float sy);
	void updatePosition(bool reset);
	void calcFPS(double deltaT);

public:
	Legend(Scene* _scene, MouseSemantics *_mouse, Texture *_texture, float upperleftx, float upperlefty, float xstep, float ystep);
	virtual ~Legend();

	void setShader(Shader *shader);
	void draw();
	void update(double deltaT);

	void prepareText();
	void setActiveSelector(std::string _sText) { selectorText = _sText; }
};

#endif //LEGEND_HPP
