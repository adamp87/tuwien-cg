/*! \file model.hpp
 *  \brief %Model handling
 *  \author Adam
*/

#ifndef MODEL_HPP
#define MODEL_HPP

#include <vector>
#include <string>
#include <memory>
#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

class Texture;

//! Encapsulates a graphical object model in the GPU.
class Model {

    enum { VBO_POSITIONS = 0,
           VBO_NORMALS = 1,
           VBO_INDICES = 2,
           VBO_UVS = 3,
           VBO_ADJ_INDICES = 4,
           VBO_COUNT = 5};
public:
    Model(const std::vector<float>& positions,
          const std::vector<unsigned int>& indices,
          const std::vector<unsigned int>& adjacentIndices,
          const std::vector<float>& normals,
          const std::vector<float>& uvs,
          Texture* texture = 0);

    ~Model();

    GLuint getPosition() const { return vbo[VBO_POSITIONS]; }
    GLuint getNormals() const { return vbo[VBO_NORMALS]; }
    GLuint getIndices() const { return vbo[VBO_INDICES]; }
    GLuint getIndicesAdj() const { return vbo[VBO_ADJ_INDICES]; }
    GLuint getUVs() const { return vbo[VBO_UVS]; }
    size_t getIndexCount() const { return indexCount; }
    size_t getIndexAdjCount() const { return indexAdjCount; }
    Texture* getTexture() const { return texture; }
    float getShininess() const { return shininess; }
    glm::vec4 getDiffuse() const { return diffuse; }
    glm::vec4 getAmbient() const { return ambient; }
    glm::vec4 getSpecular() const { return specular; }

    void setShininess(float val) { shininess = val; }
    void setTexture(Texture* val) { texture = val; }
    void setDiffuse(const glm::vec4& val) { diffuse = val; }
    void setAmbient(const glm::vec4& val) { ambient = val; }
    void setSpecular(const glm::vec4& val) { specular = val; }

    static void getTerrainModel(size_t width,
                                size_t height,
                                size_t bitPerPixel,
                                float cellSize,
                                std::vector<unsigned char>& data,
                                std::vector<float>& heights,
                                std::vector<float>& positions,
                                std::vector<unsigned int>& indices,
                                std::vector<float>& normals,
                                std::vector<float>& uvs);

    static void getCubeModel(std::vector<float>& positions,
                             std::vector<unsigned int>& indices,
                             std::vector<float>& normals,
                             std::vector<float>& uvs);

    static void getQuadModel(std::vector<float>& positions,
                             std::vector<unsigned int>& indices,
                             std::vector<float>& uvs,
                             float width,
                             float height);

    static void getSphereModel(std::vector<float>& positions,
                               std::vector<unsigned int>& indices,
                               std::vector<float>& normals,
                               std::vector<float>& uvs);

    static void loadModels(const std::string& modelPath, std::vector<std::unique_ptr<Model> >& models);

private:
    GLuint vbo[VBO_COUNT];
    size_t indexCount;
    size_t indexAdjCount;
    Texture* texture;
    float shininess;
    glm::vec4 diffuse;
    glm::vec4 ambient;
    glm::vec4 specular;
};

#endif //MODEL_HPP
