///
///
//Utiliy class for meshes, only needed for Shadow volume algorithm where GL_TRIANGLES_ADJACENCY is used
//can be extended to full halfedge compatibility if needed...
//External class, found by Felix

#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))

struct Edge
{
    Edge(int _a, int _b)
    {
        assert(_a != _b);
        
        if (_a < _b)
        {
            a = _a;
            b = _b;                   
        }
        else
        {
            a = _b;
            b = _a;
        }
    }

    void Print()
    {
        printf("Edge %d %d\n", a, b);
    }
    
    int a;
    int b;
};

struct Face
{
    int Indices[3];
    
    int GetOppositeIndex(const Edge& e) const
    {
        for (int i = 0 ; i < ARRAY_SIZE_IN_ELEMENTS(Indices) ; i++) {
            int Index = Indices[i];
            
            if (Index != e.a && Index != e.b) {
                return Index;
            }
        }
        
        assert(0);

        return 0;
    }
};

struct Neighbors
{
    int n1;
    int n2;
    
    Neighbors()
    {
        n1 = n2 = (int)-1;
    }
    
    void AddNeigbor(int n)
    {
        if (n1 == -1) {
            n1 = n;
        }
        else if (n2 == -1) {
            n2 = n;
        }
        else {
            //assert(0);
        }
    }
    
    int GetOther(int me) const
    {
        if (n1 == me) {
            return n2;
        }
        else if (n2 == me) {
            return n1;
        }
        else {
            return me;
            //assert(0);
        }

        return 0;
    }
};

struct CompareEdges
{
    bool operator()(const Edge& Edge1, const Edge& Edge2)
    {
        if (Edge1.a < Edge2.a) {
            return true;
        }
        else if (Edge1.a == Edge2.a) {
            return (Edge1.b < Edge2.b);
        }        
        else {
            return false;
        }            
    }
};


struct CompareVectors
{
    bool operator()(const aiVector3D& a, const aiVector3D& b)
    {
        if (a.x < b.x) {
            return true;
        }
        else if (a.x == b.x) {
            if (a.y < b.y) {
                return true;
            }
            else if (a.y == b.y) {
                if (a.z < b.z) {
                    return true;
                }
            }
        }
        
        return false;
    }
};
