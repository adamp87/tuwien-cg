/*! \file lightCamera.hpp
 *  \brief Unused
 *  \author Felix
*/

#ifndef LIGHTCAMERA_HPP
#define LIGHTCAMERA_HPP

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include "camera.hpp"
#include "scene.hpp"

//! Implementation of the light camera effect.
/*!
 * \author Felix
 * \details \todo At the moment not needed for simple post processing lights
*/
class LightCamera : public Camera {
public:
    LightCamera(const std::string& name, Scene* scene, glm::vec3& pos, glm::vec3& up, glm::vec3& eye, float fov, float near, float far);
    virtual ~LightCamera();

    void drawShadowMap();
    void draw() const;
    void reset();
    void setShader(Shader* val);
    void update(double deltaT);

private:
    class EffectObjectLightCamShadow;
    friend class EffectObjectLightCamShadow;

    Shader* shader;
    GLuint frameBuffer;
    GLuint depthTexture;

    void setUpDepthTexture();
};

#endif //CAMERA_HPP
