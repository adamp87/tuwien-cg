/*! \file selectorBase.hpp
 *  \brief Mouse selection handling
 *  \author Adam
*/

#ifndef SELECTORBASE_HPP
#define SELECTORBASE_HPP

#include <string>
#include <vector>

#include "sceneObject.hpp"

//! Base class for selectors
/*!
 * \details Responsible for pick buffer drawing and interface.
 * \sa SelectorQuadTree
 * \author Adam
*/
class SelectorBase : public SceneObject {

public:
    SelectorBase(const std::string& name, Scene* scene);
    ~SelectorBase();

    void reset(); //!< set up EffectObject
    void draw() const; //!< draw selectable object ids into a texture
    void update(double deltaT); //!< empty function

    virtual void changeMode() = 0; //!< debug, change mode
    virtual void pickObject(const glm::ivec2& mouse) = 0; //!< select closest object to mouse

    int getBufferWidth() const { return width; }
    int getBufferHeight() const { return height; }
    size_t getLastSelected() const { return lastSelected; }
    size_t getLastDistance() const { return lastDistance; }

protected:
    class EffectObject;

    int width; //!< width of screen
    int height; //!< height of screen
    GLuint idBuffer; //!< frame buffer
    GLuint idTexture; //!< texture of rendered object indices

    size_t lastDistance; //!< distance after pickObject() call
    size_t lastSelected; //!< selected object id after pickObject() call
};

#endif //SELECTORBASE_HPP
