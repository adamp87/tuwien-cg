/*! \file volumetricLight.hpp
 *  \brief Volumetric Lighting Effect
 *  \author Felix
*/

#ifndef VOLUMETRICLIGHT_HPP
#define VOLUMETRICLIGHT_HPP

#include "sceneObject.hpp"

#include <memory>
#include <vector>
#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

//! Implementation of the volumetric light effect.
/*!
 * \author Felix
 * \details \todo doc
*/
class VolumetricLight: public SceneObject {
public:
    
    VolumetricLight(const std::string& name, Scene* scene = 0, Model* model = 0, const glm::mat4& modelMatrix = glm::mat4(1.0f));
    virtual ~VolumetricLight();

    void firstPass();
    void draw() const;
    void reset();
    void postProcess();
    GLuint getFrameBufferTexture() { return colorTexture; }
    glm::vec4 getScreenLightPos() { return ScreenLightPos; }

private:
    class EffectObjectVolumetric;
    friend class EffectObjectVolumetric;
    class EffectObjectVolumetricPost;
    friend class EffectObjectVolumetricPost;

    bool isInScreen;
    int texWidth;
    int texHeight;

    GLuint frameBuffer;
    GLuint depthTexture;
    GLuint colorTexture;

    Model* quadModel;

    glm::vec4 ScreenLightPos;

    void setUpDepthTexture();
    void setUpColorTexture();
    void screenTest();
    void switchShader(Shader* newShader);
};

#endif //VOLUMETRICLIGHT_HPP
