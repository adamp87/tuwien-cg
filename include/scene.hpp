/*! \file scene.hpp
 *  \brief Render Engine
 *  \author Adam
*/

#ifndef SCENE_HPP
#define SCENE_HPP

#include <map>
#include <string>
#include <memory>
#include <vector>

class Debug;
class Model;
class Light;
class Shader;
class Camera;
class Texture;
class SelectorGPU;
class SelectorQuadTree;
class SceneObject;
class VolumetricLight;
class CubeMap;
class LightCamera;
class MouseSemantics;
class SelectorBase;
class Legend;
class BubbleCursor;
struct GLFWwindow;

//! Encapsulates the Rendering Engine, holds a complete scene and it's assets.
/*!
 * \author Adam
 * \par Design
 * \parblock
 * The rendering engine has the same hierarchy as the Assimp aiScene class.
 * The Assimp Scene Structure can be seen on the following image.
 * \image html assimp.png "http://ephenationopengl.blogspot.co.at"
 * The rendering engine is designed in parent-child relation.
 * Each SceneObject holds it's local model matrix.
 * In order to compute the global model matrix of an object, the parents must be traversed until the root.
 * This way the camera is also the part of the scene, therefore camera animation can be handled.
 * Also animated objects like the tank can be displayed.
 *
 * Each SceneObject (Assimp:aiNode) has a corresponding aiNodeAnim, which is accessible through the name.
 * For convenience, the number of Quaternion, Position and Scaling and their time at the same index must be the same.
 * The time is a measurement since the start of the global scene animation.
 * The local model matrices are computed using the equation M = T * R * S,
 * where the Quaternions, Position and Scalings are interpolated values between two T, R, S from aiNodeAnim.
 * The SceneObject class has a virtual animate() function, which takes the time since start as an argument.
 * This function takes the $i^{th}$ local model matrix from the animation,
 * where $M_i.time < time <= M_{i+1}.time$ and sets it as the current local model matrix.
 * The animate() function was chosen to be virtual, because special objects might need special calls,
 * for example the camera class needs to compute the view matrix on every model matrix change.
 *
 * The models are also loaded using the Assimp library.
 * The material information is stored in the model object.
 * Images are loaded using the OpenCV library and stored in a texture object.
 * Multiple SceneObjects can use the same model or texture objects.
 * Shader loading is redesigned to be able to link together arbitrary number of shaders of different types.
 * \endparblock
 *
 * \par Implementation
 * \parblock
 * The assets are stored as a std::map of std::unique_ptr.
 * If needed they can be searched by name, but objects can have direct access through raw pointers.
 * This design does not allow easy swapping of assets between different scenes.
 * The SceneObjects are stored in parent-child hierarchy, therefore only the root node is stored.
 * For rendering purpose, a render list is stored as a std::map.
 * Objects can be pushed or inserted according to indices, and can be removed with pointers.
 * The SceneObjects are stored as std::shared_ptr.
 * This might be a performance decrease, because of the atomic increments, in the current design where there is no multithreading.
 * \endparblock
*/
class Scene {
public:
    enum { SentinelPre = 0,
           SentinelDraw = 1000,
           SentinelDrawPickable = 1500,
           SentinelPost = 2000,
           SentinelUnordered = 3000 };

    typedef std::map<std::size_t, std::shared_ptr<SceneObject> >::const_iterator ConstIterator;

    Scene(GLFWwindow* window);
    ~Scene();

    void init(const std::string& scenePath);

    void clearRender();
    void remRender(std::shared_ptr<SceneObject>& object);
    void getRender(size_t idx, std::shared_ptr<SceneObject>& object) const;
    size_t pushRender(std::shared_ptr<SceneObject>& object, size_t idx = SentinelUnordered);
    size_t insertRender(std::shared_ptr<SceneObject>& object, size_t idx = SentinelUnordered);

    Shader* getShader(const std::string& name) const;
    void addShader(const std::string& name, std::unique_ptr<Shader>& shader);
    void changeRenderShader(const std::string& from, const std::string& to);

    GLFWwindow* getWindow() const { return window; }
    std::shared_ptr<Light> getLight() const { return light; }
    std::shared_ptr<Camera> getCamera() const { return camera; }
    std::shared_ptr<SceneObject> getRootNode() const { return rootNode; }
    std::shared_ptr<CubeMap> getCubeMap() const { return cubeMap; }
    std::shared_ptr<VolumetricLight> getVolumetricLight() const { return volumetricLight; }
    std::shared_ptr<MouseSemantics> getMouseSemantics() const { return mouseSemantics; }
    std::shared_ptr<SelectorBase> getActiveSelector() const { return activeSelector; }
    std::shared_ptr<Legend> getLegend() const { return legend; }
    std::shared_ptr<BubbleCursor> getBubbleCursor() const { return bubbleCursor; }
    bool getObject(const std::string& name, std::shared_ptr<SceneObject>& object, size_t pos = 1) const;

    void setWindow(GLFWwindow* val) { window = val; }
    void setLight(std::shared_ptr<Light> val) { light = val; }
    void setCamera(std::shared_ptr<Camera> val) { camera = val; }
    void setRootNode(std::shared_ptr<SceneObject> root) { rootNode = root; }
    void setCubeMap(std::shared_ptr<CubeMap> val)  { cubeMap = val; }
    void setVolumetricLight(std::shared_ptr<VolumetricLight> val) { volumetricLight = val; }
    void setMouseSemantics(std::shared_ptr<MouseSemantics> val) { mouseSemantics = val; }
    void setActiveSelector(std::shared_ptr<SelectorBase> val) { activeSelector = val; }
    void setLegend(std::shared_ptr<Legend> val) { legend = val; }
    void setBubbleCursor(std::shared_ptr<BubbleCursor> val) { bubbleCursor = val; }

    ConstIterator end() const { return renderList.cend(); }
    ConstIterator begin() const { return renderList.cbegin(); }
    ConstIterator end(size_t idx) const { return renderList.lower_bound(idx); }
    ConstIterator begin(size_t idx) const { return renderList.lower_bound(idx); }
    //static void draw(const ConstIterator& it) { it->second->draw(it->first); }
    static std::shared_ptr<SceneObject> get(const ConstIterator& it) { return it->second; }

private:
    friend class Debug; //direct access to textures
    friend class AssimpLoader; //direct access to models and textures

    GLFWwindow* window;

    //effects
    std::shared_ptr<CubeMap> cubeMap;
    std::shared_ptr<VolumetricLight> volumetricLight;

    //selektor objects
    std::shared_ptr<Legend> legend;
    std::shared_ptr<MouseSemantics> mouseSemantics;
    std::shared_ptr<SelectorBase> activeSelector;
    std::shared_ptr<BubbleCursor> bubbleCursor;

    //scene
    std::shared_ptr<Light> light;
    std::shared_ptr<Camera> camera;
    std::shared_ptr<SceneObject> rootNode;
    std::map<std::string, std::unique_ptr<Model> > models;
    std::map<std::string, std::unique_ptr<Shader> > shaders;
    std::map<std::string, std::unique_ptr<Texture> > textures;
    std::map<std::size_t, std::shared_ptr<SceneObject> > renderList;

    bool getObjectRecursive(const std::string& name, std::shared_ptr<SceneObject>& object, size_t& pos) const;
};

#endif //SCENE_HPP
