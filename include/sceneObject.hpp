/*! \file sceneObject.hpp
 *  \brief Render Engine Object
 *  \author Adam
*/

#ifndef SCENEOBJECT_HPP
#define SCENEOBJECT_HPP

#include <map>
#include <memory>
#include <vector>
#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

class Scene;
class Model;
class Shader;

//! The base class of the objects which are rendered.
/*!
 * \par SceneObject implementation
 * \parblock
 * It has a pointer to a shader, a model, a scene, holds the model matrix and a handle to a vertex array object.
 * A generic drawing function is written here. It holds the parent child hierarchy.
 * Childs are stored in std::shared_ptr. The parent is stored in a std::weak_ptr.
 * This solution is questionable, since this implies an atomic increment and decrement on each parent access, which happens frequently.
 * Another problem might be if an implementation bug causes,
 * that the parent has been deleted but the child still lives, then the std::weak_ptr will return NULL,
 * the program won't crash because this means that the root has been reached,
 * but the object has not reached the root, so it won't be in global model space.
 * \endparblock
 * \author Adam
*/
class SceneObject : public std::enable_shared_from_this<SceneObject> {
public:
    typedef std::vector<std::pair<double, glm::mat4> > Animation;

    SceneObject(std::shared_ptr<SceneObject>& effectParent, const glm::mat4& modelMatrix = glm::mat4(1));
    SceneObject(const std::string& name, Scene* scene = 0, Model* model = 0, const glm::mat4& modelMatrix = glm::mat4(1));
    virtual ~SceneObject();

    Model* getModel() const { return model; }
    Shader* getShader() const { return shader; }
    glm::mat4 getModelMatrix() const { return modelMatrix; }
    glm::mat4 getGlobalModelMatrix() const;
    std::string getName() const { return name; }
    std::shared_ptr<SceneObject> getParent() const { return parent.lock(); }

    void setAnimationTime(double time);
    bool setAnimation(const Animation& anim);

    bool delChild(size_t idx);
    bool getChild(size_t idx, std::shared_ptr<SceneObject>& child) const;
    bool addChild(std::shared_ptr<SceneObject>& child);

    bool remEffect(const std::string& name);
    SceneObject* getEffect(const std::string& name) const;
    bool addEffect(const std::string& name, std::unique_ptr<SceneObject>& effect);

    virtual void reset();
    virtual void draw() const;
    virtual bool animate(double time);
    virtual void update(double deltaT);
    virtual void setShader(Shader* val);

protected:
    GLuint vao;
    Scene* scene;
    Model* model;
    Shader* shader;
    glm::mat4 modelMatrix;

    std::string name;
    std::weak_ptr<SceneObject> parent;
    std::vector<std::shared_ptr<SceneObject> > childs;
    std::map<std::string, std::unique_ptr<SceneObject> > effectChilds;

    size_t animIDX;
    Animation animation;
};

inline glm::mat4 SceneObject::getGlobalModelMatrix() const {
    glm::mat4 M = modelMatrix;
    std::shared_ptr<SceneObject> node = getParent();
    while(node) {
        M = node->getModelMatrix() * M;
        node = node->getParent();
    }
    return M;
}

#endif //SCENEOBJECT_HPP
