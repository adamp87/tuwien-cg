/*! \file texture.hpp
 *  \brief %Texture handling
 *  \author Adam
*/

#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <string>
#include <vector>

#include <GL/glew.h>

//! Encapsulates a texture in the GPU.
class Texture {
public:
    Texture(const std::string& filename, bool readalpha = false);
    ~Texture();

    GLuint getHandle() const { return handle; }

    static void loadBMP(const std::string& filename, std::vector<unsigned char>& data, size_t& width, size_t& height, size_t& bitPerPixel, bool readalpha = false);

private:
    GLuint handle;
};

#endif //TEXTURE_HPP
