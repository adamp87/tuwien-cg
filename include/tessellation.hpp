/*! \file tessellation.hpp
 *  \brief %Tessellation effect
 *  \author Adam
*/

#ifndef TESSELLATION_HPP
#define TESSELLATION_HPP

#include "sceneObject.hpp"

//! Implementation of tessellation, abstract class
/*!
 * \author Adam
 * \details For the rendering of animated flying bullets effect, tessellation was implemented using the %Tessellation %Shader.
 *          Since this effect is part of the GPU Particle System, this section will have some references on particles.
 *          The Vertex %Shader is only a pass-through shader.
 *          In the %Tessellation Control %Shader, the inner and outer tessellation level can be set.
 *          The visualization of this two variable on triangles can be see on following image:
 * \image html tess.png "StackOverflow - Tessellation Shader - OpenGL"
 *          Using the life of a particle, the vertices of the object are scaled.
 *          For this purpose, level-of-detail is used, where larger objects will have more vertices generated.
 *          The value of tessellation levels are computed using the life of a particle.
 *          The effect uses triangles to tessellate, therefore Barycentric coordinates are received in the %Tessellation Evaluation %Shader.
 *          The vertices are computed by interpolating the Barycentric coordinates of the tessellated values with the control points.
 *          Then they are normalized to transform the cube model into a sphere.
 *          If quads would be tessellated, interpolation would be necessary only for normals.
 *          Displacement mapping is used in order to modify the shape of the sphere.
 *          To achieve a simple animation a 3D texture is used, which stores layers of Gaussian functions with increasing sigma value.
 *          The coordinates of the texture is the usual u, v and the third coordinate is the life of the particle.
 *          The positions are translated along the normal in local space to achieve displacement.
 *          To improve the normals of the displaced surface a Geometry %Shader is used,
 *          where the tessellated normals are interpolated with the normal of the face,
 *          where the normal of the face is the cross product of two vectors computed from the displaced vertices.
 *          Finally the Fragment %Shader of the OmniShadow effect is used.
 *          To achieve shadows with OmniShadow, only a simplified %Tessellation Evaluation %Shader is necessary for
 *          rendering the tessellation effect within the OmniShadow effect.
 * \sa https://www.opengl.org/wiki/Tessellation
*/
class Tessellation : public SceneObject {
public:
    Tessellation(const std::string& name, Scene* scene = 0, Model* model = 0, const glm::mat4& modelMatrix = glm::mat4(1));
    virtual ~Tessellation();

    void draw() const;
    bool animate(double time);
    void update(double deltaT);
    void setShader(Shader* val);

    GLuint getDispTexture() const { return dispTexture; }
    unsigned int getTessLevelIn() const { return tessLevelIn; }
    unsigned int getTessLevelOut() const { return tessLevelOut; }

    void setTessLevelIn(unsigned int val) { tessLevelIn = val; }
    void setTessLevelOut(unsigned int val) { tessLevelOut = val; }

protected:
    GLuint dispTexture; /*!< Displacement texture. */

    unsigned int tessLevelIn; /*!< Inner tessellation level. */
    unsigned int tessLevelOut; /*!< Outer tessellation level. */

private:
    float dispTime; /*!< Global animation time. */
};

#endif //TESSELLATION_HPP
