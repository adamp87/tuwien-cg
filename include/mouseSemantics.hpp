/*! \file mouseSemantics.hpp
 *  \brief Mouse movement handling
 *  \author Felix
*/

#ifndef MOUSESEMANTICS_HPP
#define MOUSESEMANTICS_HPP

#include <glm/vec2.hpp>
#include <glm/glm.hpp> 
#include <glm/gtx/epsilon.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "sceneObject.hpp"
#include "shader.hpp"
#include "texture.hpp"

#define MOUSE_MIN_MOVE 0.3
#define MOUSE_MAX_MOVE 3.5
#define MOUSE_CUTOFF 200

//! The class which handles the drawing of the cursor as well as the semantic scaling function that translates the movement from motor space to visual space.
/*!
* \details The purpose of the class MouseSemantics is to 
*
* 1. draw a mouse cursor and scale its appearance and 
* 2. scale the movement according to the distance of the nearest object.
* 
* The draw method uses point sprites to render the picture of a mouse pointer onto the screen.
* The size of the cursor is then directly coupled to the distance of the nearest pickable object. The bigger the distance, the bigger the mouse will be rendered.
*
*
* In addition to rendering, this class also implements the 2 scale functions which are presented in the paper. 
* Important parameters are:
*  - M maximum mouse speed, default: 3.5
*  - m minimum mouse speed, default: 0.3
*  - C cutoff distance (default 200 pixels): when the distance to the nearest pickable object is bigger than this value, mouse movement will always be at maximum specified speed
*
* Standard scale function: (M - m) * (C / D) + M
*
* Inverse scale function: (m - M) / (D + 1)^S + M
* where D is the actual distance to the nearest object and S is an empirically derived constant (0.1 in our case).
*
* Using F10 one can toggle between the inverse and the standard scale function. Using J and K one can slow down or speed up the overall mouse speed. 
*
* The scale functions are depicted as follows:
*
* \image html linearscale.png "linear scale function"
* \image html inversescale.png "inverse scale function"
*
* \author Felix Koenig
*/
class MouseSemantics : public SceneObject {
private:
	glm::vec2 latestPos;
	int distance;
	float M;
	float C;
	float m;
	float additionalScaling;
	bool scaleLinear;
	bool inWindow;
	bool firstTimeInWindow;
	bool focused;
	bool inverseScaleFunction;
	GLFWwindow *window;
	GLuint vboPositions;
	Texture *texture;

	float calculateScaleFactor();

public:
	MouseSemantics(Scene* scene, float _M, float _C, float _m, Texture* texture);
	virtual ~MouseSemantics();
	
	void setShader(Shader *shader);
	void draw();
	void update(double deltaT);

	void cursorEnterCallBack(GLFWwindow *window, int entered);
	void windowFocusCallBack(GLFWwindow *window, int entered);
	void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);

	void toggleScaleFunction();

	void updateDistanceState(unsigned int _distance);

	unsigned int getDistance() { return distance; };
	std::string scaleFunction() { if (inverseScaleFunction) return "inverse"; else return "standard"; }
	float getScaleFactor() { return additionalScaling; };
	float getSpeed() { return calculateScaleFactor(); };
	glm::ivec2 getMousePos() { return glm::ivec2(latestPos); }
};

#endif //MOUSESEMANTICS_HPP
