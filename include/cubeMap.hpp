/*! \file cubeMap.hpp
 *  \brief %CubeMap effect
 *  \author Felix
*/

#ifndef CUBEMAP_HPP
#define CUBEMAP_HPP

#include "sceneObject.hpp"

#include <memory>
#include <vector>
#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

//! Implementation of the cube map effect.
/*!
 * \author Felix
 * \details \todo doc
*/
class CubeMap: public SceneObject {
public:
    
    CubeMap(const std::string& name, Scene* scene = 0, Model* model = 0, const glm::mat4& modelMatrix = glm::mat4(1.0f));
    virtual ~CubeMap();

    void renderToCubeMap();
    void reset();
    void draw() const;

private:
    class EffectObject;
    friend class EffectObject;

    GLint cubeTexWidth;
    GLint cubeTexHeight;
    //the cube faces are rendered into a framebuffer
    GLuint frameBuffer;
    //needed for z-testing
    GLuint depthTexture;
    GLuint cubeTexture;
    GLenum cubeFaces[6];
    
    int renderPasses;

    std::vector<glm::mat4> viewMatrices;
    glm::vec4 position;

    void setUpDepthTexture();
    void setUpColorTexture();
    void setViewMatrices();
};

#endif //CUBEMAP_HPP
