/*! \file particleSystemCPU.hpp
 *  \brief Particle system effect
 *  \author Adam
*/

#ifndef PARTICLESYSTEMCPU_HPP
#define PARTICLESYSTEMCPU_HPP

#include "sceneObject.hpp"

//! Implementation of CPU particle system.
/*!
 * \author Adam
 * \details When a tank is moving, it is leaving trail after itself.
 *          Each trail is a simple quad.
 *          When a new trail is added, it's position is written into a buffer and it's transparency is 1.
 *          During each update(), the transparency is decreased with a factor until it reaches 0.
 *          On zero the particle is dead and not updated.
 *          It uses transparency therefore sorting is needed based on the camera distance.
 *          Transparent objects must be rendered in the order of their distance from the camera.
 *          The highest is rendered first. This must be done, because if a transparent object is rendered,
 *          then later an object is rendered which is occluded by the transparent one, then because of the
 *          depth test, the later object would not be rendered.
 *          On each draw() the position and the transparency of each living particle is uploaded and drawn with instancing.
 * \sa http://www.opengl-tutorial.org/intermediate-tutorials/billboards-particles/particles-instancing/
*/
class ParticleSystemCPU : public SceneObject {

    struct ParticleGPU { //keep it POD
        float xyz[3];
        float alpha; // 0-dead, 1-newly created
    };

    struct ParticleCPU : public ParticleGPU {
        float cameraDistance;

        ParticleCPU();

        bool operator>(const ParticleCPU& other) const {
            return cameraDistance > other.cameraDistance;
        }
    };

public:
    ParticleSystemCPU(size_t particleCount, const std::string& name, Scene* scene = 0, Model* model = 0, const glm::mat4& modelMatrix = glm::mat4(1.0f));
    virtual ~ParticleSystemCPU();

    void push(const glm::vec3& pos);

    void draw() const;
    bool animate(double time);
    void update(double deltaT);
    void setShader(Shader* val);

private:
    double lastAnimate;
    GLuint particlesVBO;
    int activeParticleCount;
    std::vector<ParticleCPU> particlesNew;
    std::vector<ParticleCPU> particlesCPU;
    std::vector<ParticleGPU> particlesGPU;
};

#endif //PARTICLESYSTEMCPU_HPP
