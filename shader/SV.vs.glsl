/**
  * @brief %Shadow Volumes shader
  * @author Felix
  * @namespace GLSL
  * @class ShadowVolumes
  * @details
  */

#version 430

in vec2 uv;
in vec3 normal;
in vec3 position; // object space vertex position

uniform mat4 PV_mat; // (projection * view) matrix
uniform mat4 M; // model matrix
uniform mat4 V;
uniform mat4 MV;
uniform mat4 MVP;
uniform mat3 N;
uniform vec3 MlightPosition;
uniform vec4 McameraPosition;

out vec3 mNormal;
out vec3 mLightDir;
out vec2 texCoords;
invariant out vec4 WS_pos; // to be passed on to GS
invariant gl_Position;

void main(void) {

     WS_pos = M * vec4(position, 1);

     mNormal = normalize(N*normal);

     mLightDir = normalize(MlightPosition - vec3(WS_pos));

     vec4 CS_pos = PV_mat * WS_pos;
     
     texCoords = uv;
     
     gl_Position = CS_pos;
}
