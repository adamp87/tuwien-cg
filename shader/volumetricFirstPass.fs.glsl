/**
  * @brief %Volumetric Lighting shader
  * @author Felix
  * @namespace GLSL
  * @class VolumetricLight
  * @details
  */

#version 420 core

uniform vec4 inColor;

out vec4 color;

void main(void) {
     color = inColor;
}
