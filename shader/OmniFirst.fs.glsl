/**
  * @brief %OmniShadow effect shader
  * @author Felix
  * @namespace GLSL
  * @class OmniShadowEffect
  * @details
  */

#version 420 core

uniform vec2 near_far; // near and far plane for cm-cams
uniform vec4 MlightPosition; // world space light position

invariant in vec4 WS_Pos;

void main(void) {
     
     float WS_dist = distance(WS_Pos, MlightPosition);
     
     float WS_dist_normalized = WS_dist / near_far.y;
     
     gl_FragDepth = WS_dist_normalized;
}
