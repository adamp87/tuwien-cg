/**
  * @brief %CubeMap shader
  * @author Felix
  * @namespace GLSL
  * @class CubeMap
  * @details
  */

#version 420 core

invariant in vec4 WS_pos;
in vec3 mNormal;

uniform samplerCube cubeTexture;
uniform vec4 McameraPosition;

out vec4 vFragColor;

void main(void) {
    vec3 mPosition = WS_pos.xyz / WS_pos.w;
    vec3 viewV = normalize(McameraPosition.xyz - mPosition);
    vec3 refl = normalize(reflect(-viewV, normalize(mNormal)));

    vFragColor = texture(cubeTexture, refl.stp);
}
