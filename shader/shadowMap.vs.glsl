/**
  * @brief %ShadowMap effect shader
  * @author Adam
  * @namespace GLSL
  * @class ShadowMapEffect
  * @details
  */

#version 330

in vec3 position;

out vec4 gl_Position;

uniform mat4 M;
uniform mat4 depthMVP;

void main(){
    gl_Position = depthMVP * (M * vec4(position,1));
}

