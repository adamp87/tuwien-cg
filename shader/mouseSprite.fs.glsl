/**
  * @brief %Mouse shader
  * @author Felix
  * @namespace GLSL
  * @class MouseSprite
  * @details
  */

#version 330 core

uniform sampler2D cursorTexture;

out vec4 vFragColor;

void main()
{
    vFragColor = texture(cursorTexture, gl_PointCoord);
    //vFragColor.a = 0.5;
    //vFragColor = vec4(gl_PointCoord,1.0,1.0);
}
