/**
  * @brief %Volumetric Lighting shader
  * @author Felix
  * @namespace GLSL
  * @class VolumetricLight
  * @details
  */

#version 430

in vec3 position; // object space vertex position
in vec2 uv;

uniform mat4 PV_mat; // (projection * view) matrix
uniform mat4 M; // model matrix
uniform mat4 MVP;

smooth out vec2 texCoords;

void main(void) {
     texCoords = uv;
     gl_Position = vec4(position,1);
}
