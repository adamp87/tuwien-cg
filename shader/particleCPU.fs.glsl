/**
  * @brief %Particle CPU effect shader
  * @author Adam
  * @namespace GLSL
  * @class ParticleCPU
  * @details
  */

#version 330

in vec4 fParticle;

out vec4 color;

void main(){
    color = vec4(0.0, 0.0, 0.0, fParticle.a);
}

