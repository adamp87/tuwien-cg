/**
  * @brief %OmniShadow effect shader
  * @author Felix
  * @namespace GLSL
  * @class OmniShadowEffect
  * @details
  */

#version 420 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 18) out;

uniform mat4 cm_posx;
uniform mat4 cm_negx;
uniform mat4 cm_posy;
uniform mat4 cm_negy;
uniform mat4 cm_posz;
uniform mat4 cm_negz;

out vec4 WS_Pos;

void main(void) {
	int tri_vert;
	mat4 cm = cm_posx;
	for(gl_Layer = 0; gl_Layer < 6; gl_Layer++) {
		for(tri_vert = 0; tri_vert < gl_in.length(); tri_vert++) {
			if(gl_Layer == 1) {
				cm = cm_negx;
			} else if(gl_Layer == 2) {
				cm = cm_posy;
			} else if(gl_Layer == 3) {
				cm = cm_negy;
			} else if(gl_Layer == 4) {
				cm = cm_posz;
			} else if(gl_Layer == 5){
			    cm = cm_negz;
			}
			gl_Position = cm * gl_in[tri_vert].gl_Position;
			WS_Pos = gl_in[tri_vert].gl_Position;
			EmitVertex();
		}
		EndPrimitive();
	}
}
