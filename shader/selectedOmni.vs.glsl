/**
  * @brief %Phong with OmniShadow shading
  * @author Felix
  * @namespace GLSL
  * @class PhongOmni
  * @details Selected shader for Selektron
  */

#version 430

in vec2 uv;
in vec3 normal;
in vec3 position;

uniform mat4 M;
uniform mat4 V;
uniform mat4 MV;
uniform mat4 MVP;
uniform mat3 N;
uniform mat4 depthBiasMVP;
uniform vec3 MlightPosition;

smooth out vec3 mNormal;
smooth out vec3 mLightDir;
smooth out vec2 texCoords;
smooth out vec3 mPosition;

void main(void) 
{ 
    mNormal = normalize(N*normal);

    mPosition = vec3(M * vec4(position,1));
    
    mLightDir = normalize(MlightPosition - mPosition);

    texCoords = uv;

    gl_Position = MVP * vec4(position, 1.0);
}
