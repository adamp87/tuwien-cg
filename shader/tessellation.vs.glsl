/**
  * @brief %Tessellation shader
  * @author Adam
  * @namespace GLSL
  * @class Tessellation
  * @details
  */

#version 410 core

in vec2 uv;
in vec3 normal;
in vec3 position;

// variables to pass down information from VS to TCS
out vec3 in_Position_CS;
out vec3 in_Normal_CS;
out vec2 in_UV_CS;

void main(void)
{
    in_Position_CS = position;
    in_Normal_CS = normal;
    in_UV_CS = uv;
}
