/**
  * @brief %Particle CPU effect shader
  * @author Adam
  * @namespace GLSL
  * @class ParticleCPU
  * @details
  */

#version 330

in vec3 position;
in vec4 particle;

out vec4 fParticle;
out vec4 gl_Position;

uniform mat4 M;
uniform mat4 V;
uniform mat4 MV;
uniform mat4 VP;
uniform mat4 MVP;

void main(){
    fParticle = particle;
    gl_Position = VP * vec4(particle.xyz + vec3(M * vec4(position.xyz, 1.0)), 1.0);
    //particle is in model space, translation of the particle
    //M is the global model matrix of the particle system, used to flip y, z
}

