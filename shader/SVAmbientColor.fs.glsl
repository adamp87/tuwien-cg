/**
  * @brief %Shadow Volumes shader
  * @author Felix
  * @namespace GLSL
  * @class ShadowVolumes
  * @details
  */

#version 420 core

uniform vec4 inColor;

out vec4 color;

void main(void) {
     float ambientTerm = 1.0;
     color = ambientTerm * inColor;
}
