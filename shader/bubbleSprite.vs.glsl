/**
  * @brief %Bubble Cursor shader
  * @author Felix
  * @namespace GLSL
  * @class BubbleCursor
  * @details
  */

#version 330 core

uniform vec2 mousePos;
uniform float size;
uniform float scale;

in vec2 position;

#define MAXSIZE 600.0

void main()
{
    gl_PointSize = MAXSIZE * size;
    gl_Position = vec4(2*mousePos-vec2(1.0,1.0), 0, 1);
}
