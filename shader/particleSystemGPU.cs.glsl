/**
  * @brief %Particle GPU system shader
  * @author Adam
  * @namespace GLSL
  * @class ParticleGPU
  * @details
  */

#version 430 core

#define MAX_NEW_PARTICLE 6

struct Particle {
    vec4 pos;
    vec4 dir;//x, y, power, angle
    vec4 origin;
};

struct NewParticle {
    vec3 pos;
    vec3 dir;
    vec3 attribs;//power, angle
};

layout(local_size_x=512, local_size_y=1, local_size_z=1) in;

layout(binding = 0, std430) buffer Positions { vec4 positions[]; };
layout(binding = 1, std430) buffer Particles { Particle particles[]; };

layout(binding = 2, offset = 4) uniform atomic_uint prefixSumNewParticle;
layout(binding = 2, offset = 0) uniform atomic_uint prefixSumActiveParticle;

layout(location = 0) uniform float deltaT;
layout(location = 1) uniform uint newParticleCount;
layout(location = 2) uniform uint maxParticleCount;

layout(location = 3) uniform vec3 pos;
layout(location = 4) uniform vec3 dir;
layout(location = 5) uniform vec3 attribs;
//layout(location = 3) uniform NewParticle newParticles[MAX_NEW_PARTICLE];
//Note: it is planned to extend multiple new particle handling

void moveParticle(uint threadID) {
    const float g = 9.81; //gravity

    float v = particles[threadID].dir[2];
    float fi = particles[threadID].dir[3];
    float zo = particles[threadID].origin.y;
    float d = length(vec2(particles[threadID].pos.x,    particles[threadID].pos.z) -
                     vec2(particles[threadID].origin.x, particles[threadID].origin.z));
    float z = zo + d * tan(fi) - g*pow(d, 2) / (2*pow(v*cos(fi), 2)); //wikipedia: ballistic trajectory

    particles[threadID].pos.x += particles[threadID].dir.x * deltaT;
    particles[threadID].pos.z += particles[threadID].dir.y * deltaT;
    particles[threadID].pos.y = z;
    //TODO: pos update should be coordinate sys independent, use global M of particleSys
}

void main()
{//run on each particle
    const uint threadID = gl_GlobalInvocationID.x;

    if(maxParticleCount <= threadID)
        return;

    if(particles[threadID].pos.w == 0)
    { //empty particle slot
        if(newParticleCount == 0)
            return;

        uint prefixNew = atomicCounterIncrement(prefixSumNewParticle);
        if(prefixNew < newParticleCount)
        {//add new particle
            particles[threadID].pos = vec4(pos, 1);
            particles[threadID].dir = vec4(dir.xz, attribs.xy); //x, y, power, angle
            particles[threadID].origin = particles[threadID].pos;
//            particles[threadID].pos = vec4(newParticles[prefixNew].pos, 1);
//            particles[threadID].dir = vec4(newParticles[prefixNew].dir.xy, //x, y,
//                                           newParticles[prefixNew].attribs.xy); //power, angle
//            particles[threadID].origin = particles[threadID].pos;
        }
    } else { //active particle
        uint prefix = atomicCounterIncrement(prefixSumActiveParticle);
        moveParticle(threadID);
        particles[threadID].pos.w -= deltaT * (1/3.0);//3sec
        particles[threadID].pos.w = max(0, particles[threadID].pos.w);
        positions[prefix] = particles[threadID].pos;
    }
}
