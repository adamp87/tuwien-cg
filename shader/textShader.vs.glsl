/**
  * @brief %Legend shader
  * @author Felix
  * @namespace GLSL
  * @class Legend
  * @details
  */

in vec4 position;
varying vec2 texcoord;
 
void main(void) {
  gl_Position = vec4(position.xy, 0, 1);
  texcoord = position.zw;
}
