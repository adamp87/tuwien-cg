/**
  * @brief %CubeMap effect shader
  * @author Felix
  * @namespace GLSL
  * @class CubeMapEffect
  * @details Blinn Phong shader for gs
  */

#version 420 core

in vec2 uv;
in vec3 normal;
in vec3 position;

uniform mat4 M;
uniform mat4 MVP;
uniform mat3 N;
uniform vec3 MlightPosition;

out VS_OUT {
  vec3 mNormal;
  vec3 mLightDir;
  vec3 mPosition;
} vs_out;

void main(void) 
{ 
    vs_out.mNormal = normalize(N*normal);

    vs_out.mPosition = vec3(M * vec4(position,1));
    
    vs_out.mLightDir = normalize(MlightPosition - vs_out.mPosition);

    gl_Position = MVP * vec4(position, 1.0);
}
