/**
  * @brief %Volumetric Lighting shader
  * @author Felix
  * @namespace GLSL
  * @class VolumetricLight
  * @details
  */

#version 420 core

in vec2 texCoords;

uniform sampler2D colorTexture;

out vec4 color;

void main(void) {
     color = texture(colorTexture, texCoords);
     //color = vec4(1.0,1.0,1.0,1.0);
}
