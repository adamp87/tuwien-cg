/**
  * @brief %Tessellation shader
  * @author Adam
  * @namespace GLSL
  * @class Tessellation
  * @details
  */

#version 410 core

in vec2 in_UV_FS;
in vec3 in_Normal_FS;

out vec4 color;

uniform sampler2D colorTexture;

void main(void)
{
    color = vec4(in_Normal_FS, 1.0);
}
