/**
  * @brief %Volumetric Lighting shader
  * @author Felix
  * @namespace GLSL
  * @class VolumetricLight
  * @details
  */

#version 430 core

smooth in vec2 texCoords;

uniform sampler2D frameSampler;
uniform vec4 ScreenLightPos;
uniform float exposure;
uniform float weight;
uniform float decay;
uniform float density;

out vec4 color;

const float NUM_SAMPLES = 200;

void main(void) {    
   vec2 texCoordsIterative = texCoords;
   vec2 deltaTexCoord = vec2(texCoords - ScreenLightPos.xy);   
   
   deltaTexCoord *= 1.0f / float(NUM_SAMPLES) * density;  
   color = texture(frameSampler, texCoords);  
   float illuminationDecay = 1.0f;  

   for (int i = 0; i < NUM_SAMPLES; i++)  {  
        // Step sample location along ray.  
        texCoordsIterative -= deltaTexCoord;  
        // Retrieve sample at new location.  
        vec3 texSample = texture(frameSampler, texCoordsIterative).rgb;  
        // Apply sample attenuation scale/decay factors.  
        texSample *= illuminationDecay * weight;  
        // Accumulate combined color.  
        color.rgb += texSample;  
        // Update exponential decay factor.  
        illuminationDecay *= decay;  
   }  
  // Output final color with a further scale control factor.  
   color = vec4( color.rgb * exposure, 1);  
   //color = texture(frameSampler, texCoords);
   //color = vec4(ScreenLightPos.x,ScreenLightPos.y,1,1);
}  
