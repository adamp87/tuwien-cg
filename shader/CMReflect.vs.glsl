/**
  * @brief %CubeMap shader
  * @author Felix
  * @namespace GLSL
  * @class CubeMap
  * @details Blinn Phong shader for gs
  */

#version 420 core

in vec3 normal;
in vec3 position;

uniform mat4 M;
uniform mat4 MVP;
uniform mat4 MV;
uniform mat3 N;
uniform vec4 McameraPosition;

smooth out vec3 smoothPosition;
smooth out vec3 smoothNormal;

void main(void) 
{ 
    smoothNormal = normalize(N*normal);
    smoothPosition = vec3(M * vec4(position,1));

    gl_Position = MVP * vec4(position, 1.0);
}
