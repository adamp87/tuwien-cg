/**
  * @brief %Legend shader
  * @author Felix
  * @namespace GLSL
  * @class Legend
  * @details
  */

varying vec2 texcoord;

uniform sampler2D charTexture;
uniform vec4 color;
 
void main(void) {
  gl_FragColor = vec4(texture(charTexture, texcoord).rgb,0.7);
}
