/**
  * @brief %Particle GPU shader
  * @author Adam
  * @namespace GLSL
  * @class ParticleGPU
  * @details Improves normals, but normal mapping should be used in fragment shader
  */

#version 410 core

in vec2 uv;
in vec3 normal;
in vec3 position;
in vec4 particle;

// variables to pass down information from VS to TCS
out vec4 in_Particle_CS;
out vec3 in_Position_CS;
out vec3 in_Normal_CS;
out vec2 in_UV_CS;

void main(void)
{
    in_Particle_CS = particle;
    in_Position_CS = position;
    in_Normal_CS = normal;
    in_UV_CS = uv;
}
