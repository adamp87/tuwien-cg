/**
  * @brief %Shadow Volumes shader
  * @author Felix
  * @namespace GLSL
  * @class ShadowVolumes
  * @details
  */

#version 430

out vec4 frag_data_0;
void main(void)
{
// color value actually only used when visualizing
// shadow volume mesh
// important thing happens implicitly (compare to depth buffer!):
// stencil buffer is updated according to previous
// state-configuration from the app
frag_data_0 = vec4(0.25, 0.25, 0.125, 0.25);
}
