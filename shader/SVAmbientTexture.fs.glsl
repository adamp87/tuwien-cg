/**
  * @brief %Shadow Volumes shader
  * @author Felix
  * @namespace GLSL
  * @class ShadowVolumes
  * @details
  */

#version 420 core

in vec2 texCoords;

uniform sampler2D colorTexture;
uniform vec4 ambientColor;

out vec4 color;

void main(void) {
     float ambientTerm = 0.2;
     color = vec4(ambientTerm * texture(colorTexture,texCoords).rgb,1.0);
}
