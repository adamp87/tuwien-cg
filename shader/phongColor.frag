/**
  * @brief %Phong shading
  * @author Felix
  * @namespace GLSL
  * @class Phong
  * @details simple Color shader based on the phong model, extendable to multiple light sources
  */

#version 430

out vec4 vFragColor;

in vec3 mNormal;
in vec3 mLightDir;
invariant in vec4 WS_pos;

uniform vec4 McameraPosition;
//fix this!
uniform vec4 ambientColor;
uniform vec4 diffuseColor;
uniform vec4 specularColor;
uniform float shininess;

#define LIGHTS 1

void main(void) {
     vec4 finalColor = vec4(0.0,0.0,0.0,0.0);
     vec3 mNormalNormalized = normalize(mNormal);
     vec3 mLightDirNormalized = normalize(mLightDir);
  
     for(int i=0; i<LIGHTS; i++) {
     
          vec3 vView = normalize(vec3(McameraPosition - WS_pos));
          
          vec4 diffusePart = max(0.0, dot(mNormalNormalized, mLightDirNormalized)) * diffuseColor;
          diffusePart = clamp(diffusePart,0.0,1.0);
          
          vec4 specularPart = vec4(0,0,0,0);
          
          if(diffusePart != vec4(0.0,0.0,0.0,0.0)) {
               vec3 vReflection = normalize(-reflect(mLightDirNormalized, mNormalNormalized));
               specularPart = pow(max(0.0, dot(vView, vReflection)),shininess) * specularColor;
               specularPart = clamp(specularPart,0.0,1.0);
          }
          
          finalColor += (diffusePart + specularPart);
     }
     
     vFragColor = finalColor;
}    
