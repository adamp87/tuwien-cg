/**
  * @brief Phong with %ShadowMap shader
  * @author Adam
  * @namespace GLSL
  * @class ShadowMap
  * @details Compute in MV space
  * @sa http://www.opengl-tutorial.org/beginners-tutorials/tutorial-8-basic-shading/
  */

#version 330

in vec3 normal;
in vec3 position;

out vec3 M_position;
out vec3 MV_normal;
out vec3 MV_eyeDirection;
out vec3 MV_lightDirection;
out vec4 gl_Position;
out vec4 shadowCoord;
out float M_lightDistance;

uniform mat4 M;
uniform mat4 V;
uniform mat4 MV;
uniform mat4 MVP;
uniform mat4 depthBiasMVP;
uniform vec3 MlightPosition;
 
void main(void)
{
    // Position of the vertex, in worldspace : M * position
    M_position = vec4(M * vec4(position, 1)).xyz;

    // Vector that goes from the vertex to the camera, in camera space.
    // In camera space, the camera is at the origin (0,0,0).
    vec3 MV_position = vec4(MV * vec4(position, 1)).xyz;
    MV_eyeDirection = vec3(0, 0, 0) - MV_position;

    // Vector that goes from the vertex to the light, in camera space.
    vec3 MV_lightPosition = vec4(V * vec4(MlightPosition, 1)).xyz;
    MV_lightDirection = MV_lightPosition + MV_eyeDirection;
    M_lightDistance = distance(MlightPosition, M_position);

    // Normal of the the vertex, in camera space
    MV_normal = vec4(MV * vec4(normal, 0)).xyz;

    // Output position of the vertex, in clip space : MVP * position
    gl_Position = MVP * vec4(position, 1.0);

    shadowCoord = depthBiasMVP * vec4(M_position, 1.0);
}
