/**
  * @brief Draw object ID to texture
  * @author Adam
  * @namespace GLSL
  * @class Selector
  * @details
  */

#version 330

in vec3 position;
out vec4 gl_Position;

uniform mat4 MVP;

void main(){
    gl_Position = (MVP * vec4(position, 1));
}

