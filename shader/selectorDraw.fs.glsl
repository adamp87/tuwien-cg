/**
  * @brief Draw object ID to texture
  * @author Adam
  * @namespace GLSL
  * @class Selector
  * @details
  */

#version 330

layout (location=0) out uint id;

uniform uint ID;

void main() {
    id = ID;
}

