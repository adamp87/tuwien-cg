/**
  * @brief %Shadow Volumes shader
  * @author Felix
  * @namespace GLSL
  * @class ShadowVolumes
  * @details
  */

#version 430

layout(triangles_adjacency) in;
layout(triangle_strip) out;

//(3 + 3 for the two caps plus 4 x 3 for the sides)
layout(max_vertices=18) out;

uniform mat4 PV_mat;
uniform vec4 l_pos;

in vec3 mNormal[6];
in vec3 mLightDir[6];
in vec2 texCoords6[6];
invariant in vec4 WS_pos[6]; 


void main(void)
{

vec3 ns[3]; // Normals
vec3 d[3]; // Directions toward light
vec4 v[4]; // Temporary vertices

// Triangle oriented toward light source
vec4 or_pos[3];
or_pos[0] = WS_pos[0];
or_pos[1] = WS_pos[2];
or_pos[2] = WS_pos[4];

// Compute normal at each vertex.
ns[0] = cross(WS_pos[2].xyz-WS_pos[0].xyz,WS_pos[4].xyz-WS_pos[0].xyz );
ns[1] = cross(WS_pos[4].xyz-WS_pos[2].xyz,WS_pos[0].xyz-WS_pos[2].xyz );
ns[2] = cross(WS_pos[0].xyz-WS_pos[4].xyz,WS_pos[2].xyz-WS_pos[4].xyz );
// Compute direction from vertices to light.
d[0] = l_pos.xyz-l_pos.w*WS_pos[0].xyz;
d[1] = l_pos.xyz-l_pos.w*WS_pos[2].xyz;
d[2] = l_pos.xyz-l_pos.w*WS_pos[4].xyz; 

// Check if the main triangle faces the light.
if( !(dot(ns[0],d[0])>0 || dot(ns[1],d[1])>0 ||dot(ns[2],d[2])>0) ) 
{
return;
// Not facing the light => irrelevant for SV
}

// when we get here, we know current triangle is facing the light
const bool faces_light=true;
float eps = 0.000;


// Render caps-only needed for z-fail.
// Near cap-simply render triangle

gl_Position = PV_mat*(or_pos[0] - vec4(d[0]*eps,0.0)); EmitVertex();
gl_Position = PV_mat*(or_pos[1] - vec4(d[1]*eps,0.0)); EmitVertex();
gl_Position = PV_mat*(or_pos[2] - vec4(d[2]*eps,0.0)); EmitVertex();
EndPrimitive();

// Far cap-extrude positions to infinity (w=0)
// note the different triangle-winding order (0-1-2 => 0-2-1)
v[0] = vec4(l_pos.w*or_pos[0].xyz-l_pos.xyz,0);
v[1] = vec4(l_pos.w*or_pos[2].xyz-l_pos.xyz,0);
v[2] = vec4(l_pos.w*or_pos[1].xyz-l_pos.xyz,0);
gl_Position = PV_mat*v[0]; EmitVertex();
gl_Position = PV_mat*v[1]; EmitVertex();
gl_Position = PV_mat*v[2]; EmitVertex();
EndPrimitive();




// Loop over all edges and extrude if needed.
for(int i=0; i<3; i++ ) {
// Compute indices of neighbor triangle.
int v0 = i*2;
int nb = (i*2+1);
int v1 = (i*2+2) % 6;
// Compute normals at vertices, the *exact*
// same way as done above!
ns[0] = cross(WS_pos[nb].xyz-WS_pos[v0].xyz,WS_pos[v1].xyz-WS_pos[v0].xyz);
ns[1] = cross(WS_pos[v1].xyz-WS_pos[nb].xyz,WS_pos[v0].xyz-WS_pos[nb].xyz);
ns[2] = cross(WS_pos[v0].xyz-WS_pos[v1].xyz,WS_pos[nb].xyz-WS_pos[v1].xyz); 
// Compute direction to light, again as above.
d[0] =l_pos.xyz-l_pos.w*WS_pos[v0].xyz;
d[1] =l_pos.xyz-l_pos.w*WS_pos[nb].xyz;
d[2] =l_pos.xyz-l_pos.w*WS_pos[v1].xyz; 

// Extrude the edge if it does not have a
// neighbor, or if it's a possible silhouette.
if( WS_pos[nb].w<0.001 || (faces_light!=(dot(ns[0],d[0])>0 ||dot(ns[1],d[1])>0 ||dot(ns[2],d[2])>0) )) {
// Make sure sides are oriented correctly.
int i0 = faces_light ? v0 : v1;
int i1 = faces_light ? v1 : v0;
v[0] = WS_pos[i0];
v[1] = vec4(l_pos.w*WS_pos[i0].xyz-l_pos.xyz, 0);
v[2] = WS_pos[i1];
v[3] = vec4(l_pos.w*WS_pos[i1].xyz-l_pos.xyz, 0);
// Emit a quad as a triangle strip.
gl_Position = PV_mat*v[0]; EmitVertex();
gl_Position = PV_mat*v[1]; EmitVertex();
gl_Position = PV_mat*v[2]; EmitVertex();
gl_Position = PV_mat*v[3]; EmitVertex();
EndPrimitive();
}
}
}
