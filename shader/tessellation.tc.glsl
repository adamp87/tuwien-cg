/**
  * @brief %Tessellation shader
  * @author Adam
  * @namespace GLSL
  * @class Tessellation
  * @details
  */

#version 410 core

// define the number of Vertices in the output patch
// (can be different from the input patch size)
layout (vertices = 3) out;

//attributes of the input Vertices (from Vertex Shader)
in vec3 in_Position_CS[];
in vec3 in_Normal_CS[];
in vec2 in_UV_CS[];

// attributes of the output Vertices (to Tessellation Evaluation Shader)
out vec3 in_Position_ES[];
out vec3 in_Normal_ES[];
out vec2 in_UV_ES[];

uniform uint tessLevelIn;
uniform uint tessLevelOut;

void main(void)
{
    // Set the control points (vertices) of the output patch
    in_Position_ES[gl_InvocationID] = in_Position_CS[gl_InvocationID];
    in_Normal_ES[gl_InvocationID] = in_Normal_CS[gl_InvocationID];
    in_UV_ES[gl_InvocationID] = in_UV_CS[gl_InvocationID];

    // Calculate the tessellation levels
    if (gl_InvocationID == 0) {
        //NOTE: zero means dynamic, disabled
        uint _tessLevelIn = tessLevelIn;
        if(tessLevelIn == 0)
            _tessLevelIn = 1;
        uint _tessLevelOut = tessLevelOut;
        if(tessLevelOut == 0)
            _tessLevelOut = 1;

        gl_TessLevelOuter[0] = _tessLevelOut;
        gl_TessLevelOuter[1] = _tessLevelOut;
        gl_TessLevelOuter[2] = _tessLevelOut;
        gl_TessLevelInner[0] = _tessLevelIn;
    }
}
