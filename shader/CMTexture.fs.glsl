/**
  * @brief %CubeMap effect shader
  * @author Felix
  * @namespace GLSL
  * @class CubeMapEffect
  * @details
  */

#version 420 core

in GS_OUT {
	vec3 mNormal;
	vec3 mLightDir;
	vec2 texCoords;
	vec3 mPosition;
} gs_out;

uniform vec4 McameraPosition;
uniform sampler2D colorTexture;
uniform vec4 ambientColor;
uniform vec4 diffuseColor;
uniform vec4 specularColor;
uniform float shininess;

layout(location = 0) out vec4 color;

#define LIGHTS 1

void main(void) {

     vec4 finalColor = ambientColor;
     vec3 mNormalNormalized = normalize(gs_out.mNormal);
     vec3 mLightDirNormalized = normalize(gs_out.mLightDir);
     
     #if 0
     for(int i=0; i<LIGHTS; i++) {
     
          vec3 vView = normalize(vec3(McameraPosition - vec4(gs_out.mPosition,1)));
          
          vec4 diffusePart = max(0.0, dot(mNormalNormalized, mLightDirNormalized)) * diffuseColor;
          diffusePart = clamp(diffusePart,0.0,1.0);
          
          vec4 specularPart = vec4(0,0,0,0);
          
          if(diffusePart != vec4(0.0,0.0,0.0,0.0)) {
               vec3 vReflection = normalize(-reflect(mLightDirNormalized, mNormalNormalized));
               specularPart = pow(max(0.0, dot(vView, vReflection)),shininess) * specularColor;
               specularPart = clamp(specularPart,0.0,1.0);
          }
          
          finalColor += (diffusePart + specularPart);
     }
     #endif
     
     vec4 diffusePart = clamp(dot(mNormalNormalized, mLightDirNormalized),0.0,1.0) * diffuseColor;
     finalColor += diffusePart;
     color = finalColor * texture(colorTexture, gs_out.texCoords) /** vec4(0.95, 0.80, 0.45, 1.0)*/;
}
