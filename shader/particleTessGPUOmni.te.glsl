/**
  * @brief %Particle GPU shader
  * @author Adam
  * @namespace GLSL
  * @class ParticleGPU
  * @details Shader to compute OmniShadows
  */

#version 410 core

// tell PG to emit triangles in counter-clockwise order with equal spacing
layout(triangles, equal_spacing, ccw) in;

//these vertex attributes are passed down from the TCS
in vec4 in_Particle_ES[];
in vec3 in_Position_ES[];
in vec3 in_Normal_ES[];
in vec2 in_UV_ES[];

uniform mat4 M;

uniform sampler3D dispTexture; // displacement values(u, v, time)

// Interpolate values v0-v2 based on the barycentric coordinates
// of the current vertex within the triangle
vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2) {
    return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
}

// Interpolate values v0-v2 based on the barycentric coordinates
// of the current vertex within the triangle
vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2) {
    return vec3(gl_TessCoord.x) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;
}

void main(void)
{
    vec4 particle = in_Particle_ES[0];
    // Interpolate attribs of output vertex using its barycentric coords
    vec3 position = interpolate3D(in_Position_ES[0],
                                  in_Position_ES[1],
                                  in_Position_ES[2]);

    vec3 normal = normalize(interpolate3D(in_Normal_ES[0],
                                          in_Normal_ES[1],
                                          in_Normal_ES[2]));

    vec2 uv = interpolate2D(in_UV_ES[0], in_UV_ES[1], in_UV_ES[2]);

#if 0
    // transform cube to sphere
    float x = position.x;
    float y = position.y;
    float z = position.z;
    float dx = x * sqrt(1 - (y*y/2) - (z*z/2) + (y*y*z*z/3));
    float dy = y * sqrt(1 - (z*z/2) - (x*x/2) + (z*z*x*x/3));
    float dz = z * sqrt(1 - (x*x/2) - (y*y/2) + (x*x*y*y/3));
    position = vec3(dx, dy, dz);
#endif
    // transform cube to sphere
    // faster for unit cube
    position = normalize(position);

    // Displace the vertex along the normal, particle.w is the time
    float displacement = texture(dispTexture, vec3(uv, particle.w)).r;
    position += normal * displacement * (1 - particle.w);

    // like vertex shader
    //particle is in model space, w is the time, used as a scaling factor
    
    vec3 mPosition = particle.xyz + vec3(M * vec4(position.xyz * particle.w, 1));

    gl_Position = vec4(mPosition, 1);
}
