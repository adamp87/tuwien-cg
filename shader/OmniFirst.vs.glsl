/**
  * @brief %OmniShadow effect shader
  * @author Felix
  * @namespace GLSL
  * @class OmniShadowEffect
  * @details
  */

#version 330 core

uniform mat4 M; //model matrix (passed per object)
in vec3 position; // object space vertex positions

void main(void) {
     gl_Position = M * vec4(position, 1.0);
}
