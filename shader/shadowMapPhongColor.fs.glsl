/**
  * @brief Phong with %ShadowMap shader
  * @author Adam
  * @namespace GLSL
  * @class ShadowMap
  * @details Compute in MV space
  * @sa http://www.opengl-tutorial.org/beginners-tutorials/tutorial-8-basic-shading/
  */

#version 330

in vec4 shadowCoord;
in vec3 M_position;
in vec3 MV_normal;
in vec3 MV_eyeDirection;
in vec3 MV_lightDirection;
in float M_lightDistance;

out vec4 color;

uniform vec4 ambientColor;
uniform vec4 diffuseColor;

uniform vec3 lightColor;
uniform vec3 lightPower;
uniform sampler2DShadow shadowMap;

void main()
{
    // Normal of the computed fragment, in camera space
    vec3 n = normalize(MV_normal);

    // Direction of the light (from the fragment to the light)
    vec3 l = normalize(MV_lightDirection);

    // Cosine of the angle between the normal and the light direction,
    // clamped above 0
    //  - light is at the vertical of the triangle -> 1
    //  - light is perpendicular to the triangle -> 0
    //  - light is behind the triangle -> 0
    float cosTheta = clamp(dot(n, l), 0, 1);

    // compute shadow map using PCF
    float bias = 0.005;
    float visibility = texture(shadowMap, vec3(shadowCoord.x, shadowCoord.y, shadowCoord.z - bias));

    //compute output color
    color = ambientColor +
            visibility * diffuseColor * vec4(lightColor,1) * vec4(lightPower,1) * cosTheta / M_lightDistance;
}
