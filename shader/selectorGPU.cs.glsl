/**
  * @brief GPU based object selector
  * @author Adam
  * @namespace GLSL
  * @class Selector
  * @details
  */

#version 430 core

#define LOCAL_SIZE 8

layout(local_size_x=LOCAL_SIZE, local_size_y=LOCAL_SIZE, local_size_z=1) in;

layout(binding = 0, std430) buffer Minimum { uint minimum[]; };
layout(binding = 1, r16ui) uniform readonly uimage2D idBuffer;

layout(location = 0) uniform ivec2 size;
layout(location = 2) uniform ivec2 mouse;

void main()
{//run in a pixel

    //compute thread's pixel position
    ivec2 pixel = mouse + ivec2(gl_GlobalInvocationID) - ivec2(gl_NumWorkGroups) * LOCAL_SIZE / 2;

    //is pixel in bounds
    if(size.x <= pixel.x || size.y < pixel.y || pixel.x < 0 || pixel.y <= 0)
        return;

    //get object id at pixel
    uint id = imageLoad(idBuffer, ivec2(pixel.x, size.y - pixel.y)).r;

    //no object at pixel
    if(id == 0)
        return;

    //compute distance, pack id and store if less
    float distPixel = distance(pixel, mouse);
    uint idist = (uint(distPixel) << 16) | id;
    atomicMin(minimum[0], idist);
    //atomicMin(minimum[gl_WorkGroupID.x * gl_NumWorkGroups.x + gl_WorkGroupID.y], idist);
    //atomicMin(minimum[gl_LocalInvocationID.x * LOCAL_SIZE + gl_LocalInvocationID.y], idist);
}
