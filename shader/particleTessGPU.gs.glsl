/**
  * @brief %Particle GPU shader
  * @author Adam
  * @namespace GLSL
  * @class ParticleGPU
  * @details Improves normals, but normal mapping should be used in fragment shader
  */

#version 410 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in vec3 in_gs_mNormal[3];
in vec3 in_gs_position[3];
in vec3 in_gs_mPosition[3];
in vec3 in_gs_mLightDir[3];

out vec3 mNormal;
out vec3 mPosition;
out vec3 mLightDir;

uniform mat3 N;

void main(void)
{
    vec3 A = vec3(in_gs_mPosition[2] - in_gs_mPosition[0]);
    vec3 B = vec3(in_gs_mPosition[1] - in_gs_mPosition[0]);
    vec3 faceNormal = normalize(cross(B, A));

    float ratio = 0.5;
    mNormal = in_gs_mNormal[0]+(faceNormal-in_gs_mNormal[0])*ratio;
    mLightDir = in_gs_mLightDir[0];
    mPosition = in_gs_mPosition[0];
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();

    mNormal = in_gs_mNormal[1]+(faceNormal-in_gs_mNormal[1])*ratio;
    mLightDir = in_gs_mLightDir[1];
    mPosition = in_gs_mPosition[1];
    gl_Position = gl_in[1].gl_Position;
    EmitVertex();

    mNormal = in_gs_mNormal[2]+(faceNormal-in_gs_mNormal[2])*ratio;
    mLightDir = in_gs_mLightDir[2];
    mPosition = in_gs_mPosition[2];
    gl_Position = gl_in[2].gl_Position;
    EmitVertex();

    EndPrimitive();
}
