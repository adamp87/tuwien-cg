/**
  * @brief %Phong with OmniShadow shading
  * @author Felix
  * @namespace GLSL
  * @class PhongOmni
  * @details simple Color shader based on the phong model, extendable to multiple light sources
  */

#version 430

out vec4 vFragColor;

smooth in vec3 mNormal;
smooth in vec3 mLightDir;
smooth in vec2 texCoords;
smooth in vec3 mPosition;

uniform vec4 McameraPosition;
uniform vec3 MlightPosition;
uniform sampler2D colorTexture;
//fix this!
uniform vec4 ambientColor;
uniform vec4 diffuseColor;
uniform vec4 specularColor;
uniform float shininess;

uniform vec2 near_far;
uniform samplerCubeShadow cm_z_tex;

#define LIGHTS 1

void main(void) {    
 // calculate vector from surface point to light position
    // (both positions are given in world space)
    vec3 cm_lookup_vec = mPosition.xyz - MlightPosition.xyz;
    float lightDist = length(cm_lookup_vec);
    float bias = 0.005;
    // read depth value from cubemap shadow map
    float shadowCoord = texture(cm_z_tex, vec4(normalize(cm_lookup_vec),(lightDist/near_far.y) - bias));
          vec4 finalColor = vec4(0.5,0.5,0.5,0.0);
          vec3 mNormalNormalized = normalize(mNormal);
          vec3 mLightDirNormalized = normalize(mLightDir);
          
          for(int i=0; i<LIGHTS; i++) {
          
               vec3 vView = normalize(vec3(McameraPosition - vec4(mPosition,1)));
               
               vec4 diffusePart = max(0.0, dot(mNormalNormalized, mLightDirNormalized)) * diffuseColor;
               diffusePart = clamp(diffusePart,0.0,1.0);
               
               vec4 specularPart = vec4(0,0,0,0);
               
               if(diffusePart != vec4(0.0,0.0,0.0,0.0)) {
                    vec3 vReflection = normalize(-reflect(mLightDirNormalized, mNormalNormalized));
                    specularPart = pow(max(0.0, dot(vView, vReflection)),shininess) * specularColor;
                    specularPart = clamp(specularPart,0.0,1.0);
               }
               
               finalColor += ((diffusePart + specularPart) * shadowCoord);
          }
          
          vFragColor = finalColor * texture(colorTexture, texCoords);
}
